
// 
const express = require('express');
const PORT = 3;
const path = require('path');
const app = express();
const url = require('url');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const pug = require('pug');
const mongo = require("mongodb");

// import my modules for middleware :
const con_pg = require('./config/database/con_pg.js');
const main_routes = require("./server/routes/main_routes/main_routes.js");
const con_adm = require("./server/routes/admin_routes/admin_routes.js");
const express_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_EXPESS_JS/ROUTES_EXPESS_JS.js");     
const node_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_NODE.JS/ROUTES_NODE.JS.js");
const radio_electronica = require("./server/routes/wallpapers/radio_electronics/RADIO_ELECTRONICA.js");
const security_web = require("./server/routes/wallpapers/security_web/SECURITY_WEB.js");
const header_menu_news = require("./server/routes/header_menu/menu_routes/menu_routes.js");
const javascript_routes = require('./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_JAVASCRIPT/ROUTES_JAVASCRIPT.js');
const operating_systems = require('./server/routes/wallpapers/operating_systems/OPERATING_SYSTEMS.js');
const all_about_the_state = require('./server/routes/wallpapers/all_about_the_state/ALL_ABOUT_THE_STATE.js');
const learn_english = require('./server/routes/wallpapers/learn-english/LEARN_ENGLISH.js');
const mathematics = require('./server/routes/wallpapers/mathematics/MATHEMATICS.js');
const juridica = require('./server/routes/wallpapers/juridica/juridica.js');
const page_programming = require('./server/routes/wallpapers/programming/page-programming/PAGE_PROGRAMMING.js');
const text_redactors = require("./server/routes/wallpapers/programming/text-redactors/TEXT_REDACTORS.js");
const c_plus_plus_stack = require("./server/routes/wallpapers/programming/choose-stack/STACK_C++/STACK_C++.js");
const css_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_CSS3/ROUTES_CSS3.js");
const biologia = require("./server/routes/wallpapers/biologia/BIOLOGIA.js");
const entertainment = require("./server/routes/wallpapers/entertainment/ENTERTAINMENT.js");
const hided_left_rectangle = require("./server/routes/hided-left-rectangle/hided_left_rectangle.js");
const registration = require("./server/routes/registration/registration.js");
const routes_pug = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_PUG/ROUTES_PUG.js");
const java_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_ASSEMBLY/STACK_ASSEMBLY.js");
const assembly = require("./server/routes/wallpapers/programming/choose-stack/STACK_JAVA/STACK_JAVA.js");
const web_pack_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_WEB_PACK/ROUTES_WEB_PACK.js");
const sass_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_SASS/ROUTES_SASS.js");
const terminal_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_TERMINAL/ROUTES_TERMINAL.js");
const git_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_GIT/ROUTES_GIT.js");
const redis_routes = require("./server/routes/wallpapers/programming/databases/REDIS_DB/REDIS_DB.js");
const postgres_routes = require("./server/routes/wallpapers/programming/databases/POSTGRES/POSTGRES.js");
const os = require("./server/routes/wallpapers/operating_systems/OPERATING_SYSTEMS.js");
const network = require("./server/routes/wallpapers/work_with_networks/NETWORKS.js");
const automation = require("./server/routes/wallpapers/automation/AUTOMATION.js");
const universe = require("./server/routes/wallpapers/universe/UNIVERSE.js");
const react_routes = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_REACT_JS/REACT.js");
const bootstrap = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_BOOTSTRAP/BOOTSTRAP.js");
const ecmascript = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_ECMASCRIPT/ECMASCRIPT.js");
const typescript = require("./server/routes/wallpapers/programming/choose-stack/STACK_JS/ROUTES_TYPESCRIPT/TYPESCRIPT.js");


// config
app.set('view engine', 'pug');
app.set('views', './views');

// Trebuie pentru ca să lucreze CSS Files
app.use(express.static('public'))

// connect middleware : 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", con_adm);
app.use('/', con_pg);
app.use("/", main_routes);
app.use("/", express_routes);
app.use("/", node_routes);
app.use("/", javascript_routes);
app.use("/", radio_electronica);
app.use("/", security_web);
app.use("/", header_menu_news);
app.use("/", operating_systems);
app.use("/", learn_english);
app.use("/", mathematics);
app.use("/", juridica);
app.use("/", page_programming);
app.use("/", text_redactors);
app.use("/", c_plus_plus_stack);
app.use("/", all_about_the_state);
app.use("/", css_routes);
app.use("/", biologia);
app.use("/", entertainment);
app.use("/", hided_left_rectangle);
app.use("/", registration);
app.use("/", routes_pug);
app.use("/", java_routes);
app.use("/", assembly);
app.use("/", web_pack_routes);
app.use("/", sass_routes);
app.use("/", terminal_routes);
app.use("/", git_routes);
app.use("/", automation);
app.use("/", postgres_routes);
app.use("/", redis_routes);
app.use("/", os);
app.use("/", network);
app.use("/", automation);
app.use("/", universe);
app.use("/", react_routes);
app.use("/", bootstrap);
app.use("/", ecmascript);
app.use("/", typescript);

app.get("/zero", (req, res) => {
	res.render('index', { title: 'welcome', message: "Aici o fost un bag. " });
});

let server = app.listen(PORT, function() {
	console.log("Server running port " + PORT + "\n");
});

// ciuti ne hrohnul proiect. :|