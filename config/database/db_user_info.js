import client from "../config/database.js";

export default async function getUserInfo(username) {
	try {
		const query = "SELECT * FROM info WHERE username = ?";
		const [rows] = await client.query(query, [username]);
	} catch (error) {
		console.error(error);
	}
}
