![image](public/img/TO.png)

## Screenshots :
If you want to see more images from this project [look here](md/docs/PROJECT_SCREENS.md)

## Topics :
### * [Static Site](md/docs/STATIC.md)
### * [Install on your PC](md/docs/INSTALL_YOUR_PC.md)
### * [Tasks](md/docs/TASKS_AT_SITE.md)
### * [Questions](md/docs/QUESTIONS_AT_SITE.md)

### Extern links :
* link 1
* link 2
