let slideIndex = 0;
showSlidesAuto();

// Функция для автоматического переключения слайдов
function showSlidesAuto() {
  let i;
  let slides = document.getElementsByClassName("slide");
  let dots = document.getElementsByClassName("dot");

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";

  // Меняем слайд каждые 5 секунд (5000 миллисекунд)
  setTimeout(showSlidesAuto, 5000); 
}

// Управление слайдами вручную (если нужно)
function plusSlides(n) {
  showSlidesManual(slideIndex += n);
}

// Функция для смены слайдов вручную
function showSlidesManual(n) {
  let i;
  let slides = document.getElementsByClassName("slide");
  let dots = document.getElementsByClassName("dot");

  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

// Функция для перехода на конкретный слайд по клику на точку
function currentSlide(n) {
  showSlidesManual(slideIndex = n);
}
