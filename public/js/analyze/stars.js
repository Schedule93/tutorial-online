const stars = document.querySelectorAll('.star');

stars.forEach((star, index) => {
    star.addEventListener('click', () => {
        reset_stars(); // Сбрасываем все звезды
        highlightStars(index); // Подсвечиваем звезды до текущей включительно
        star.classList.add('selected'); // Отмечаем текущую как выбранную
    });

    star.addEventListener('mouseover', () => {
        highlightStars(index); // Подсвечиваем при наведении
    });

    star.addEventListener('mouseout', () => {
        const selectedStar = document.querySelector('.star.selected');
        if (selectedStar) {
            highlightStars([...stars].indexOf(selectedStar)); // Подсвечиваем выбранные звезды
        } else {
            reset_stars(); // Если звезда не выбрана, сбрасываем подсветку
        }
    });
});

function highlightStars(index) {
    stars.forEach((star, i) => {
        if (i <= index) {
            star.style.color = 'gold'; // Подсвечиваем все звезды до текущей включительно
        } else {
            star.style.color = 'lightgray'; // Оставляем остальные звезды серыми
        }
    });
}

function reset_stars() {
    stars.forEach(star => star.style.color = 'lightgray'); // Сбрасываем все звезды
}
