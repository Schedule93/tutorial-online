![image](../../../public/img/kuro-haikyuu.png)

### [return main page](../../../STATIC.md) | Chapter 1. Learn Vim-Text-Editors : 

> Практика — это путь к совершенству.

### Chapter 1. Learn Vim.
**notes** Dacă o să lucrezi cu recatorul Vim. nu o să-ți trebuiască linia de comandă sau terminal, deoarece Vim are a lui, cît și terminal atît și redactor text. Кроме того, интересно то, что его более простым аналогом является Vi — стандартный текстовый редактор. Хочу отметить, что Windows-пользователям я рекомендую использовать WSL (Windows Subsystem for Linux

**notes** vim основные команды

### Уроки Vim
* [Intro in Vim](VIM_INTRO.md)
* [Install Vim](VIM_INSTALL.m)
* [Vim Tutor](VIM_TUTOR.md)
* [Vim Licence](VIM_LICENCE.md)
* [Vim Script](VIM_SCRIPT.md)
* [Vim Plugins](VIM_PLUGINS.md)
* [Vim Modes](VIM_MODES.md)
* [Vim Questions](VIM_QUESTIONS.md)
* [Vim Integration](VIM_INTEGRATION.md)
* Перенастройка Клавиш
* [Лицензия Vim На Русском](VIM_LICENCE.md)

### Hot keys
* "i" - для того, чтобы начать вводить текст
* 

### Books :
* [Просто о VIM](https://cloud.mail.ru/public/rNZ3/FeHk4iRjV)
* [Практический Vim](http://needworld.ru/attachments/drju-nejl-prakticheskoe-ispolzovanie-vim-pdf.22182/)

### Extern Video :
* Video 1 
* Video 2

### Extern Links :
* Второе что нам нужно это програма `vimtutor` где подробно описывает базовые уроки VIM на русском языке.
* [YouTube : Основы Vim playlist](https://www.youtube.com/watch?v=zNnsNtBF80g&list=PLcjongJGYetkY4RFSVftH43F91vgzqB7U&ab_channel=DevTalk)
* [You Tube : Vim in 100 Seconds](https://www.youtube.com/watch?v=-txKSRn0qeA&list=PLQqEY2kzSbZ4NMd7xsuc28a6Kc-_300Jb&index=13&ab_channel=Fireship)
* [Stack Overflow](https://stackoverflow.com/tags/vim/info)
* [full tutorial](https://vimhelp.org/vim_faq.txt.html) 
* [Open Vim](https://www.openvim.com/)
* [Tutorialpoint](https://www.tutorialspoint.com/vim/index.htm)
* [Полезные материалы по vim](https://kergma.net/stranica/poleznye-materialy-po-vim)
* Uite-te la mine pe github
* [Stack Overflow : Frequent Questions](https://stackoverflow.com/questions/tagged/vim?tab=Frequent)
* [Vim Stack Exchange](https://vi.stackexchange.com/) Want to ask a question. Can i work with a help at vim from my `mobile` and `terminal` + `ssh` ?
* [Каковы преимущества изучения Vim?](https://stackoverflow.com/questions/597077/what-are-the-benefits-of-learning-vim)
* [Режымы в Vim](http://rus-linux.net/MyLDP/BOOKS/Vim/prosto-o-vim-07.html)
* [Программирование скриптов](https://jenyay.net/Programming/VimScript1)
* [ЧТО ТАКОЕ VIM ?](https://codeby.net/blogs/chto-takoe-vim-dlja-chego-on-nuzhen-i-s-chego-nachat-ego-izuchenie/)
