### [return main page](../../README.md) | [Chapter 1. Learn Text Editors](PAGE-TEXT-EDITORS.md) : 

### Text Recator : Sublime-text 3 :

* **Lcence**
* **Written**
* **Platform**

**note 1** Trebuie să cunoști că la `Sublime-Text 3` după ce faci toate setarile de care ai nevoie, peste o perioadă de timp o să-ți schimbe automat setarile în `default` și ai să le pierzi pe celea setate, adica toate. Așa că dacă doresști să nu se întîmple așa ceva atunci îți recomand să faci în felul următor. În primul rînd deschide meniul principal la sublime-text 3. 

1. `Preference`
2. `Browser Packages`
3. În cazul meu, dacă ai tema `ayu`. O să ai folder de mai jos : 
4. Copie folderul `zzz A File Icon zzz` în alt loc unde dorești tu sau
5. Arhivează folderul zzz A File Icon zzz cu ajutorul la `Winrrar` 
6. În aceast folder zzz A File Icon zzz, se află totul: tema ayu, font-size mai mare, icon.
7. În caz dacă ceva sa stricat pu și sumplu adaugă folderul de mai sus care am spus să-l copii, acum adaugă în directoria `C:\Users\Nicolai Cushnir\AppData\Roaming\Sublime Text 3\Packages`

**Note 2** Licența la Sublime Text 3 este `Proprietary licence`

**Answers: Hot **

* `Ctrl + I` Căutare live
* `Ctrl + D` Evidențiază un cuvînt
* `Ctrl + L` Evidențiază tot rîndul
* `Ctrl + F` Căutarea unui cuvînt
* `Alt + 3` Navigarea la открытые вкладки. Numărul 3 asta e doar a cîta vrei so deschizi, dar dacă ai deschise numai 2 atunci nimic nu o să se întîmple.

**Questions**

1. Cum să pleci la început de la rîndul `n` ?
2. Cum Să pleci la sfîrșit de la rîndul `n` ?
3. `ctrl + l` evidențiază rîndul `n`.
4. Cum să pleci la primul rînd din ST3 ?
5. Cum să pleci la ultimul rînd din ST3 ?
6. Cum Să înlocuești un cuvînt, cu altul.

### links

* [Шпаргалка ниндзя](https://nicothin.pro/sublime-text/sublime-text-3-hotkeys.html)
