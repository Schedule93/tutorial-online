### return 

=    W e l c o m e   t o   t h e   V I M   T u t o r    -    Version 1.5      =

* `:q!` exit from Vim without to save something
* Для перехода в командный режим требуется нажатие Esc. Все команды в VIM вводятся с символа двоеточия - ":" (без кавычек). Для перехода в визуальный режим требуется нажатие Ctrl+V.

### Links
* [Super User](https://superuser.com/questions/246487/how-to-use-vimtutor)
* [Stack Overflow : How do I exit the Vim editor?](https://stackoverflow.com/questions/11828270/how-do-i-exit-the-vim-editor)