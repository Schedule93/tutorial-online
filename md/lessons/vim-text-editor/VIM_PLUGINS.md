### [return main page](../../README.md) | Chapter 1. Learn Text Editors:

* В `VIM` есть очень много плагинов. Это означает что можно превратить в палнацаную IDE.
* Настройте редактор Vim под себя

### Links
* [Color Schemes](https://blog.pabuisson.com/2018/06/favorite-color-schemes-modern-vim-neovim/)
* [Vim Awesome](https://vimawesome.com/)