### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

### Topic File System
* Open File : fs.open()
* Create files : fs.writeFile(), fs.appendFile()
* Update files : 
* Delete files : fs.unlink()
* Rename files : fs.rename() 
* readFile()

### Bellow is File system in Sync Mode:

* Transform from string in object and reverse.

```js
// from string in object
var words = JSON.parse(data);
console.log(typeof(words) + " " + words);

// from object in string 
var data = JSON.stringify(words);
console.log(typeof(data) + " " + data);
```

* cel mai probabil acest cod nu este corect. Dar va fii ceva asemănător cu exemplu de mai jos. Așa că uitete pe internet cum să salvezi json files in formă sinhronă, apoi dupa ce ai facut multe exemple cu sync uite și asinhrona.

```js
function save_json() {
    var data = fs.readFileSync('words.json');
    var words = JSON.parse(data);
    console.log(words);
}
```

* Sync Mode

```javascript
	
	// write file. Asyn
	fs.writeFile('test.txt', 'WOW! Here i wrote some text.', function(err){
		if(err) throw err;
		console.log("Text \'fs'\ Saved!");
	});

	// Write file at finish of text.
	fs.appendFileSync("test.txt", "\nHere is some text", function(){
		console.log("Text was successful added. ");
	});

	// read file
	console.log(fs.readFileSync("BUILD.json", {encoding:"utf-8"} ));

	// Create Folder
	fs.mkdirSync('myFolder');

	// open file in sync mode
	fs.openSync();

	// Delete file : fs.unlink()

	// rename file. Also you can cange directory at file.
	console.log(fs.renameSync("myFolder", "thisFolder"));

	// Uitete la Sorax node.js. Async
	fs.readdir();

	// Returns `true`` if the path exists, `false` otherwise.
	fs.existsSync();

	// more exactly than exist way than method existSync()
	fs.stat();

	// delete file
	try {
		fs.unlinkSync('/tmp/hello');
			console.log('successfully deleted /tmp/hello');
	} catch (err) {
			// handle the error
	}

	// delete folder
	fs.rmdirSync("bob", function(){
		console.log("del bob folder.")
	});
```

* Bellow is File system in Async Mode:

```javascript
	// You can open a file for reading or writing using fs.open() method. 
	// This is async version.
	// Trebue sa ai o func close() fara dansa o as deschida la nesfarshit 
	fs.open("test.txt", "w+", function(error, fd) {
		if (error) {
			console.error("open error:  " + error.message);
		} else {
			console.log("Successfully opened " + "test.txt");
		}
	});

	// Read File
	fs.readFile('task.txt', function (err, data) {
   		if (err) {
			return console.error(err);
		}
		
		console.log("Asynchronous read: " + data.toString());
	});

	fs.readFile("BUILD.json", function(err, data){
		if(err) throw err;
		console.log(data.toString());
	});
```

* code train

```js

var data = fs.readFileSync('words.json');
var words = JSON.parse(data);

console.log(words);

app.get('/add/:word/:score?', addWord); 

function addWord(req, res){
	var data = req.params;
	var word = data.word;
	var score = Number(data.score);
	var reply;
	if(!score) {
		reply = {
			msg: "Score is required."
		}
	} else {
		words[word] = score;
		// var data = JSON.stringify(words, null, 2);
		var data = JSON.stringify(words, null, 2);
		fs.writeFile('words.json', data, finished);
		
		function finished(err) {
			console.log('all set. ');
			reply = {
				word: word,
				score: score,
				status: "success"
			}

			res.send(reply);
		}
		
	}
}

app.get('/all', sendAll);
function sendAll(req, res) {
	res.send(words);
} 

app.get('/search/:word/', searchWord);

function searchWord(req, res) {
	var word = req.params.word;
	var reply;
	if(words[word]) {
		reply = {
			status: "found",
			word: word,
			score: words[word]
		}
	} else {
		reply = {
			status: "not found",
			word: word
		}
	}
	res.send(reply);
} 

app.listen(port, function(){
	console.log(`Server running port ${port}`);
});
```

### Exteern links :
* [appdividend.com](https://appdividend.com/2018/10/09/node-js-file-system-module-example-tutorial/)
* [tutorialsteacher.com](https://www.tutorialsteacher.com/nodejs/nodejs-file-system)
* [informit.com](http://www.informit.com/articles/article.aspx?p=2266929&seqNum=3)
* [tutorialpoint](https://www.tutorialspoint.com/nodejs/nodejs_file_system.htm)
* [Get HTTP request body data using Node.js](https://nodejs.dev/get-http-request-body-data-using-nodejs)
