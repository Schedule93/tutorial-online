### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

### Node.js HTTP module.
* Informational responses || Информационные ответы  ( 100 – 199 )
* Successful responses || Успешные ответы ( 200 – 299 )
* Redirection messages || Сообщения о перенаправлении ( 300 – 399 )
* Client error responses || Ответы клиента на ошибку  ( 400 – 499 )
* Server error responses || Ответы сервера на ошибку ( 500 – 599 )

### Questions
* Как обработать http запросы в Node.js

### Links :
* [MDN HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
* Link 2