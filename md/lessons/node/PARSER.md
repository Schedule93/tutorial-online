### [return main page](../../../README.md) || [Chapter ?. Learn Node.js](#)

### Topic : What Means Parser :
`Parser` - Ce-i asta ? Și pentru ce avem nevoie de el ? Ei bine hai să-ți explic pe pași:
1. În primul rînd parser din engleză înseamnă analizator. Dar totusși, 
2. Cu ajutorul lui putem extrage informații de care avem nevoie de pe un site web, de pe internet. Spre exemplu: 
- **Exemplu 1.** Noi dorim să extragem de pe un site web, spre exemplu, `Narutospot`, toate episoadele la noi pe calculator.  
- **Exemplu 2.** Poate vrem să analizăm bugetul unei țări și facem parser la ultimii 10 ani. Și apoi vedem statistica.
- **Exemplu 3.** Un alt exemplu este, cînd dorim să creăm un compilator, atunci se folosește parser, la analiză codului.
- **Exemplu 4.** Desigur poți să parsezi numai o categorie annumite de știri, de la vre un portal de noutăși precum publicaMD etc.
- **Exemplu 5.** Să downlodez numai muzica Rock care îți place de pe YouTube.
3. Parser - Poate fi folosit cu un limbaj de programare. În cazul nostru Node.js.

### Short Algorithm to learn Parser:
1. Cunoașterea HTML, CSS și javaScript.
2. Изучи основы парсенга
3. Быбор инструмент для parser-a.
4. Load the page, `axios`.
5. Поиск и извличение данных: (class, selectors, atributes, tags, text).
6. Обработка данных: (Не хотим всё, только часть).
7. Сохранить где то это всё дабро. :)
8. Обработка оыбок 
9. Применяйте на практику 

### Meet with cheerio:
not be soon ... 

### Exemple of Code :

```js

const cheerio = require('cheerio');
const request = require('request');

// URL для парсинга
const url = 'https://example.com';

// Функция для загрузки HTML с URL и его парсинга
request(url, (error, response, html) => {
  if (!error && response.statusCode === 200) {
    // Загрузка HTML в cheerio
    const $ = cheerio.load(html);

    // Найти элементы с определенным классом, тегом или другими селекторами и сделать что-то с ними
    $('h2').each((index, element) => {
      console.log($(element).text());
    });
  } else {
    console.error('Ошибка при загрузке страницы:', error);
  }
});

```

### Modules for Parser:
* [Intro](INTRO.md)
* [osmosis](#)
* [webdriver](#)
* [casper.js](#)
* [CHALCK](#) Это не парсер! А просто чтобы красиво отобразилось.

### Extern links :
* [iomosis](https://tproger.ru/translations/web-scraping-node-js/)
* [Как парсить сайты и материалы СМИ с помощью JavaScript и Node.js](https://skillbox.ru/media/code/kak-parsit-sayty-i-materialy-smi-s-pomoshchyu-javascript-i-nodejs/)
