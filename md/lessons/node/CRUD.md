### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

```js
const express = require('express');
const app = express();
const cors = require('cors');
const PORT = 5000;
const pool = require('./db');
const helmet = require('helmet');
const morgan = require('morgan');
const pg = require('pg');
const bodyParser = require('body-parser');
const fs = require("fs");

// middleware //
app.use(cors());
app.use(express.json()); // req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// create a todo
app.post('/todos', async (req, res) => {
	try {
		const { description } = req.body;
		const new_todo = await pool.query(
			"INSERT INTO todo (description) VALUES($1) RETURNING *", [description]
		);

		res.json(new_todo.rows[0]);		
	} catch {
		console.error(err.message);
	}
});

app.get('/todos', async (req, res) => {
	try {
		const all_todos = await pool.query("SELECT * FROM todo");
		res.json(all_todos.rows);
	} catch (err) {
		console.error(err.message);
	}
});

// Get a todo
app.get('/todos/:id', async (req, res) => {
	try {
		const {id} = req.params;
		const todo = await pool.query("SELECT * FROM todo WHERE todo_id = $1", [
			id
		]);

		res.json(todo.rows[0]);
	} catch {
		console.error(err.message);
	}
});

// Update a todo
app.put("/todos/:id", async (req, res) => {
	try {
		const {id} = req.params;
		const {description} = req.body;
		const update_todo = await pool.query(
			"UPDATE todo SET description = $1 WHERE todo_id = $2",
			[description, id]
		);

		res.json("todo was update! ...");
	} catch {
		console.error(err.message);
	}
}); 

// Delete a todo
app.delete("/todos/:id", async (req, res) => {
	try {
		const {id} = req.params;
		const delete_todo = await pool.query(
			"DELETE FROM todo WHERE todo_id = $1", [
			id
		]);

		res.json("Todo was deleted!");
	} catch {
		console.error(err);
	}
});


app.listen(PORT, function(){
	console.log("Server Running Port : " + PORT);
});
```

* Download files ?

```js

// Wrong Code !!!
router.get('/', (req, res) => res.download('./file.pdf'))

router.post('/', function(req, res){
   fs.
});
```

### Extern Links :
* [Postman - POST Requests](https://www.tutorialspoint.com/postman/postman_post_requests.htm#:~:text=Postman%20POST%20request%20allows%20appending,information%20to%20us%20in%20Response.)
* [Building a Simple CRUD app with Node, Express, and MongoDB](https://zellwk.com/blog/crud-express-mongodb/)
