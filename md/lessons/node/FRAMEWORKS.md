### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

### Frameworks and Libraries. When to use it.
* Node.js logs - (err)
* Moragan.js
* Windston.js
* [Canvas](https://canvasjs.com/)
* [P5.js](https://p5js.org/)
* [Lodash](https://lodash.com/docs/4.17.11)
* [Matter.js](http://brm.io/matter-js/)
* [Top Frameworks](https://blog.sagipl.com/javascript-frameworks/#What_is_a_JavaScript_Framework)
* [Hapi.js](#)
* [Kraken.js](#)
* [Retire.js](#)
* [Fabric.js](#)
* [Moment.js](https://momentjs.com/)

### Frameworks for Graphics and Charts ...
* [Apexcharts.js](APEXCHARTS.md)
* [Three.js](https://threejs.org/)
* [SVG.js](https://svgjs.com/docs/3.0/getting-started/)

### Framework for High Performance
* [Asm.js](http://asmjs.org/)
* [Web Assembly](https://webassembly.org/getting-started/js-api/)

### Frameworks for Create Desktop App
* Neutralinojs
* Xojo
* OS.js
* Flutter 
* Meteor ?
* Haxe.js

### Model View Controller (MVC)
* [Sails.js](https://sailsjs.com/)
* [Ember.js : Quick Start](https://guides.emberjs.com/release/getting-started/quick-start/)

### Addition
* [Dart.js](https://dart.dev/samples)
* [Haxe](https://haxe.org/manual/target-javascript-getting-started.html)
* [Fastify](FASTIFY.md)
* [Backbonejs](https://backbonejs.org/)

### Links :
* [Everything You Need to Know About Ember.js](https://learn.habilelabs.io/everything-you-need-to-know-about-ember-js-487ee9a686c2)
* Link 2
