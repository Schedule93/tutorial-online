![image](public/img/what-is-node.png)

### [return main page](../../../README.md) | Chapter 12. Learn Node.js : 

### All what you need to know about Node.js
* [Debugger](DEBUGGER.md)
* [Routes](ROUTES.md) **???**
* [Package Managers](PM.md)
* [Import Syntax](IMPORT_&&_REQUIRE.md)
* [JSON](JSON.md)
* [CRUD Operations](CRUD.md)
* [Sync && Async](SYNC_ASYNC.md)
* [Async - Await](ASYNC_AWAIT.md)
* [Parser](PARSER.md)
* [Promise](PROMISE.md)
* [File System](FS.md)
* [Buffer](BUFFER.md)
* [Minuses](MINUSES.md)
* [Architecture](ARCHITECTURE.md) (Dublare)
* [Net](NET.md)
* [Security](SECURITY.md)
* [Modules Node.js](MODULES.md)
* [Readline (I/O)](READLINE.md)
* [Frameworks](FRAMEWORKS.md)
* [Validation Data](VALIDATION.md)
* [Stacktraces](STACKTRACES.md)
* [Fibers](#)
* [Движок Chrome V8](https://v8.dev/) 
* [Middleware](#) **???**
* [Мониторинг](#)
* [Process](#) (Multithreading)
* [Authorization](#)
* [JSON Web Token](#)
* [Рефакторинг](#)
* [Server Side Rendering](#)
* [Deploy](#)
* [Web Workers](https://developer.mozilla.org/ru/docs/Web/API/Web_Workers_API/Using_web_workers)
* [OAuth](https://ru.wikipedia.org/wiki/OAuth) 
* [Менеджер паролей](#)
* [Репликация](#)
* [Конфигурация Node.js](#)
* [Mode.js token](#)
* [Trafic at Node.js](#)
* [Questions](QUESTIONS.md)
* [Архитектура Приложении](#)
* [Node.js Events](#)
* [Logs](LOGS.md)
* [API](API.md)
* [Fetch](FETCH.md)
* [Session](#)
* [Performance](PERFORMANCE.md)

### YouTube
* [YouTube chanel JavaScript.ru](https://www.youtube.com/watch?v=ILpS4Fq3lmw&list=PLsuEohlthXdkRSxJTkmTstWKHgBHsd3Dx)

### Read Books
* Node.js в действии. М.Кантелон, М.Хартер и др.
* Node.js переходим на сторону сервера

### Additionally links :
* [Top 10 Mistakes Node.js Developers Make](https://www.airpair.com/node.js/posts/top-10-mistakes-node-developers-make)
* [Getting Started With Node Hero Tutorial](https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/)
* [Building Live Streaming App with Node.js and React](https://morioh.com/p/76f6ad95bdd2/building-live-streaming-app-with-node-js-and-react)
* [Callback vs promise vs async-await](https://vegibit.com/javascript-callbacks-vs-promises-vs-async-await/)
* [TODO](https://dev.to/abiodunjames/build-a-todo-app-with-nodejs-expressjs-mongodb-and-vuejs--part-1--29n7)
* [Node Hero](https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/)
* [About restify](http://mcavage.me/node-restify/)
* [node-postgres](https://www.youtube.com/watch?v=ufdHsFClAk0&t=795s0/) 
* [blog.logrocket.com](https://blog.logrocket.com/setting-up-a-restful-api-with-node-js-and-postgresql-d96d6fc892d8/)
* [Stack Overflow : How do I update Node.js?](https://stackoverflow.com/questions/8191459/how-do-i-update-node-js)
* [??? Front End ???](https://www.decipherzone.com/blog-detail/front-end-developer-roadmap-2021)
