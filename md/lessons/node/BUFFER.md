### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

### Buffer
Buffer is a matter which work with bites and beats in Node.js.

Класс Bufferв Node.js предназначен для обработки необработанных двоичных данных. Каждый буфер соответствует некоторой необработанной памяти, выделенной вне V8. Буферы действуют как массивы целых чисел, но не изменяют размер и имеют целый набор методов специально для двоичных данных. Каждое целое число в буфере представляет собой байт и поэтому ограничено значениями от 0 до 255 включительно. При использовании console.log()для печати Bufferэкземпляра вы получите цепочку значений в шестнадцатеричном формате.

### Где вы видите буферы:
В дикой природе буферы обычно рассматриваются в контексте двоичных данных, поступающих из потоков, таких как fs.createReadStream.

In primu rand pentru a intelege buffeer trbuie sa cunosti `fs` module. Buffer inseamna nu alt ceva decat cand vrei sa transformi o informatie de tip text cu ajutorul la file system se transofrma in buffer. Cu alte cuvinte buffer asta echivalent cu masiv Baitov. Vrode Easy. Vez nu uita ca din dreapta in singa merge buffer.