### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)
**Note 1** для преобрахование обьекта в строку есть метод JSON.stringfy
**Note 2** Why JSON use like http and not like database ? 

```Algorithm
1. Create a object
2. Check if is object. Obligatoriu!
3. преобрахование обьекта в строку есть метод JSON.stringfy
4. Verificam sa fie tip de date string.
5. sh amu invers din string transforma in object
6. Acum facem asta in practica + module fs
```

1. Mai jos am creat un simplu obiect

```js
var user_1 = {
  id: 19121993,
  full_name: "Nicolai Cushnir",
  age: 27,
  gender: "male"
};
```

2. Pentru ca să verificăm dacă este obiect utilizăm funția `typeof`. În genere aceasă funție arată arată ce tip de dată este. Dar fii atent că javascript aici are niuanse și poți să confunzii multe lucruri așa că fii antent. 

```js
console.log(typeof person); //object
```

3. Pentru ca să convertăm din obiect în string, folosim metoda `JSON.stringfy`. Acum **FOARTE IMPORTANT**, pornește serverul tău cu node.js, spre exemplu `localhost:5555`, că așa în javascript curat el nu o să meargă, trebuie numaidecit să pornești node.js. 

```js
data = JSON.stringify(user_1);
console.log(data);
```

4. Verificam sa fie tip de date string.

```js
console.log(typeof data);
```

5. `JSON.parse();` de string in obiect

```js
console.log("zzz ...");
```

### Exemples:

```js
{
	"1": {
		"id": 1,
		"full name": "Nicolai Cushnir",
		"gender": "male",
		"age": 27
	},

	"2":
	{
		"id": 2,
		"full name": "Mark Tzunkerberg",
		"gender": "male",
		"age": 41
	},

	"3":
	{
		"id": 3,
		"full name": "Lisa Smith",
		"gender": "female",
		"age": 32
	}
}
```