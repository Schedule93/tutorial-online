### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

### Node Events
**note** for learn events in Node.js : i must learn before javascript event and after Node.js events.

* `event emitter`
* `event handler`
* `Trace Events`

### Links
* [The Node.js events module](https://nodejs.dev/learn/the-nodejs-events-module)
