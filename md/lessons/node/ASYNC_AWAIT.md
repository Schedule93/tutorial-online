### [return main page](../../README.md) | Chapter 12. Learn Node.js : 

### Topic : Async / Await.

* E ciudat că `onsole.log` e fără paranteze rotunde. Dar numa așa merge. Fii atent.

```js
async function actresses(arg) {
	return arg;
}

actresses("I love javaScript 2. ").then(console.log);
```


### Extern Links :
* [JavaScriptRu - async-await](https://javascript.info/async-await)
* [Асинхронность в JavaScript](https://doka.guide/js/async-in-js/)