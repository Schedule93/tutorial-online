### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

### Topic : Net

**Note 1** `Web Socket Services` или `Net module`, предназначенный для обмена сообщениями между браузером и веб-сервером в режиме реального времени. 

* Play With Socket

```js
var net = require('net');
var Readable = require('stream').Readable;
var stream = new Readable();
var data = ("bob.js and JavaScript Development bob.js is a JavaScript framework which is intended to make a web/JavaScript developer's life easier. It's all about object orientation and better coding practices in the JavaScript applications. Why objects? I deeply believe that the future of JavaScript development lays under the object-oriented approach. Nowadays, most JavaScript developers seem to prefer more sort of a procedural kind of JavaScript development, which consists of declaring simple functions and calling them from each-other. Although this approach is enough to solve most of the simplistic scenarios and requirements, still there is something else JavaScript developers must take advantage of. Web software demands are increasing both in amount and in complexity, which leads to the very important two questions: Is JavaScript ready to handle complex software scenarios? and Are JavaScript developers ready to handle complex software designs? I will answer these questions below. ").split(" ");

stream._read = function () {
	if(data.length) {
		setTimeout(function(){
			stream.push(data.shift() + ' ');
		}, 200);
	} else {
		stream.push(null)
	}
};

stream.pipe(process.stdout);
```

### Links :

* [Wiki : WebSocket](https://ru.wikipedia.org/wiki/WebSocket)
