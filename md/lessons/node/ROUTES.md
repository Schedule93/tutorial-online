### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

**How to import a new personal module in Node.js**

1. Create a file in root project. In my case will be file `main.js`. 

2. Importing all module `my_db`, what exist in file `db.js` in file `main.js` with next command of more lower. (In other words, ask from him, i mean ask from file db.js)

```js
const my_db = require('./database/db.js');
```

3. Now create another file `db.js`, with direcotry `database/db.js`. Inside root project, and write :

```js
const my_db = require('express').Router();
console.log("This is a simple sentence for to check this module.");
```

4. Lower in file `db.js`, write export the module.

```js
module.exports = my_db;
```

**topic 2 : Abrabotka Routes in Node.js**

i don't know how to make now ...

### Code Train from YouTube -> Flowers :

* Write in browser `http://localhost:5555/search/good/50` or other word && number

```js
app.get('/search/:flower/:num', function(req, res){
	var data = req.params;
	var num = data.num;
	var reply = "";
	
	for(var i = 1; i <= num; i++) {
		reply += "<h2>" + i + " I love " + data.flower + " too." + "</h2>";
	}

	res.send(reply);
});
```

* if we will restart server `nodemon main.js` datele se va pierde care tu ai scris in browser. However, trebuie sa te joci in url adress, cuvinte cheie asa ca 
	1. `http://localhost:5555/all`
	2. `http://localhost:5555/add/rainden`
	3. `http://localhost:5555/search/raiden`
	4. `http://localhost:5555/search/rainden/6`

```js

var words = {
	"rainbow": 5,
	"unicorn": 3,
	"doom": -3,
	"gloom": -2
}

app.get('/add/:word/:score?', addWord); 

function addWord(req, res){
	var data = req.params;
	var word = data.word;
	var score = Number(data.score);
	var reply;
	if(!score) {
		reply = {
			msg: "Score is required."
		}
	} else {
		words[word] = score;
		reply = {
			msg: "Thank you your word."
		}
	}
	res.send(reply);
}

app.get('/all', sendAll);

function sendAll(req, res) {
	res.send(words);
} 

app.get('/search/:word/', searchWord);

function searchWord(req, res) {
	var word = req.params.word;
	var reply;
	if(words[word]) {
		reply = {
			status: "found",
			word: word,
			score: words[word]
		}
	} else {
		reply = {
			status: "not found",
			word: word
		}
	}
	res.send(reply);
} 

app.listen(port, function(){
	console.log(`Server running port ${port}`);
});
```

* ааа

```js
app.get(/^(.+)$/, function(req, res) {
    console.log("Static file request : " + req.params);
    res.sendFile(__dirname + req.params[0]);
});
```