### [return main page](../../README.md) | [Chapter 12. Learn Node.js](PAGE_NODE.md)

**learn first**
* [fs](#)
* [Passport.js](https://github.com/jaredhanson/passport)
* [net](#)
* [websocket](#)
* [HTTP](HTTP.md)

**learn after**
* express
* pug
* morgan
* [WebAssembly](https://webassembly.github.io/spec/core/)

**indreapta mai jos**

### require modules from npm. Why you must know ?
Below is example to connect module `pug`.

```js
const pug = require('pug');
const request = require('request');
const errorHandler require('express-error-handler');
const cron = require('node-cron');
```

All these modules you can find official site `npm`

* node-supervisor
* pug
* morgan
* socket.io
* eslint 
	(ESLint is an open source project that helps developers find and fix problems with their JavaScript code.)
* body-parser
* express-validator
* net
	* (TCP - pentru ca un calculator sa vorbeasca cu alt calculator prin internet.) 
* express-session
* passport
	* Why many people don't recomand to use passport module ?
* passport-local
* session-file-store
* request  
* errorHandler );
* morgan 
* cookie-parser
* session
* colors
* cron
* debug
* express-error-handler

