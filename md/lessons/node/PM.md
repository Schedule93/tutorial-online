### [return main page](../../README.md) | [Chapter 12. Learn Node.js :](PAGE_NODE.md) 

### Package managers
* yarn
* npm
* scoop - for windows
* choco
* Homebrew
* curl
* yum

**npm** list commands :
1. `npm install` - Помогает вам установить зависимости для вашего проекта.
1. `npm init` - Используется для создания генерируемыва нового файла `package.json` вашего проекта.
1. `npm audit` - Запрашивает отчет об известных уязвимостях.
1. `npm update` - Помогает вам обновить установленные версии ваших зависимостей.
1. `npm uninstall` - Removes dependencies from both `package.json` and the `node_modules` directory. like an example `npm uninstall <package_name>`
1. `npm --version` - Показывает версию вашего NPM.
1. `npm run` - Помогает вам запустить сценарии, установленные в вашем `package.json`.
1. `npm start` - Запускает стартовый скрипт вашего проекта.
1. `npm publish` - Публикует ваш пакет в реестре NPM.
1. `npmmrc` - ?
1. `npm prefix` - ?
1. `npm --help` - ? 
1. `npm test` - ?
1. `npm help npm` - ?
1. `npm config` - ?
1. `npm diff` - ?
1. `npm repo` - ?
1. `npm whoami` - ?
1. `npm hook` - ?
1. `npm access` - ?
1. `npm explain` - ?
1. `npm ci` - ?
1. `npm completion` - ?
1. `npm adduser` - ?
1. `npm dedupe` - ?
1. `npm deprecate` - ?
1. `npm dist-tag` - ?
1. `npm doctor` - ?
1. `npm edit` - ?
1. `npm exec` - ?
1. `npm ` - Arată în consolă informația de bază.
1. `npm view` - Если вы хотите узнать о том, каков номер самой свежей версии некоего пакета, доступного в npm-репозитории, вам понадобится команда следующего вида: `npm view [package_name] version`. Example : { npm view express version }
1. `npm list` - Arată în consolă lista modulelor instalate în proiectul tău. De exemplu acum în proiectul meu node-server e instalat {body-parser, express, nodemon, pg, pug, readline}
1. `npm list -g` - Analogic cu npm list, doar că diferența este că npm list -g, arată lista globală a modulelor instalate.
1. `npm list express` Pentru a afla concret versiunea modului/packet de care ai nevoie, atunci scrie npm list name_package. Exemple { npm list body-parser, npm list morgan }  
1. `npm install <package>@<version>` **Установка старых версий npm-пакетов**. Иногда может понадобиться npm-пакет для решения проблем совместимости. Установить нужную версию пакета из npm можно, воспользовавшись следующей конструкцией:

**Yarn** list commands :
* `yarn add` - Adds a package to your existing package.
* `Yarn init` - Starts the package development process.
* `yarn install` - Installs all of the package's dependencies in the `package.json` file
* `yarn publish` - Sends a package to the package management system.
* `yarn remove` - Removes an unnecessary package from the current package.

### Package manager for Linux
* Pacman
* Portage