<details><summary>Click to expand</summary>

</details>![IMG LEARN TERMINAL](../../img/my-eyes-blinding.png)

### [return main page](../../README.md) | | Chapter 13. Learn Network : 
1. [Компоненты сети](NETWORK_COMPONENTS.md)
2. Сетевые протоколы
	* Правила комуникации
	* Сетевые протоколы и стандарты 
	* Передача данных в сети
3. Сетевой Доступ
	* Протоколы физического уровня
	* Сетевые средство потключение
	* Протоколы канального уровня
	* Управление доступом к среде(Топологии, кадр канала)
4. Ethernet
	* Протокол Ethernet(Кадр Ethernet / MAC-адреса)
	* Комутаторы LAN
	* ARP
5. Сетевой Уровень
	* Протоколы сетевого уровня
	* Маршутизация
6. IP-адресация
	* IPv4-адреса
	* IPv6-адреса
	* Проверка соединения(ICMP, ping, traceroute)
7. Разделение IP-сетей на подсети
8. [Транспортный уровень](TRANSPORT_LAYER.md)
9. Уровень приложение
	* Протоколы уровня приложения
	* Сервисы уровня приложение (DNS, DHCP, SMB, [FTP](https://en.m.wikipedia.org/wiki/File_Transfer_Protocol))
	* Поиск и управление неполодок в сети
	* Ping, traceroute, ipconfig
10. Концепция маршутизации
	* Статическая маршутизации,  динамическая
11. Комутируемые сети
12. Сети VLAN
13. Список контроля доступа (ACL, Firewall)
14. Преобразование NAT
	* Принып работы NAT
15. Тунели (IPSec)
16. SMPP
17. SIP

### See little later
* gRPC? - Это протокол удаленного вызова процедур (RPC).
* [Коммутатор](SWITCH.md)
* [Маршутизатор()
* [Swithcer()
* [Router](ROUTER.md) - Da internet la utilizatori fară a te conecta prin cablu.
* HTTP
* `SSH` это сетевой протокол управление удаленный операционый системы на вашем сервере 
* `SOCKET` - ?
* `PORT` - `netstat -a`, `netstat -n -b` => run like administration, тоесть нужна вессти пра адитнистратора. Как открыть порты? Как их закрыть?
* `IP` - ?
* [VPN](https://vc.ru/dev/66942-sozdaem-svoy-vpn-server-poshagovaya-instrukciya)
* `FTP/SFTP` - ?
* `SSL` - SSL (англ. Secure Sockets Layer — уровень защищённых сокетов) — криптографический протокол, который подразумевает более безопасную связь. ... SSL изначально разработан компанией Netscape Communications для добавления протокола HTTPS в свой веб-браузер Netscape Navigator. Link [Wiki](https://ru.wikipedia.org/wiki/SSL#%D0%92%D0%B5%D0%B1-%D1%81%D0%B0%D0%B9%D1%82%D1%8B)
* `TLS` - ?
* **Прокси сервер** : может применяться для решения следующих задач:
	* усиление безопасности
	* защита приватности
	* балансировка нагрузки на посещаемый ресурс. Можно посмотреть тут. [ПРОКСИ СЕРВЕР – ЧТО ЭТО, ВИДЫ И ЗАЧЕМ НУЖЕН?](https://wiki.merionet.ru/seti/23/proksi-server-chto-eto-vidy-i-zachem-nuzhen/)
* Проверка сети, решение междоменных проблем.

```Algorithms
* HTTP (на уровне протоколов)
* Базы данных (индексы, планирование запросов)
* Сети доставки контента (CDN)
* Работа с кэшем (LRU-кэш, memcached, Redis)
* Системы балансировки нагрузки
* Распределенные системы воркеров
```

### links
* [Протоколы](https://selectel.ru/blog/network-protocols/)
* [IBM DOCS Ru](https://www.ibm.com/docs/ru/aix/7.1?topic=protocol-tcpip-protocols)
* [Сети, шпаргалка](https://wiki.merionet.ru/seti/45/top-40-voprosov-na-sobesedovanii-it-specialistu/)
* [Wiki : Percent Coding](https://en.wikipedia.org/wiki/Percent-encoding) - это когда вы `url` adress ты видишь знак `?` и `%`, это get request. Например смотри [тут](https://en.ryte.com/wiki/GET_Parameter#:~:text=GET%20request%5Bedit%5D,%2Dcalled%20name%2Dvalue%20pairs.)
* [http-status wiki](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
* [Wiki Communication protocol](https://en.wikipedia.org/wiki/Communication_protocol)
* [Wiki List of network protocols OSI model](https://en.wikipedia.org/wiki/List_of_network_protocols_(OSI_model))
* Protocols : [TCP](http://www.magister-stylus.narod.ru/TCPserver.html) and [wiki](https://en.wikipedia.org/wiki/Lists_of_network_protocols)
* [Основы компьютерных сетей. Тема №1. Основные сетевые термины и сетевые модели](https://habr.com/ru/post/307252/)
* [0.Видео уроки Cisco Packet Tracer. Курс молодого бойца. Введени](https://www.youtube.com/watch?v=voGkaUXFw-I)
* [Основы компьютерных сетей. Тема №2. Протоколы верхнего уровня](https://habr.com/ru/post/307714/)
* [Cisco часть I, урок 19. Отказоустойчивое подключение к провайдеру (CISCO)](https://www.youtube.com/watch?v=KBugCTsxbjs&list=PLvGOckMhKg76rMzrows3RqNBmQeeBK-sn)
* [Курс MIT «Безопасность компьютерных систем». Лекция 16: «Атаки через побочный канал», часть 2 ](https://habr.com/ru/company/ua-hosting/blog/429392/)
* [Таблица сетевых протоколов по функциональному назначению](https://ru.m.wikipedia.org/wiki/%D0%A2%D0%B0%D0%B1%D0%BB%D0%B8%D1%86%D0%B0_%D1%81%D0%B5%D1%82%D0%B5%D0%B2%D1%8B%D1%85_%D0%BF%D1%80%D0%BE%D1%82%D0%BE%D0%BA%D0%BE%D0%BB%D0%BE%D0%B2_%D0%BF%D0%BE_%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%BC%D1%83_%D0%BD%D0%B0%D0%B7%D0%BD%D0%B0%D1%87%D0%B5%D0%BD%D0%B8%D1%8E)
* [Fast CGI](https://ru.wikipedia.org/wiki/FastCGI)
* [http status](https://www.w3schools.com/tags/ref_httpmessages.asp)
* [Newtorks Youtube Russian](https://www.youtube.com/watch?v=OLFA0soYGhw&list=PLtPJ9lKvJ4oiNMvYbOzCmWy6cRzYAh9B1)
* [УПРАВЛЕНИЕ КОМПЬЮТЕРНОЙ СЕТЬЮ](http://inftis.narod.ru/adm/ais-n4.htm)
* [Stack Overflow : CRC32](https://stackoverflow.com/questions/tagged/crc32)
* [20 Windows Commands](https://www.makeuseof.com/tag/15-cmd-commands-every-windows-user-know/)
* [???](https://www.opennet.ru/docs/RUS/bash_scripting_guide/x8707.html)

