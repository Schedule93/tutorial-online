![image](../../img/girl_and_cup.png)

### [return main page](../../README.md) | Chapter 14. Learn Algorithms and structures data : 

### Algorithms and Structure Data
* [Big O](#)
* Оптимицация
	* [Жатый алгоритм](https://ru.wikipedia.org/wiki/%D0%96%D0%B0%D0%B4%D0%BD%D1%8B%D0%B9_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC)
* [Деревя](#)
* [Функции](https://en.wikipedia.org/wiki/Function_composition)
* [Сортировка]()
* Пойск
* Связанный список
* Строки
* [Backstrack](https://www.google.com/search?q=backtracking&oq=backtracking&aqs=chrome..69i57j0l3.5587j0j7&client=ms-android-xiaomi-rev1&sourceid=chrome-mobile&ie=UTF-8)
* [Генетический алгоритм](https://www.youtube.com/watch?v=SfEZSyvbj2w)* 
* Understanding of basic data structures (arrays, linked lists, trees, queues, stacks, hash maps, etc.)
* Алгори́тм Де́йкстры — алгоритм на графах, изобретённый нидерландским учёным Эдсгером Дейкстрой в 1959 году. Находит кратчайшие пути от одной из вершин графа до всех остальных. Алгоритм работает только для графов без рёбер отрицательного веса. (Этот алгоритм нужен для сетей)
* Criptare - Шифр Виженера
* Преобразование Фурье

### Read Books
* Алгоритмы. Построение и анализ. Т.Кормен, Ч.Лейзерсон и др.

### Links algorithm in js
* [TheAlgorithms/C-Sharp](https://github.com/TheAlgorithms/C-Sharp)
* [Advance : Dictionary of Algorithms and Data Structures](https://xlinux.nist.gov/dads/)
* [Advance : All Algorithms](https://allalgorithms.com/docs/dbscan)
* [Beginner : JS GitHub](https://github.com/dmitrymorozoff/algorithms-in-javascript)
* [Beginner Essential JS interview question](https://github.com/ganqqwerty/123-Essential-JavaScript-Interview-Questions)
* [Beginner : js-interview-questions](https://github.com/sudheerj/javascript-interview-questions)
* [Beginner : front-end-interviewk](https://github.com/yangshun/front-end-interview-handbook/blob/master/questions/javascript-questions.md#explain-event-delegation)
* [Avance : Awesome Interviews](https://github.com/MaximAbramchuck/awesome-interview-questions#javascript)
* [Введение в алгоритмы](https://www.youtube.com/watch?v=8JlTwMg1dyw&list=PLwwk4BHih4fjIT5cT4i1s93b99aJScUGB)
* [Алгоритмы и структуры данных
простыми словами](https://codonaft.com/%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D1%8B-%D0%B8-%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%82%D1%83%D1%80%D1%8B-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85-%D0%BF%D1%80%D0%BE%D1%81%D1%82%D1%8B%D0%BC%D0%B8-%D1%81%D0%BB%D0%BE%D0%B2%D0%B0%D0%BC%D0%B8/?utm_source=youtube&utm_medium=description&utm_campaign=social)
* [Парадигмы программирования](https://inf1.info/programmingparadigm)
* [Tutorialpoint](https://www.tutorialspoint.com/data_structures_algorithms/data_structures_algorithms_online_test.htm)
* [ProPfrofs](https://www.proprofs.com/quiz-school/story.php?title=data-structures)
* [Wiki Algorithm : Big O notation](https://en.wikipedia.org/wiki/Big_O_notation)