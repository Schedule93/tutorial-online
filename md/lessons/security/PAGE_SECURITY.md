![image](../../img/bleach-purple-hair.jpg)

### [return main page](../../README.md) | [Chapter 31. Learn Information Security](PAGE_SECURITY.md)

### Security Information
Для начала есть такая професия `Security Architect`. Так вот! Ниже несколько советов для зашиты вашего сайта:
* **Note 1** Вы должны исходить из того, что по умолчанию все НЕ безопасно. Уязвимости есть во всех уголках. 
* **Note 2** Например, только со страницы входа злоумышленник может попробовать несколько вещей. Они могут выдавать себя за другого пользователя или внедрять вредоносный код в поля ввода, чтобы вызвать ответ.
* **Note 3** Мы должны разработать надежные протоколы аутентификации и авторизации.
* **Note 4** Важно понимать основные понятия и терминологию кибербезопасности

### Topics :
* [Типы уязвимостей](VULNERABILITIES.md)
* Межсайтовый скриптинг (XSS); - XSS
* Ошибки буфера CWE-119 | Проверка ввода CWE-20 | CWE-200 Information Leak / Disclosure
* Кибербезопасность
* [Protocols](PROTOCOLS.md)
* Authorisation
* Network
* Password
* Encription {3DES является устаревшим шифровальным шифром, поскольку он был взломан в 2017 году.}
* Certificates
* Web-App
* Malware
* Hash
* Linux
* Privacy
* Email
* Web-Browser
* [Security for Node.js](#)
* [Wi-Fi Security](#)
* [VPN](#)
* [Virus](#)
* [Data Breaches]()
* System Glitches
* Penetration Testing - также известное как этический взлом, требует глубокого понимания таких тем, как `архитектура компьютера` и `операционные системы`, `бизнес-операции`, `сетевые протоколы` и `языки сценариев`.
* Оценки рисков и уязвимостей
* Отказ в обслуживании
* Кибератак { The Sweet32 attack }
* Сжатия || сжатия и шифрования

### Links
* [Information Security](https://security.stackexchange.com/tags)
* [CWE](https://cwe.mitre.org/about/)
* [Wiki : ](https://en.wikipedia.org/wiki/Communication_protocol)
* [Wiki : Протокол передачи данных](https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D1%82%D0%BE%D0%BA%D0%BE%D0%BB_%D0%BF%D0%B5%D1%80%D0%B5%D0%B4%D0%B0%D1%87%D0%B8_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85)
* [IBM Docs Protocols Ru](https://www.ibm.com/docs/ru/aix/7.1?topic=protocol-tcpip-protocols)
