### Topic : Tag Table.

th - ?
tr - merge pe verticală, sus-jos
td - merge pe orizontală, dreapta-stinga sau invers nu conteza.
thread - ?

* mai jos se arata ca tagul tr pune rinduri adica sun 2 rinduri.

```html
<style>
	table, tr, td, th { width: 400px; height: auto; border: 1px solid black; }
</style>

<table id="name-at-topic">
	<tr>
		<td>unu</td>
		<td>doi</td>
		<td>unu</td>
		<td>doi</td>
	</tr>
	<tr>
		<td>unu</td>
		<td>doi</td>
		<td>unu</td>
		<td>doi</td>
	</tr>
</table>
```