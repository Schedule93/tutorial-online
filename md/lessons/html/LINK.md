### [return main page](../../README.md) | [Chapter 2. Learn HTML](PAGE_HTML.md)

The tag `a` has atributes:

* `target` Открывает внешнюю страницу. Ну это когда вместо нажатие на правом курсоре мышки вы видите натпись открыть в новом вклатке. Только это открывает сразу без танцев с бумном.  :)

```HTML
<a id="footer" href="http://freecatphotoapp.com" target="_blank">cat photos</a>
```

* `title` Подсказка для ссылки
* `alt`

когда нам нужно перейти на ту же страницу только ниже или выше.

```html
<a href="#example">be more good</a>

// пишем где мы хотим чтобы был прокрута

<a name="example">be more good</a> 
```