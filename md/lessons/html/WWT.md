### [return main page](../../README.md) | [Chapter 2. Learn HTML](PAGE_HTML.md)

### Work with text. Bellow is list with tags.

* `em` : Tilt the text to one side.
* `b` : Bold text.
* `strike` : Cut the text and show him. 
* `abbr` : Shows a hint when you place the mouse cursor over the text. 
* `strong` : Highlighted text.
* `h1 - h6` : Header. From 1 to 6.
* `em` - analogic italic text.
* `p` : The tag 'p' means paragraph.
* `u` : The tag 'u' means underline.
* `small` : The text will be more small than normal text.
* `big` : The text will be more bigger than normal text.
* `pre` : The tag 'pre' allows you to make more spaces between words.
* `blockquote` : It is used when writing quotes.
* `q` : anallogic 'tag' `blockquoate`.
* `br` : Brike line.
* `center` : Any object will be align in center.

Atribute at tags

`cite` : Shows where you took information.
`align` : `right`, `left`, `bottom`, `top`, `center`, `fustify`

```html
	<style>
		.design {
			background: rgb(250, 220, 220);
			width: 800px;
			height: 20px;
			padding: 5px 0px 5px 10px;
			margin: auto;
		}
	</style>

	<br />

	<p class="design">
		<em>Here is the tag em. And I want to see what made.</em>
	</p><br />

	<p class="design">
		<abbr title="put the mouse cursor to see hint."> put the mouse cursor to see hint.</abbr>
	</p><br />

	<p class="design" align="left" cite="https://naruto.fandom.com/wiki/Naruto_Uzumaki">
		Naruto was born on the night of October 10th to Minato Namikaze and Kushina Uzumak
	</p><br />

	<hr />
```

### Addition information

