### [return main page](../../README.md) | [Chapter 2. Learn HTML](PAGE_HTML.md)

### details

```html
	<details open="open">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit beatae praesentium ullam, repellat incidunt sint distinctio corrupti aut facere facilis nisi soluta laudantium sapiente labore atque voluptatem a veniam totam adipisci velit quam maiores, sit ut. Nihil eius vero esse sit excepturi doloribus tenetur repellendus voluptatibus in, minima, quis praesentium, animi iusto facere sint delectus neque cupiditate dolore dolor minus ullam! Quam temporibus exercitationem quibusdam itaque, consectetur corporis obcaecati accusantium sed dignissimos consequuntur, provident, culpa ad molestias doloribus ea, deleniti! Suscipit numquam libero quam provident aut ea ipsa expedita, repellendus rem tempora nihil sit tempore! Repellendus, eveniet velit harum consequuntur.
	</details>
```