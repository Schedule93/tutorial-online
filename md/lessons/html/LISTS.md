### [return main page](../../README.md) | [Chapter 2. Learn HTML](PAGE_HTML.md)

### Lists ol and ul in JavaScript
* `ol` - numbered list are called **Ordered lists**
* `ul` - list with point are called **unordered list**

### Atributes ul/ol :

* type="1"	The list items will be numbered with numbers (default)
* type="A"	The list items will be numbered with uppercase letters
* type="a"	The list items will be numbered with lowercase letters
* type="I"	The list items will be numbered with uppercase roman numbers
* type="i"	The list items will be numbered with lowercase roman numbers

```html
	<ol type="1">
		<li>dog</li>
		<li>cat</li>
		<li>eagle</li>
	</ol> <br />
	
	<ol type="A">
		<li>good</li>
		<li>normal</li>
		<li>bad</li>
	</ol> <br />
	
	<ol type="a">
		<li>red</li>
		<li>lilak</li>
		<li>orange</li>
	</ol>

	<ol type="I">
		<li>Coffee</li>
		<li>Tea</li>
		<li>Milk</li>
	</ol>

	<ol type="i">
		<li>Coffee</li>
		<li>Tea</li>
		<li>Milk</li>
	</ol>
```

* `reverseed` - begins to count from the end. Used only for tag `ol`. Example:

```html
	<ol reversed>
		<li>one</li>
		<li>two</li>
		<li>three</li>
	</ol>
```

* `start` - starts counting from the desired number, not necessarily from scratch or one.

```html
<ol start="14">
	<li>apple</li>
	<li>quince</li>
</ol>
```

* `style` - property is used to define the style of the list item marker.There are use 4 types, `list-style-type:none;`, `list-style-type:square`, `list-style-type:disc;`, `list-style-type:circle;`, `list-style-type:square;`

```html
<ul style="list-style-type:disc;">
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
</ul>

<ul style="list-style-type:circle;">
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
</ul>

<ul style="list-style-type:square;">
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ul>

<ul style="list-style-type:square;">
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
</ul>

<ul style="list-style-type:none;">
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ul>
```

### Additionaly Information:

If you want horizontal list below is example how to do:

```html
	<style>	
		ul {
			display: flex;
			flex-direction: row;
			align-items: center;
		}

		.see {
			background: #eee;
			padding: 20px;
			list-style: none;
		}

	</style>
	
	<ul>
		<li class="see">I</li>
		<li class="see">you</li>
		<li class="see">we</li>
		<li class="see">they</li>
		<li class="see">them</li>
	</ul>
```

tag `nav` you must use with `ol` or `ul` because it will show a good structure.  

```
<nav id="" class="">
	<ol class="">
    	<li class="item"><a href="">Home</a></li>
    	<li class="item"><a href="#">Forum</a></li>
    	<li class="item"><a href="#">Catalog</a></li>
    	<li class="item"><a href="#">Tutorial</a></li>
    	<li class="item"><a href="#">Info</a></li>
  	</ol>
</nav>
```

* list with image

```html
	<style>
		ul {
			list-style-image: url('favicon.ico');
		}
	</style>
	<nav>
		<ul>
			<li>home</li>
			<li>home</li>
			<li>home</li>
			<li>home</li>
			<li>home</li>
		</ul>
	</nav>
```

```css
body {
	background: #000;
}
```

### YouTube Links :
* [CSS : list item](https://www.youtube.com/watch?v=fBI-p8ekWlI&list=PLQqEY2kzSbZ7_9iFe_d7bc_uFR4MpF0UL&index=3&t=19s)

### Extern links :
* [W3school](https://www.w3schools.com/html/html_lists.asp)