### return main page

### Chapter 2. HTML | Topic Angle Tag.
* `<angle>` - Se folosește pentru a învîrti ceva, spre exemplu un pătrat. Analogic cu ctrl + T din Adobe Photoshop cînd vrei să învîrți ceva. Deseori întîlnim cazuri in `degrees`, `gradians`, `radians`.  For example, in `<gradient>` s and in some `transform` functions.

### Extern Links :
* [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/CSS/angle)
