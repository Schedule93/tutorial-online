### [return main page](../../README.md) | [Chapter 2. Learn HTML](PAGE_HTML.md)

### Difference between tags :
* `span`, `div`, `section`, `article`,  `nav`, `aside`

### Tag span
* tag `span` provides no visual change by itself.
* the span can correspond to other elements so like:

```html
<p>
	I have <span style="color: green"><strong>dark green</strong></span> eyes. but my older brother has <span style="color:blue;">blue</span> eyes.
</p>
```

### Tag div
* Самый простой способ создать блок - использовать `<div>` с `css`.

```html
<div class="wallpaper"></div>

<style>
	.wallpaper {
		background: purple;
		width: 200px;
		height: 50px;
		border: 1px solid #000;
	}
</style>
```

### Tag section and article

`section` - Показывает лишь мини описание статьи. А тег `article` уже показывает всю статью целиком. Section подходит для таких элементов страницы, как элементы списка новостей, элементы списка товаров в каталоге. Так же section может использоваться для разбиения текста на части. Например главы. Использование section как аналог article - неправильно и противоречит спецификации. (Т.е. проще говоря section это аналог тега li только для блоков страницы). article — статья, section — кусок этой статьи или любой кусок некоторого целого материала.

##### Rules :
* Каждый тег `<article>` должен иметь заголовок от `h1` до `h6` как дочерний элемент `<article>`.
* Тег `<article>` может быть вложыным друг в друга. Как например когда мы отвечаем к комментарии кому нибудь. [late](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/article)

### Additionaly information

* [html.spec.whatwg.org](https://html.spec.whatwg.org/multipage/sections.html#the-article-element)