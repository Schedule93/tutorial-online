un![IMAGE LEARN HTML5](../../img/anime-babies.png)

### [return main page](../../README.md) | Chapter 2. Learn HTML :

### Bellow is list with HTML topics : 
* [List ol and ul](LISTS.md)
* [Types of blocks](SPANORDIV.md)
* [Details](DETAILS.md)
* [Work with text](WWT.md)
* [Links](LINK.md)
* [Symbol w3school](https://www.w3schools.com/html/html_entities.asp)
* [Symbol 2 w3school](https://www.w3schools.com/html/html_symbols.asp)
* [Response w3school](https://www.w3schools.com/html/html_responsive.asp)
* [Form](FORM.md)
* [Table](TABLE.md)
* [tag pre](#)
* [tag code](#)
* [Pattern](PATTERN.md)
* [Self Closing Tag](SELF_CLOSING_TAG.md)

### Graphics
* Canvas
* SVG

### Media
* Audio
* Video

### Tags
* [figure](FIGURE.md)
* [mark](#)
* [Angle](ANGLE.md)

### YouTube Links :
* [HTML5 уроки для начинающих #12](https://www.youtube.com/watch?v=SHRC_rzVfBs&list=PL0lO_mIqDDFUpe6yMyXAlcrfT6AO0KW1a&index=12)

### Extern links :
* [Htmlbookru](http://htmlbook.ru/html)
* [Htmlnet](http://html.net/)
* [Quackit](https://www.quackit.com/)
* [Templates](https://www.wix.com/website/templates/html/blog)
