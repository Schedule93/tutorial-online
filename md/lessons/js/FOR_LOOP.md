### [return main page](../../../README.md) | [Chapter 19. Learn Operating Systems](../page-main-os/PAGE_OS.md) :

### For Loop :

* Count how many times is each letter repeated from argument

```js
function play(argument) {
	var count = 0;
	argument = argument.toLowerCase();
	for(var letter = 0; letter < argument.length; letter++) {
		if(argument[letter] === "s") {
			count += 1;
		}
	}

	return count;
}

console.log( play("Here is a simple text for : Schedule") );
```


* cel mai simplu `for loop` 

```js
for(var i = 10; i > 0; i--) {
	console.log(i);
}
```

* peredaiom danaie po kushocikam

```js
var arr = [];
var each = 0;
for (each of arr) {
	return 0;
} 
```

* loop for objects.

```js
for (elem in obj) {

}
```

* Show each letter from sentence which equal with vowel

```js
function for_loop (sentence) {
    var vowel = "aeouiAEOUI";
    for(var i = 0; i < sentence.length; i++) {
        for(var j = 0; j < vowel.length; j++) {
            if(sentence[i] === vowel[j]) {
                console.log(sentence[i]);
            }
        }
    }
}
```

* Two for loops in one statement. 

```js
function twoForLoopsInOneStatement () {
    for (var i = 0, j = 0; i <= 10 && j <= 10; i++, j++) {
      console.log(i+j);
    }
}
```

Un fel de fizzbuzz

```js

function is_even(number) {
    if (number % 2 === 0) {
        return 1;
    } else {
        return 0;
    }
}

for (let i = 0; i <= 30; i++) {
    if (is_even(i) === 1) {
        console.log(i + " even" + "\n");
    } else {
        console.log(i + " odd" + "\n");
    }
}

is_even();
```

* What is this ?

```js
function func(board) {
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            if (board[i][j] === null) return [i, j];
        }
    }
}

function func(board) {
    for (let i = 0; i < 9; i++) {
        const j = board[i].indexOf(null);
        if (j != -1) return [i, j];
    }
}
```

* Fii atent la parametre și la chemarea programei în `console.log();`. Această programă pur și simplu arată numărul elementului la `name_at_item`, pe ce poziție stă am învidere. Cu alte cuvinte găsirea cuvîntului de care ai nevoie.

```js
var arr = ['apple', 'banana', 'cherry', 'donuts', 'eggplant', 'french fries', 'goulash', 'hamburger', 'ice cream', 'juice', 'kebab', 'lemonade', 'mango', 'nuts', 'octopus', 'parsley', 'quail egg', 'risotto', 'stew', 'tapas', 'udon', 'vanilla', 'wheat', 'xylotil', 'yogurt', 'zucchinni'];

function find_number_index_at_item(number_at_index, name_at_item) {
    for (var i = 0; i < number_at_index.length; i++) {
        if(number_at_index[i] === name_at_item) {
          return i;
        }
    }

    return -1;
}

console.log(find_number_the_index_at_item(arr, "kebab"));
```

### Links :
* [Nesting For Loops javascript](https://forum.freecodecamp.org/t/nesting-for-loops-javascript/204084)
* link 1[CodeAcademy](https://discuss.codecademy.com/t/javascript-for-loops-inside-for-loops/275591), link 2[CodeAcademy](https://discuss.codecademy.com/t/javascript-for-loops-inside-for-loops/275591/16)