### Topic : Method join();

1. Возвращает массив в виде строки.
2. Элементы будут разделены
3. создает и возвращает новую строку.
4. объединяет все элементы в массиве 

```js
const elements = ['Fire', 'Air', 'Water'];

console.log(elements.join());
// expected output: "Fire,Air,Water"

console.log(elements.join(''));
// expected output: "FireAirWater"

console.log(elements.join('-'));
// expected output: "Fire-Air-Water"
```