### [return main page](../../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)
Анонимные функции это однострочные функции, которые используются в случаях, когда вам не нужно повторно использовать функцию в программе. Они идентичны обыкновенным функциям и повторяют их поведение.

### Exemple of Code :
```js
console.log("Here will be anonymos functions. ");
```

### Extern Video :
* Video 1 
* Video 2

### Extern Links :
* Link 1
