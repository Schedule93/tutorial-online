### [return main page](../../../README.md) | [Chapter 4 JavaScript](PAGE_JS.md)

### CharAt(); meth

* ???

```js
var obj={}
var repeats=[];
str='banana'

for(x = 0, length = str.length; x < length; x++) {
    var l = str.charAt(x)
    obj[l] = (isNaN(obj[l]) ? 1 : obj[l] + 1);
}

console.log(obj);
```

* ???

```js
console.log("nothing");
```