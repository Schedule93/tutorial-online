### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Write tests

* Test 1

```js
// Examle 1
function separateOptions(settings) {
    const options = {
      playerVars: {}
    };
    const optionParams = ["width", "height", "videoid", "host"];

    const myStringArray = ["param", "value"];
    const settingsEntries = Object.entries(settings);
  
    for (let i = 0; i < settingsEntries.length; i++) {
      for (i; i < myStringArray; i++) {

        if (optionParams.includes(param)) {
          options[param] = value[i][0];
        } else {
          options.playerVars[param] = value[i][1];
        }
      }
    }
    return options;
  }

// Examle 2 : все, что я здесь сделал, это сохранил результат Object.entries
let entries = Object.entries(settings);
for (let i = 0; i < entries.length; i++) {
  let [param, value] = entries[i];
  if (option_params.includes(param)) {
    options[param] = value;
  } else {
    options.player_vars[param] = value;
  }
}
```

* **Test 2**

```js
console.log(2);
```

* **Test 3**

```js
console.log(3);
```


* Test 4

```js
function loadScript(src, callback) {
            let script = document.createElement('script');
            script.src = src;

            script.onload = () => callback(script);

            document.head.append(script);
        }

        loadScript('https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.2.0/lodash.js', script => {
            alert(`Cool, the script ${script.src} is loaded`);
            alert( _ ); // function declared in the loaded script
        });
```

* **Test 5**. you'd probably want to give it some data, unless your handler can get all the data it needs from the history api ... To add more data to the event object, the CustomEvent interface exists and the detail property can be used to pass custom data.
```js
const event = new Event('stateChanged', {qs: query_string_object});
// this is the correct way to pass data, right?
// but why I can't access the qs from the handler
 window.addEventListener('stateChanged', (e) => {
        console.log(e.qs)
 });
```

* ?


```js
 var day = new Date();
  var n = day.getUTCDay();
  //Monday(0), Tuesday(1), Wednesday(2), Thursday(3), Friday(4), Saturday(5), Sunday(6)

  if (n === 4){
   alert("Our hours for Thursday are 9-5");
}
```