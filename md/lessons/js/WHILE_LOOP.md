### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### While Loop

* Simple while loop. 

```javascript
function wwhile_loop_1 {
  let number = 1;
  while(number < 11) {
    console.log(number);  number++;
  }
}

function while_loop_2 {
  let number = 11;
  while(number > 0) {
    console.log(number);  number--;
  }
}

function while_loop_3() {
	var i = 0;
	while(i <= 10) {
		console.log(a); a++;	
	}
}
```

* Show each letter in new line from `arg`.  

```js
// Wrong! ...
function example(arg) {
  var i = 0;
  while(i < arg.length) {
    console.log(arg[i]);
    i++;
  }
}
```

* Show each letter from variable `vowel` in new line.

```js
function while_loop() {
  var vowel = "aeouiAEOUI";
  var cout = 0;
  while (cout < vowel.length) {
    console.log(vowel[cout]);
    cout++;
  }
}
```

* Stack Overflow : [Nested while loops in javascript](https://stackoverflow.com/questions/36375261/nested-while-loops-in-javascript)

```js
var m, n;
m = 1;
while(m <= 5) {
    n = 1;
    while(n <= 10) {
        console.log("*" + " ");
        n++;
    }
    console.log("<br>");
    m++;
}
```

* Show all numbers which is more than 10;

```js
function example_1() {
  var arr = [1, 12, 5, 13, 21, 2, 3, 7];
  var count = 0;
  while(count < arr.length) {
    if(arr[count] >  10) {
      console.log(arr[count]);
    }
    count++; 
  }
}
```

* Show each letter from sentence which equal with vowel

```js
function show_only(sentence) {
  var vowel = "aeouiAEOUI";
  var i = 0;

  while (i < sentence.length) {
    var j = 0;
    while (j < vowel.length) {
      // console.log("i=" + i + " j=" +j);
      if (sentence[i] === vowel[j]) {
        console.log(sentence[i]);
      }

      j++;
    }
    i++;
  }
}
```

* sss

```js
// wrong the program
function while_loop_play(sentence) {
  var vowel = "aeouiAEOUI";
  var i = 0;
  var j = 0;

  while(i < sentence.length) {
    i++;
    while(sentence[i] === vowel[j]) {
      console.log(sentence[i]);
      j++;
    }
  }
}
```

* sssss

```js
function while_loop_play(sentence) {
  var vowel = "aeouiAEOUI";
  var i = 0;
  var j = 0;

  while(i < sentence.length) {
    i++;
    while(sentence[i] === vowel[j]) {
      console.log(sentence[i]);
      j++;
    }
  }
}
```

* infinite wwhile loop

```js
function infiniteWhileLoop () {
    var count = 1;
    while(count) {
        console.log(count), count++;
    }
}
```

* Găsirea elementului de care ai nevoie. Așa programă este realizată cu ajutorul la `for loop` și metoda `indexOf();` Inclusiv aceasta cu `while loop`.

```js
var arr = ['apple', 'banana', 'cherry', 'donuts', 'eggplant', 'french fries', 'goulash', 'hamburger', 'ice cream', 'juice', 'kebab', 'lemonade', 'mango', 'nuts', 'octopus', 'parsley', 'quail egg', 'risotto', 'stew', 'tapas', 'udon', 'vanilla', 'wheat', 'xylotil', 'yogurt', 'zucchinni'];


function find_number_the_index_at_item(array, item) {
  var i = 0;
  while (i < array.length) {
      if (array[i] === item) {
          return i;
        }
        i += 1;
    }
    return -1;
}

console.log(find_number_the_index_at_item(arr, 'donuts'));
```

**note**

* for in : When work with objects or json. Be careful : Он имеет побочные эффекты
* `for of` - ?
* Siimplu `for loop` nu lucreză cu tip de date `object` sau `array`. Cu `for` și `while` folosești un index ca sa accesezi elementul din array
* `while loop` : se foloseshte atunci cind noi nu stim de cite ori trebuie sa ciclam. De exxemplu avem o cutie mica de carti IU GI OH. Presupunem ca in ea se afla 100 carti/cartonashe. Shi noi trebuie sa gasim un cartonash anume `n`. Si cu ajutorul la while loop spunem scoate fiecare cartonash pana cand il gasesti. In asa caz avem nevoe de while loop.
* `do while` - indiferent de condiție ciclu `do while` el va executa macar o dată ciclul înăuntru la `do {}` .
* forEach - pentru array. El e asemănător cu while. Foreach îți dă elementul 

### Links :

* [Stack Overflow : Nested while loops in javascript](https://stackoverflow.com/questions/36375261/nested-while-loops-in-javascript)
