### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Method : math.sqrt();

* Metoda `math.sqrt()` întoarce radacina patrată dintr-un numar.
