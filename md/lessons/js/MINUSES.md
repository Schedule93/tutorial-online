### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Minuses in JavaScript
* `Schimbarea Rapidă` JavaScript inclusiv Node.js se schimbă într-un temp foarte rapid. Problema constitue nu că aparte versiuni în fiecare an la Node.js de exemplu, dar că nu dovedim noi programatorii să învășăm toate functiile dintr-un modul. Dar asta numai dintr-un unu. Dar cîte sunt ... Ooo... Multe! Cel putin vreo 30 de module unde in fiecare modul sunt cite 100 de functii care trebuie invatate de programator
* `Performanța`. Comparativ cu C++ performanța e cu mult mai slabă. De aceaia propun și chiar mă gîndesc că ar fii bine să avem un compilator în lumea javascript așa ca `gcc compiler`. Cu toate acestea chiar mă gîndesc la ideaia să creez un compilator pentru js unde o să aibă tipizare ca în C++.
* `tipizare dinamică`?
* `V8.JS` Personal consider că motorul V8 trebuie să fie scris în C++ nu JS.
