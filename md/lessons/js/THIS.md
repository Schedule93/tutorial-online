### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Область видимость. This :
* **1. Глобальный контекст** - ?
```js
// result will be: Undefinedd, why ?
var a = global.this;

// result will be: {}, why ?
var b = this;
```

* **2. Конттекст выполнения функции** - ?
```js
// will be true. 🤷‍♀️
function try_this() {
	console.log(this === global);
}; try_this();

// will be true. 🤷‍♀️
let func_expresion = function() {
	console.log(this === global);
}; func_expresion();

// JavaScript был бы не javascript, если и тут не было нюансов :

// will be: undefined
function try_this_2() {
	'use strict'
	console.log(this);
}; try_this_2()

// will be: undefined
let func_expresion_2 = function() {
	'use strict'
	console.log(this);
}; func_expresion_2();

// Можно конечно 'use strict' написать в начале файла, эффект тот же.
```

* **3. Контекст выполнение функции eval** - ?
```js
console.log("soon will be ...");
```

А теперь давайте расмотрим кажый пункт по одельности :
```js
console.log("soon will be ...");
```

### Функции конструкторы :
* Abia am inceput ...
```js
// De ce Main e scris cu literă mare ? Constructor ? 🤷‍♂️
function Main() {
	function Login(id, user, password) {
		this.id = id,
		this.user = user,
		this.password = password
		this.recorded = function(){
			return "Dear user : " + this.user + "you successed registered. ";
	}
}

	// What is more below? I don't understand :
	var nicolai = new Login(1, "nicolai", "a12345b");
	var alex = new Login(2, "alex", "123");

	// return result :
	console.log(alex);
}

// show result :
Main();
```

### Вызов функции как метод обьекта :
* Эту программу надо редактировать :
```js
var hi = function() {
	return "Hi! " + this.name + ". ";
}

// check this :
let person_1 = {
	"name": "Ion",
	"hi": hi
}


let person_2 = {
	"name": "Alex",
	"hi": hi
}

// result will be: { name: 'Alex', hi: [Function: hi] }
console.log(person_2);
```

### Вызов функции как метода прототипа обьекта :
```js
console.log("soon will be ...");
```

### Вызов функции с помощью метода call и apply :
* bind
* apply
* call

* ?
```js
f.call(null);

function f() {
  console.log(this);
}
```

### ### Вызов функции как обрабочика события DOM :
* Soon will be ...
```js
console.log("soon will be ...");
```

### IIFE - Immediately invoked function expression :
Они же - самовызывающиеся функции.
```js
(function(){
  console.log(this === window)
})()
```

### Как теряется контекст ?
* Так как ? 
```js
console.log("soon will be ...");
```

### Исправление ошибок привязки значения к this :
Были у нас способы привязки значения к this с помощью методов прототипа Function call и apply, но в нашей ситуации они нам не очень помогут (вообще не помогут). Мы не можем сразу вызвать функцию callback, ведь она ещё не дождалась своего часа.

Но в современном Javascript есть способы справиться с ситуацией.

### Привязка значения к this с помощью Function.prototype.bind()
Метод этот пришел вместе с ES5, и в отличие от своих соратников call и apply, он не вызывает функцию. В результате своего исполнения он возвращает новую функцию, которая при выполнении будет устанавливать в значение this то, что мы передали в качестве первого аргумента.

Применяем на практике :
```js
class Hero {    
  constructor(heroName) {
    this.heroName = heroName || 'Default'
    // Получить список каждого элемента в документе
    let elements = document.getElementsByTagName('button'); 

    // Добавить callback как обработчика кликов       
    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', this.log.bind(this))
    }    
  }    
  
  log() {
    console.log(this.heroName);
  }
}

const batman = new Hero('Batman')
```
Жмём на нашу изящную кнопку и смотрим в консоль.

### Arrow functions и их отношения с контекстом :
А вот уже ES6 представила нам новую возможность борьбы за **this** Этим способом стали стрелочные функции. Все дело в том, что в отличие от обычных, они не создают собственного контекста. Они наследуют контекст, в котором были созданы, а вместе с этим у них отсутствует своё привязывание значения к **this. Где бы и как они не вызывались, это никак не влияет на значение this.**

Смотрим как это выглядит на практике :
```js
class Hero {
  constructor(heroName) {
    this.heroName = heroName || 'Default'
    // Получить список каждого элемента в документе
    let elements = document.getElementsByTagName('button');
    // Добавить callback как обработчика кликов
    for (let i = 0; i < elements.length; i++) { 
      elements[i].addEventListener('click', this.log)
    }    
  }
  
  log = () => {
    console.log(this.heroName);    
  }
}

const batman = new Hero('Batman')
```

### Extern links :
* [Хабра - Javascript: базовые вопросы и понятия для самых маленьких - (Автор:{OkoloWEB
})](https://habr.com/ru/companies/lanit/articles/718130/)





