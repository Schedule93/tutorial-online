### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Types of functions :
* [Асинхронные функции](ASYNC_FUNCS.md)
* [Анонимные функций](ANONYMOUS_FUNCTIONS.md)
* [Стрелычные фунуции](ARROW_FUNCS.MD)
* [Рекурсивные функции](#)
* [Лямбда выражения](LAMDA_EXPRESSIONS.md)
* [Callbacks functions](CALLBACK.md)
* [Objects inside callback](#)
* [Call Stack](CALL_STACK.md)

### Introduction
Ce știu eu despre funcții? Ele pot transmise cu ajutorul altor funcții sau pot fi ca parametru. Mai scurt ele trebuiesc pentru a nu aștepta o acțiune. Mai ales cînd vine vorba de citirea datelor sau scrierea lor.

### Various ways to declare functions
```javascript
// function declaration
function A(){};

// functional expression
var B = function(){};

// functional expression with grouping operators
var C = (function(){});

// named functional expression
var D = function foo(){}; 

// immediately executable functional expression (IIFE), which returns a function
var E = (function(){ 
    return function(){}
})();

// function constructor 
var F = new Function();

// special case: object constructor
var G = new function(){};

// Arrow Functions
let R = () => {}
```

```javascript
// return func
function anime (req_pers) {
	var naruto_shippuden = function () {
		return function() {
			console.log("What are ... ?");
		}
	};

	naruto_shippuden()();
}
```

* ??? I don't remember.

```js
function insideFunc() {
  var another = function () {
    var text = "simple text.";
    return function () {
      return text;
    }
  }
  var call = function () {
    console.log(another()());
  }; call();
};
```

*  a simple counter value from `1 - Infinite`.

```js
function counter() {
  var counter = 1;
  setInterval(function(){
    console.log(`Counter value ${counter}`) + counter++;
  }, 500);
}
```

* ssss. ???

```js
function sayHello(name) {
  var text = 'Hello ' + name;
  var say = function() {
    console.log(text);
  }
  say();
}
```

* Concatinarea la 3 functii

```js
function one() {
  return "This is first sentence.";
}

function two(){
  return "The second sentence.";
}

function together() {
  return one() + " But here is : " + two(); 
}

console.log(together());
```

* Will return only the name at function not the text inside the her. Be careful.

```js
// way 1
const func_one = function () {
  return "Will return only the name at function not this text.";
};

// way 2
const object = {
  func_two : function() {
    return "Analogic way to call name at function, not this text.";
  }
};

console.log(func_one.name);
console.log(object.func_two.name);
```

### YouTube :
* [Multivariable functions | Multivariable calculus | Khan Academy](https://www.youtube.com/watch?v=TrcCbdWwCBc&list=PLSQl0a2vh4HC5feHa6Rc5c0wbRTx56nF7)

### Extern links :
* Разные способы определения функций в JavaScript (это сумасшествие!)](http://langtoday.com/?p=303)
* [Выразительный JavaScript: Функции](https://habr.com/ru/post/240349/)
* [Хекслет: Что такое callback-функция в JavaScript?](https://ru.hexlet.io/blog/posts/javascript-what-the-heck-is-a-callback)
* [Рекурсия Хабра](https://habr.com/ru/post/337030/)
