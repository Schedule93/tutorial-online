### [return main page](../../../README.md) || Topic. Async Await! И Есть "async function". Remember! Important!

* Объявление асинхронной функции создает привязку новой асинхронной функции к заданному имени. Ключевое слово await разрешено в теле функции, что позволяет писать асинхронное поведение на основе обещаний в более чистом стиле и избегать необходимости явно настраивать цепочки обещаний.
```js
function resolveAfter2Seconds() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("resolved");
    }, 2000);
  });
}

async function asyncCall() {
  console.log("calling");
  const result = await resolveAfter2Seconds();
  console.log(result);
  // Expected output: "resolved"
}

asyncCall();
```

### Extern links 
* [Javascript.ru : Async/await](https://learn.javascript.ru/async-await)