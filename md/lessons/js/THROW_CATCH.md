### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

* a simple example
```js
tryCatch = function () {
  var calculate = function(n) {
    if(n > 10) throw new Error(" \'n\' should be less than 10 ");
  return n + 10;
}

  try {
    calculate(20);
  } catch (error) {
    console.log("Can't execute calculate " + error.message);
  }
}
```
