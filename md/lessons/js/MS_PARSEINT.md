
* Will return type of data is number in first example and nan in second example.

```js
function tyoe_of_data_number() {
	var a = parseInt("2");
	var b = typeof(a);
	return b;
}

function tyoe_of_data_nan() {
	var a = parseInt("za");
	var b = typeof(a);
	return b;
}
```