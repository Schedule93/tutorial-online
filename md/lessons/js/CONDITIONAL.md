### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

* O sa trebuiasca de pus tablita cu logica:

1. if `a || b` === something then true
2. f a && b === something. Trebuie ambele sa fie adevarate altfel false 

* instead of condition you can use `?` and `:`, they look alike. And performs the same role.

```javascript
function letter_indexOf(){
  const letter = arg => console.log(
    /[aeiou]/i.test(arg) ? 'vowel' : 'consonant'
  );
  letter('a');
  letter('f');
}
```

* If the condition is `true`, then the statements within are executed. Otherwise, the statements are skipped and the program continues on.

```js
function check_arg() {
	var a = 10; b = 10;
	if(a > 30) {
		console.log("a este mai mare ca 30");
	} else if(b === 10) {
		console.log("Intradevar b egal cu zece");
	} else {
		console.log("ceva gresit");
	}
}

check_arg();
```

* Must work bellow ... Don't like this example.

```js
function showTheWeather(weather) {
  console.log("Cum este timpu de afară?\n");
  switch (weather) {
    case 'ploaie':
      console.log("Nu uita umbrela. "); break;
    case 'soare':
      console.log('Îmbracă-te mai ușor.'); break;
    default:
      console.log('Timp neînțeles.'); break;
  }
}
```
