### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Регулярные выражения!
* `.` - любой одиночный символ
* `[ ]` - любой из них, диапазоны
* `$` - конец строки
* `^` - начало строки
* `\` - экранирование
* `\d` - любую цифру
* `\D` - все что угодно, кроме цифр
* `\s` - пробелы
* `\S` - все кроме пробелов
* `\w` - буква
* `\W` - все кроме букв
* `\b` - граница слова
* `\B` - не границ
* Квантификация
* `n{4}` - искать n подряд 4 раза
* `n{4,6}` - искать n от 4 до 6
* от нуля и выше
* Просто + от 1 и выше
* ? - нуль или 1 раз

### After is code :

* check examples:

```javascript
var reg1 = new RegExp("abc");
var reg2 = /abc/;
console.log(reg1);
console.log(reg2);
```

### Links
* [Регулярные выражения](https://learn.javascript.ru/regular-expressions)
* [regex101](https://regex101.com/)
* [Stack Overflow](https://stackoverflow.com/questions/4736/learning-regular-expressions)