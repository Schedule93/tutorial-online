### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Method indexOf(); in JavaScript.
Метод `indexOf();` возвращает первый индекс, по которому данный элемент может быть найден в массиве, или -1, если он отсутствует.

* if exist or not exist a element from array.

```js
function main (arg) {
	const users = ['nicolai', 'ana', 'alex', 'max', 'andrei'];
	if (users.indexOf(arg) === -1) {
		console.log("So user not exist... ");
	} else {
		console.log(arg);
	}
}

main("ana");
```

* i will must to add description here, bellow ...

```javascript
function one () {
  var name = ["Smith", "John", "Robert"];
  var name2 = ["Nick", "John", "Rony"];

  for (var c of a) {
    for(var d of b) {
      if(c == d) {
        console.log(c);
      }
    }
  }
}

function two() {
  var nume = ["nicu", "ion", "vadim"];
  var nume2 = ["andrei", "alex", "ion", "vadim"];

// dar daca === -1?
  for (var n of nume) {
    if(nume2.indexOf(n) != -1) {
      console.log(n);
    }
  }
}
```

* Arata in consola numai acelea nume care se repeta. Fii `indexOf()` la acest exemplu se foloseshte in loc de alt 2-lea for loop.

```js
function check () {
  var result = "";
  var nume = ["ion", "alex", "roma", "vasile", "vadim"];
  var nume2 = ["sandu", "alex", "cristi", "vova", "roma"];  
  for(var n of nume) {
    if(nume2.indexOf(n) !== -1) {
      result += n + "\n";
    }
  }

  return result;
}

console.log( check() );
```

* i will must to add description here, bellow ...

```js
function find_indexOf () {
	var name = ["Nicolai", "Radu", "Sergiu", "Ion", "Mihai"];

	// -1 inseamna ca nu exsista elementul
	console.log( name.indexOf("Vasile") );  

	// 2 inseamna cu care element o sa inceapa sa calculeze
	console.log( name.indexOf("Mihai", 2) );

	// primu element incepe cu numarul zero
	console.log( name.indexOf("Nicolai") );

	// cauta doar primu argument.
	console.log( name.indexOf("Semion", "Mihai") ); 
}
```

* Arată în consolă doar numărul sau nemerele care nu se repetă. [Look](https://pretagteam.com/question/how-to-return-the-only-value-that-is-not-repeated-in-this-array-duplicate)

```js
let arr = [1, 0, 0, 0, 2, 2, 0, 5, 0, 0];
let result = arr.filter(x => arr.indexOf(x) === arr.lastIndexOf(x));
console.log(result);
```

* sss

```js
function exemplesIndexOf() {

	function exemple2 () {
		var arr = [2, 9, 9];
		var a = arr.indexOf(2) // 0
		var b = arr.indexOf(7) // -1
		var c = arr.indexOf(9, 2) //  ? 2 ?
		var d = arr.indexOf(2, -1) // ? -1 ?
		var e = arr.indexOf(2, -3) // 0 ?
		console.log(a);
	}

	function exemple3 () {
		var name = ["Igor", "Nicolai", "Oleg", "Vova", "Alex", "Ion", "Ati"];
		var box1 = name.indexOf("Igor"); // 0
		var box2 = name.indexOf("Valeara"); // -1 
		var box3 = name.indexOf("Igor", "Ati"); // 0 ?
		var box4 = name.indexOf("Ati", "Igor"); // 6 ?
		console.log(box4);
	}

	function exemple4 () {
		var indices = [];
		var array = ["a", "b", "a", "c", "a", "d"];
		var element = "a";
		var idx = array.indexOf(element);
		while(idx != -1) {
			indices.push(idx);
			idx = array.indexOf(element, idx + 1);
		}
		console.log(indices);
	}

	function exemple5 () {
		console.log("Ok");
	}
	
}

exemplesIndexOf();
```

* Găsirea elementului. Scrie în parametru de ce ai neoie și el o să găsească dacă o să fie în arr.

```js
var arr = ['apple', 'banana', 'cherry', 'donuts', 'eggplant', 'french fries', 'goulash', 'hamburger', 'ice cream', 'juice', 'kebab', 'lemonade', 'mango', 'nuts', 'octopus', 'parsley', 'quail egg', 'risotto', 'stew', 'tapas', 'udon', 'vanilla', 'wheat', 'xylotil', 'yogurt', 'zucchinni'];

function indexOfNative(array, item) {
	return array.indexOf(item);
}

indexOfNative(arr, 'donuts');
```

* vowel or consonant, method `indexOf();`

```js
function check_letter_method_return(arg) {
  var vowel = "aeouiAEOUI";
  var result = "";
  for (var i = 0; i < vowel.length; i++) {
    if (arg === vowel[i]) {
      console.log("vowel");
      return;
    }
  }
  console.log("consonant");
```

* comparing only month in date string Javascript

```js
const Date1 = 'Fri Sep 13 2020';
const Date2 = 'Sun Feb 21 2020';
const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
const getMonthIndex = date => months.indexOf(date.split(' ')[1]);

if (getMonthIndex(Date1) > getMonthIndex(Date2)) {
    // var X=greater
    console.log('greater');
} else {
    // var X=smaller
    console.log('smaller');
}
```