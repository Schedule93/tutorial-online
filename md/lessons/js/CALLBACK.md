### [return main page](../../../README.md) | [Chapter 4 JavaScript](PAGE_JS.md)

### Introduction in callback function
1. функции в js это объекты. Очень страно, особина когда хотим посмотреть какой тип он относится. С помощю `typeof()`. Показывает undefined. Я не понимаю до канца. Это тип данных функциия, обиьект или undefined ? 
2. Проще говоря: обратный вызов - это функция, которая должна выполняться после 
завершения выполнения другой функции - отсюда и название «обратный вызов».
3. А что с анонимные функции ? `callback` это переданой аргумент в качесте анонимной функциии. Aici la 3-lea punct trebuie sa redactez numaidecat. Ca numi place.
4. С помощью колбэк функции можем организовать строгий порядок выполнение кода на
JavaScript. И то не всегда.
5. А что если эти callbacks function больше чем одна ? Например 3, 5 или большее ?
6. для чего нужны callback function

### Algorithm at a CallBack function
1. Write a simple function. 
```js
function main() {
	return "something";
}
```

2. After, put inside her an argument and return it.
```js
function main(callback) {
	return callback();
}
```

3. Write a another function.
```js
function callback() {
	return "I am a callback function.";
}
```

4. We have now to show your result with a `anonym function`.
```js
main(function(){
	console.log(callback());
});
```

5. Now all together.
```js
function main(callback) {
	callback();
}

function callback() {
	return "I am a callback function.";
}

main(function(){
	console.log(callback());
});
```

### Examples of callback functions.
* Freecodecamp.

```javascript
function createQuote(quote, callback){ 
	var myQuote = "Like I always say, " + quote;
	callback(myQuote); // 2
}

function logQuote(quote){
	console.log(quote);
}

createQuote("Eat your vegetables!", logQuote); // 1
```

* A simple callbacks functions. Be careful! Example 2 and 3 a differents examples.
```js
// Example 1 : weird method.
var simple = function() {
  var func = function(callback) {
    var name = "Nicolai";
  callback(name);
};
  func(function(user){
    console.log("hello " + n);
  });
};

// Example 2 : A simple callback function.
function main(callback) {
	return callback();
}
function callback() {
	return "vreu sa intorc un callback function.";
}
main(function(){
	console.log(callback());
});

// Example 3 : Use a callback function with a argument
function main(callback) {
	return callback();
}
function callback(arg) {
	return arg;
}
main(function(){
	console.log(callback("write something ..."));
});
```

* Dar dacă sunt mai multe callbaks ? 
```js
function one() {}
function two() {}
function three() {}
function four() {}
function five() {}
```

### Notes
**remember 1** функции обратного вызова вызываются при срабатывании определенного события. Posibil nu sunt sigur, presupun `event emitter`, `event handler`

### Extern Video :
* Video 1 
* Video 2

### Extern Links 
* [Stack Overflow](https://stackoverflow.com/questions/824234/what-is-a-callback-function)
* [True JS 24. Callback функции](https://www.youtube.com/watch?v=Gpvw-lfVSwg&list=PLQqEY2kzSbZ7nLB8fooOpq89XfuMmwtMW&index=2)
* [Что такое callback-функция в JavaScript?](https://ru.hexlet.io/blog/posts/javascript-what-the-heck-is-a-callback)
