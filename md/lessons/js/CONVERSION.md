### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Conversion

Note: Mai jos eu o să trebuiască să redactez deoarece asta e bătae de joc nu code. A da! Important! Nu uita că asta ai luat din repositul tău de pe GitHub. Dar pe cela ai șters.

```js
console.log("1" + 2); // 12

// Automatic Type Conversion

/*
 * In javascript are three types transformations
    a. String conversion.
    b. Numerical transformation.
    c. Convert to Boolean.
*/

// 1 Comparison by equality: a. Check value: '==', b. Check type of data: '==='
// 2. If you got "NaN", find where the random type conversion occurred.
// 3. undefined is equal to himself.

function conversion() {
    console.info(undefined + " Vs" + " 'undefined' ");
    console.log('undefined' === undefined);
    console.log("" + 1 + 0); // string [+]
    console.log(typeof("" - 1 + 0)); // number [-]
    console.log(true + false) // -1 [-]
    console.log(6 / "3") // 2 [-]
    console.log("2" * "3") // 6 [+]
    console.log(4 + 5 + "px"); // string [-]
    console.log("$" + 4 + 5); // string [+]
    console.log("4" - 2); // 2 [+]
    console.log("4px" - 2) // NaN [+]
    console.log(7 / 0) // 0 [+]
    console.log("  -9\n" + 5) // 5 [-]
    console.log("  -9\n" - 5) // 14 [-]
    console.log(5 && 2) // 2 [-]
    console.log(2 && 5) // 5 [-]

    // console.log(5 || 0) // []
    // console.log(0 || 5); // []
    // console.log(null + 1); // []
    // console.log(undefined + 1); // []
    // console.log(null == "\n0\n"); // []
    // console.log(+null == +"\n0\n"); // []
    console.log("2" + 2);
    console.log(8 * null);                  // -> 0
    console.log('5' - 1);                   // -> 4
    console.log('5' + 1);                   // -> 51
    console.log('good' * 3);                // -> NaN
    console.log('2' == 2);                  // -> true
    console.log('2' === 2);                 // -> false
    console.log(false == 0);                // -> true
    console.log(false == '');               // -> true
    console.log(NaN == NaN);                // -> false
    console.log(NaN === NaN);               // -> false
    console.log(null == 0);                 // -> false
    console.log(null == null);              // -> true
    console.log(null === null);             // -> true
    console.log(null == undefined);         // -> true
    console.log(undefined == undefined);    // -> true
    console.log(undefined === undefined);   // -> true
}



```