### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### The difference between var, let and const is in js:

**Scope (Область видимость)** Переменные делится на два область видимость локальные и глобальные. Локальные переменные создаются внутри функции а глобальные переменные создаются в не каких либо функциях. Рекомендуется большинство переменных писать именно в локальном области видимость. Потому что локальные переменные имеют больше приоритет чем глобальные. 

Код ниже показывает локальную переменную `message` которыя находится внутри функции `show_message();`.

```javascript
function show_message() {
  var message = "Here is my message.";
  return message;
}

console.log( show_message() );
```

But here is global variable.

```js
var eight = 8;
console.log(eight)
```

**Hosting (Подём переменой)** Подъем переменных означает что мы вызываем переменную которую хотим показать на экран раньше чем она была объявлена. И только функции может видеть подъем переменных. А еще только функции создаёт области видимость.

```javascript
console.log(main());
function main(){
  var message = "This is a simple text for me.";
  return message;
}
```

**Variable type (Тип переменой)** Тут просто и одновременно запутано. В `js` есть `var`, `let` и `const`. 

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


Переменная `let b = 2;` не может использоватся для поднятие переменных или по другому называется хостинг. Но почему вы спосите.

```javascript
console.log(b);
let b = 2;
```

Обратите внимание на все три ошыпки:

* Не объявлена переменная `const Z;`. Выдст ошыпку виде ` Missing initializer in const declaration`
* Изменяемая переменная `C = 5;` Выдст ошыпку виде `Assignment to constant variable.`
* Переменная хостинг `const F = 15;` не поднимается. Выдст ошыпку виде `Cannot access 'F' before initialization`

```javascript
// error 1
const Z;
console.log(Z);

// error 2
const C = 3;
C = 5;
console.log(C);

// error 3
console.log(F);
const F = 15;
```

Можно изменить свойство объекта, например `name`. Но нельзя изменить название переменный`const PERSON` в другом месте. Сразу выдаст ошибку.

```javascript
const PERSON = {
  id: 1,
  name: "John",
  age: 25
}

PERSON.name = "Ron"

console.log(PERSON); 
```

* а как вам такое : Говорите `const` неизменяимая переменая.

```js
{
  const arr = [1, 2, 3];
  arr.push(4, 5);

  const obj = {
    name: "hello"
  };
  obj.name = "world";

  console.log(arr);
  console.log(obj);
}
```

A simple example with keyword `var`.

```javascript
var callbacks = [];
(function() {
  for (var i = 0; i < 5; i++) {
    callbacks.push( function() { return i; } );
  }
})();
console.log(callbacks.map( function(cb) { return cb(); } ));
```

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

### Additional links:

* [Hacker Noun](https://hackernoon.com/why-you-shouldnt-use-var-anymore-f109a58b9b70)
* [Stack Overflow : What's the difference between using “let” and “var”?
](https://stackoverflow.com/questions/762011/whats-the-difference-between-using-let-and-var)
* [Check Links](https://stackify.com/18-websites-every-developer-should-visit-right-now/)
* [Stack Overflow : What's the difference between using “let” and “var”?](https://stackoverflow.com/questions/762011/whats-the-difference-between-using-let-and-var)