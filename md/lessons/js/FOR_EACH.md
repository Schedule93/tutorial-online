### [return main page](../../../README.md) || Topic. Explanation, forEach Loop :
* Во-первых, чтобы пройтись по массиву с помощью метода forEach, вам понадобится функция обратного вызова (или анонимная функция):

```js
numbers.forEach(function() {
    // code
});
```

* Функция будет выполняться для каждого элемента массива. Он должен принимать хотя бы один параметр, представляющий элементы массива:

```js
numbers.forEach(function(number) {
    console.log(number);
});
```

* Это всё, что нам нужно было сделать для перебора массива:

### Code Examples :
* Simple exemple. Want to make here simple bubble sort.
```js
let arr = [8, 1, 3, 5, 2, 9, 4, 6, 7];
arr.forEach( (elem, index) => {
	console.log("Curent index is: " + index + " " + elem);
});
```

* Still an example of a simple `forEach loop`.
```js
let arr = [0, 3, 1, 2, 5, 4];
function main() {
	let result = [];
	arr.forEach(function(number, index) {
		if(index > 1 || index === 1) {
			 result += "Index: " + index + ", value: " + number + "\n";
		}		
	})

	return result;
}

// show result
console.log( main() );
```

* Mai jos am făcut o programă unde din variabila `arr`, am dispărțit `boys` of `women`. Și în total s-a primit 2-ouă arrays.
```js
let arr = [
	{
		"name": "Ioana Lutkovskaia",
		"gender": "female",
		"age": 23
	},
	{
		"name": "Alex Cel Bun",
		"gender": "male",
		"age": 30
	},
	{
		"name":"Nicolai Spătaru",
		"gender": "male",
		"age": 31
	},
	{
		"name":"Ion Morari",
		"gender": "male",
		"age": 23
	},
	{
		"name": "Anna Marya",
		"gender": "female",
		"age": 20
	},
	{
		"name":"Nicoleta Raileanu",
		"gender": "female",
		"age": 27
	},
	{
		"name":"Valeriu Melenic",
		"gender": "male",
		"age": 30
	},
	{
		"name":"Valeria Lomonovskii",
		"gender": "female",
		"age": 26
	}
];

let boys = [];
let women = [];

arr.forEach(function(person, index) {
	if(person.gender === "male") {
		boys.push(person);
	} else if(person.gender === "female") {
		women.push(person);
	} 

	return boys;
});

// show results :
console.log("Below is boys list : \n", boys, "\n");
console.log("But here is women list : \n", women, "\n");
```

* simple way to make a `arrow function`.
```js
let arr = [3, 5, 7, 9]
arr.forEach((person, index) => {
	console.log(person);
});
```

* [...]
```js
let arr = ['one', 'two', 'three'];
arr.forEach((elem) => {
	console.log(elem);
});
```

* forEach with `callback` :
```js
console.log("here will be forEach with callback. ");
```

* Difference between "For Loop" and "For Each": 
```js
console.log("Here will be difference between for loop and forEach. ");
```

* A simple exemple of `arrow function` with `forEach`.
```js
let arr = [5,1,4,2,3];
let store = [];
arr.forEach((number, index) => {
	console.log("Index is: " + index + "; Value is: " + number + ";");
});
```

### Extern links :
* [JavaScript forEach – How to Loop Through an Array in JS](https://www.freecodecamp.org/news/javascript-foreach-how-to-loop-through-an-array-in-js/)
* [Stack Overflow - If statement not working inside foreach loop JavaScript](https://stackoverflow.com/questions/50773374/if-statement-not-working-inside-foreach-loop-javascript)
* Плюсы и минусы `forEach loop` ?
