* Pentru a putea crea noi parametri într-o funcție noi deobicei folosim argumente înăuntru la paranetele rotunde. 

```js
function new_user(nickname, gender, age){
	let arr = [nickname, gender, age];
	return arr;
}

console.log(new_user("nicolai", "male", 26));
```

* Dar uite-te la codul de mai jos avem înainte o variabilă deoarce să putem manipula în viitor cu această variabilă.

```js
var Person = function(nickname, gender, age, password, repeat_password) {
  this.nickname = nickname;
  this.gender = gender;
  this.age = age;
  this.password = password;
  this.repeat_password = repeat_password;
};
```

### Extern links :
* [YouTube : Lesson 66. JavaScript. Prototypes](https://www.youtube.com/watch?v=fBEQzyUobnE&ab_channel=%D0%9A%D0%B0%D0%BC%D0%B8%D0%BB%D1%8C%D0%90%D0%B1%D0%B7%D0%B0%D0%BB%D0%BE%D0%B2)