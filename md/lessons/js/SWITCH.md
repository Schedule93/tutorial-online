### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Switch

* Sintax

```js
function cheesBoard () {
    var chessBoard = "";
    var size = 8;
    for (var lineCounter = 1; lineCounter < size; lineCounter++) {
        if (lineCounter % 2 === 0) {
            for (var charCounter = 1; charCounter < size; charCounter++) {
                var evenOdd = (charCounter % 2 === 0);
                switch (evenOdd) {
                    case true:
                        (chessBoard += "#"); break;
                    case false:
                        (chessBoard += " "); break;
                }
            }
        }

    else {
        for (var charCounter = 1; charCounter < size; charCounter++) {
            var evenOdd = (charCounter % 2 === 0);
            switch (evenOdd) {
                case true:
                    (chessBoard += " "); break;
                case false:
                    (chessBoard += "#"); break;
            }
        }
    }
    chessBoard += "\n";
    }
    console.log(chessBoard);
}
```