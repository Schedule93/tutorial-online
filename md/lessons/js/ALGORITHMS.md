### [return main page](../../README.md) || [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Algorithms and structures of data : 
* Factorial ?
* Fibonacci ? 
* Backtracking
* Cache
* Алгоритм графы
* Битовые манипуляции
* Алгоритмы и их сложности
* Linked List
* Queue
* Stack
* Hash Table, Hashes
* Linear alebra 
* Binary Search Tree
* Heap
* Priority Queue
* Threes
* String
* Other
* Math
* Find
* Linear Search
* Binary Search
* Permutations
* Combinations
* Quicksort
* Greedy-algorithm
* Cryptography
* Genetics
* [Поиск в ширину && поиск в глубину](https://habr.com/ru/post/504374/)
* Быстрая сортировка, сортировка слиянием
* Динамические массивы
* Двоичное дерево поиска
* Динамическое программирование
* Big-O анализ
* 2D множества
* Sequence algorithms [wiki](https://en.wikipedia.org/wiki/Sequence)

### Secondary, it's not important for me ...

* Graphs
* Tree Depth First Search
* Tree Breadth First Search
* Graph for search
* Graph Depth First Search
* Graph Breadth First Search


- Scoate in consola cifre care numa se repeta. 

```js
const arr = [1, 9, 4, 3, 3, 1, 3, 9, 2, 4, 2, 1, 4, 4];
const pickDuplicate = arr => {
   const res = [];
   for(let i = 0; i < arr.length; i++){
      if(arr.indexOf(arr[i]) !== arr.lastIndexOf(arr[i])){
         if(!res.includes(arr[i])){
            res.push(arr[i]);
         };
      };
   };
   return res;
};

console.log(pickDuplicate(arr))
```

### Links:

* [Tutorialpoint](https://www.tutorialspoint.com/data_structures_algorithms/algorithms_basics.htm)