### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Patern

**note 1** Firstly, what you must know about `OOP` and `Pattern Design`, is that they are two different things. Don't confuse them, please. This is very important thing!

**note 2** ООП - это особый способ объединения функций в один «класс». OOP это обядинение фунции и структур данных + данных.

* Vezi că ai făcut un exemplu asemănător de pe site-ul MEDIUM.

```javascript
// Eu nu prea î-mi aduc aminte.
function example_1() {
	var myModule = (function() {

		var memes = ['cats', 'doge', 'harambe'];

		var getMemes = function() {
	    	 return memes;
		};

		return {
			getMemes: getMemes
		};
	})();

	console.log(myModule.getMemes());
}
```

* Тип шаблон проектирование : `constructor`

```js
var User = function(id, name, age, email) {
	this.id = id;
	this.name = name;
	this.age = age;
	this.email = email;
}

User.prototype.run = function() {
	console.log(this.id, + " " + this.name + " " + this.age + " " + this.email);
}

var nicolai = new User(1, "nicolai", 26, "cushnirnicolai@gmail.com");
var bob = new User("3", "Bob", "33", "bob@mail.com");

bob.run();
```

### Links

* [MEDIUM](https://medium.com/@marina.kovalyova/java-script-design-patterns-569c627d25f9)
* [Use Strict Pattern](https://ru.stackoverflow.com/questions/435546/%d0%a7%d1%82%d0%be-%d0%b7%d0%bd%d0%b0%d1%87%d0%b8%d1%82-use-strict)
* [Habra](https://habr.com/ru/post/136766/)
* [Паттерны для масштабируемых JavaScript-приложений](http://largescalejs.ru/)
* [Learning JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/?fbclid=IwAR09xoEedD4jdCYH9sm2-1KmtbZLj_HPKBl9gq_iirptlnJQX-uuD1E59-8)