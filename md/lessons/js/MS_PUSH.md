
### Method Push

```js
function learn_push_method() {
	var user = ["Emty User", "Nicolai", "Alex", "Ion"];
	user.push("Vasile", "Petru", "Eugeniu", "Dima");

	var result = "";
	for(var i=0; i<user.length; i++) {
		result += i + " " +  user[i] + "\n";	
	}

	return result;
}

console.log(learn_push_method());
```
