### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### All Methods in JavaScript

**number Methods**
* [math.pow();](MS_MATH_POW.md)
* [math.Math.sqrt();](MS_MATH_SQRT.md)
* [Math.floor();](MS_MATH_FLOOR.md)
* [Math.ceil();](MS_MATH_CEIL.md)
* [Math.random();](MS_MATH_RANDOM.md)
* [Math.round();](MS_MATH_ROUND.md)
* [Math.PI();](MS_MATH_PI.md)
* [parseInt();](MS_PARSEINT.md)

**String Methods**
* [String.fromCharCode()](#)
* [CharAt();](CHARAT.md) Извлекает из строки символ, находящийся в указанной позиции.
* [concat();](#) Выполняет конкатенацию одного или нескольких значений со строкой.
* [substr();](#) Извлекает подстроку из строки. Аналог метода substring().

**Array Methods**
* map();
* reduce();
* [push()](MS_PUSH.md) 
* [pop();](#)
* [slice();](#) Возвращает фрагмент строки или подстроку в строке.
* [reverse();](#)
* [splice();](#)
* [toString();](#)
* [some();](#)
* [sort();](#)
* [Object.entries();](#)
* Object.is();
* Object.keys();
* Object.values();
* aaa.hasOwnProperty()
* [indexOf();](MS_INDEX_OF.md) Выполняет поиск символа или подстроки в строке.
* [findIndex();](#)
* [includes();](#)
* [find();](#)
* [filter();](#)

**Object Methods**
* ?
* ?

**Time Methos**
* [setTimeout();](MS_SET_TIMEOUT.md);
* [setInterval();](MS_SET_INTERVAL.md);

**Others**
* [charCodeAt();](MS_CHAR_CODE_AT.md)
* [call();](#)
* [apply();](#)
* [lastIndexOf();](MS_INDEX_OF.md) Выполняет поиск символа или подстроки в строке с конца.
* [console.clear();](#)

**Регулярные Выражения**
* [match();](#) Выполняет поиск по маске с помощью регулярного выражения.
* [replace();](#) Выполняет операцию поиска и замены с помощью регулярного выражения.
* [search();](#) Ищет в строке подстроку, соответствующую регулярному выражению.
* [split();](MS_SPLIT.md) Разбивает строку на массив строк по указанной строке разделителю или регулярному выражению.

### External Links:
* [MDN : Math Doc](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math)
* [sebhastian](https://sebhastian.com/javascript-math-floor/)
* [Список функций и методов PHP. Important](https://www.php.net/manual/ru/indexes.functions.php)
* [MDN : JavaScript reference methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
* [YouTube : Learn to code a JavaScript Random Name Generator](https://www.youtube.com/watch?v=J3MIQTxV8-c&list=PLQqEY2kzSbZ4NMd7xsuc28a6Kc-_300Jb&index=6&ab_channel=JuniorDeveloperCentral)

