![image](../../../public/img/blond_right_corner.png)

### [return main page](../../../STATIC.md) | Chapter 7. Learn JavaScript :
* [Variables](VARIABLES.md)
* [Оформление JavaScript](#)
* [Type of data](TYPE_OF_DATA.md)
* [Typification](TYPICICATION.md)
* [This](THIS.md)
* [Bitwise](https://www.w3schools.com/js/js_bitwise.asp)
* [Type Conversion](CONVERSION.md)
* [Undef or Null](UNDEF_OR_NULL.md)
* [Switch](SWITCH.md)
* [Objects](OBJECTS.md)
* [if else vs Switch](CONDITIONAL.md)
* [Try Throw Catch](THROW_CATCH.md)
* [Array](ARRAY.md)
* [While loop](WHILE_LOOP.md)
* [For loop](FOR_LOOP.md)
* [forEach](FOR_EACH.md)
* [Closing](CLOSING.md) || Замыкания
* [GET and SET](GET_AND_SET.md)
* [Parser](PARSER.md)
* [Types of functions](FUNCTIONS.md)
* [Функции‑Конструкторы](CONSTRUCTOR_FUNCTIONS.md)
* [Async - Await](#)
* [Sync / Async](SYNC_ASYNC.md)
* [Callback](CALLBACK.md)
* [Call stack](CALL_STACK.md)
* [Types of Errors](ERRORS.md)
* [RegExp](REGEXP.md)
* [Prototype](PROTOTYPE.md)
* [OOP](OOP.md)
* [Patern Design](PATERN.md)
* [Garbage Collection](GARBAGE.md)
* [DOM](DOM.md)
* [Canvas](CANVAS.md)
* [Parallelism](PARALLELISM.md)
* [Minuses](MINUSES.md)
* [Debugging](DEBUGGING.md)
* [Algorithms](ALGORITHMS.md)
* [Обработка Событий](#)
* [Tools](TOOLS.md)
* [Methods](METHODS.md)
* [Статическим Анализатором](STATIC_ANALYZER.md)
* [Events JavaScript](EVENTS.js)
* [Questions](QUESTIONS_JS.md)
* [Generate HTML](GENERATE_HTML.md)
* [Console](CONSOLE.md)
* [Cache](CACHE.md)
* [Coockies](#)

### Read Books :
* JavaScript для детей. Н.Морган
* JavaScript для профессионалов. Д.Резиг, Р.Фергюсон и др
* JavaScript. Подробное руководство. 6-е изд. Д.Флэнаган
* JavaScript. Шаблоны. С.Стефанов
* JavaScript: сильные стороны. Д.Крокфорд
* ES6 и не только. К.Симпсон
* JavaScript. The definitive guide. 6th ed. D.Flanagan
* "Выразительный JavaScript"
* [Eloquent JavaScript](https://eloquentjavascript.net/)
* [The JavaScript Beginner's Handbook (2020 Edition)](https://www.freecodecamp.org/news/the-complete-javascript-handbook-f26b2c71719c/)
* [10 Best JavaScript Books for Beginners & Advanced Developers](https://hackr.io/blog/javascript-books)

### Extern Video :
* Video 1
* Video 2

### Extern Links :
* [Javascript.ru](https://learn.javascript.ru/)
* [dweebd.com](http://www.dweebd.com/index.php?s=javascript)
* [YouTube : Вебинары по Javascript](https://www.youtube.com/watch?v=-2WiaSvOj78&list=PLyeqauxei6jezJsOYzsxZFPv8OJe5fb6a)
* [Flaviocopes](https://flaviocopes.com/)
* [Stack Overflow Frequent](https://stackoverflow.com/questions/tagged/algorithm?sort=frequent&pageSize=15)
* [Как проверить гласные в JavaScript?](http://qaru.site/questions/367622/how-do-i-check-for-vowels-in-javascript)
* [Sitepoint](https://www.sitepoint.com/5-typical-javascript-interview-exercises/)
* [quirksmode.org](https://quirksmode.org/js/contents.html)
* [neverentgeek.com](https://reverentgeek.com/do-you-hate-javascript/)
* [Blog : Joel on Software](https://www.joelonsoftware.com/2006/08/01/can-your-programming-language-do-this/)
* [5 Common Causes of JavaScript Errors (and How to Avoid Them)](https://geekflare.com/javascript-common-errors/#:~:text=To%20avoid%20such%20syntax%20errors,them%20with%20your%20developed%20application.)
* [YouTube playlist WebDev с нуля. Канал Алекса Лущенко](https://www.youtube.com/watch?v=hcsdZzjd42c&list=PLM7wFzahDYnEcQh1G_fxFXBZ4O1tlVsDG&ab_channel=WebDev%D1%81%D0%BD%D1%83%D0%BB%D1%8F.%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%9B%D1%83%D1%89%D0%B5%D0%BD%D0%BA%D0%BE)
* [TheAlgorithms/C-Sharp](https://github.com/TheAlgorithms/C-Sharp)

### Future Tasks:
* Scrie o propoziție. Acum fă așa ca din această propoziție să-ți arate cea mai des folosită literă, dar ce dacă sunt egale atunci continuă jocul.
