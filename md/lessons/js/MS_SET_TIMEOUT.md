### Return

### setTimeout
setTimeout позволяет нам запускать функцию один раз по истечении определенного интервала времени. setInterval позволяет нам многократно запускать функцию, начиная с определенного интервала времени, а затем непрерывно повторяя через этот интервал.

* Vez nu sterge e foarte important pentru mine. Each second will show value at `i` with help at iteration. (redacteaza mai incolo)

```js
var i = 1;
var intervalId = setInterval(function(){
	console.log(" * Now value is : " + i), i++;
	var timoutId = setTimeout(function(){ 
	}, 1000);
}, 1000);
```

### External Links:
* [medium](https://medium.com/@eric.stermer/setinterval-simply-put-is-a-timed-loop-652eb54bd5f8)
