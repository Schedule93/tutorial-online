### new Set

* Arată în consolă toate numerele care se transformă din repetate într-o singură dată, pentru fiecare element.

```js
var arr = [9, 9, 1, 7, 1, 3, 3, 5, 5, 4, 4, 8, 8];
var unique = [...new Set(arr)];
console.log(unique);
```

