### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Method math.floor();
* `Math.floor();` - (Trebuie să verific dacă numerele pozitive se rotungesc in jos ...) Numerele pozitive se rotungesc in jos, iar cele negative în sus. Метод `Math.floor();` всегда возращяет только первый параметр, несмиотя даже там больщее параметров. Пример:

```js
var a = Math.floor(12.5, 234.2);
console.log(a);
```

Является частью объекта Math, который можно использовать для округления десятичного числа в меньшую сторону. Например, числовая переменная 7.2 будет округлена до 7, когда вы передадите ее в метод.

Метод принимает только один параметр и это `number`. А если вы дадите `string`, `object` или даже `array` метод, то тогда метод math.floor() вернёт `NaN`, что означает `Not a Number`. Например:

```js
// Warning: All Exemples are Wrongs !!!
Math.floor("Hello there!");
Math.floor([1, 2, 3]);
Math.floor({ name: "Andy" });
```

Но когда вы использываете `boolean`, значение может быть `true or false`. Тоесть `1 для true` и `0 для false`.

```js
Math.floor(true); // returns 1
Math.floor(false); // returns 0
```

Не забудьте использовать точку (.) При передаче десятичного числа, потому что запятая (,) в параметрах JavaScript является индикатором отдельных переменных. В следующем примере JavaScript просто подставит первое число и проигнорирует второе, когда вы передадите два числа:

```js
Math.floor(9, 2); // returns 9
```

Наконец, метод Math.floor () округляет число в меньшую сторону, независимо от того, является ли десятичное значение 0,5 или больше:

```js
Math.floor(8.5); // returns 8
```

Метод также округляет отрицательное число в меньшую сторону. Отрицательные числа тем меньше, чем дальше они от нуля, поэтому числовое значение `-55,2` будет округлено до `-56`, а не `-55`:

```js
Math.floor(-55.2) // returns -56
```

**Be Careful !!!** Cînd merge vorba de numere negative la metoda Math.floor(); ele se ridică în sus. Indiferent ce număr cu virgulă este acolo. De exemplu `-1.1`. Va fi -2, nu -1. Fii atent! 

Cu alte cuvinte. Reține:
* numerele pozitive se rotungesc în jos. 1.1 va fi 1
* numerele degative se rotungesc în sus. `-1.1` va fii -2.

```js
console.log(Math.floor(21.5)); // 21
console.log(Math.floor(-12.2)); // 12 [-]
console.log(Math.floor(3.1)); //  3
console.log(Math.floor(-1.1)); //  -1 [-]
console.log(Math.floor(-1.9)); // -1 [-]
```