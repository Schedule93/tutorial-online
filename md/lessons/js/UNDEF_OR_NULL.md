### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### How to check a variable is undefined or null
`Warning!` В JavaScript чтобы проверить что параметр/переменая отсуствует мы исользуем

* Global Variables : `typeof nameVar === 'undefined'`
* Local Variables : `nameVar === 'undefined'`
* Properties : `object.prop === undefined`

Global variable `undefined`

```javascript
function global_undef (arg) {
	if(typeof arg !== 'undefined') {
		console.log('Good');
	} else {
		console.log("Bad");
	}
}
```

Local variables `undefined`

```javascript
function local_undef() {
	var a;
	if(a === undefined) {
		console.log("nu este definita");
	}
}
```

* [How to check for “undefined” in JavaScript?](https://stackoverflow.com/questions/3390396/how-to-check-for-undefined-in-javascript)
* [undef vs 'undef'](https://stackoverflow.com/questions/4725603/variable-undefined-vs-typeof-variable-undefined)