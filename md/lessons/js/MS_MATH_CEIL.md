### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Math.ceil(); Method.
* `Math.ceil()` - (Verifica această propoziție) **=>** Numerele pozitive se rotungesc in sus, iar cele negative în jos.

Когда вы хотите округлить число в большую сторону, а не в меньшую

Если вы хотите округлить число в большую сторону, а не в меньшую (от 7,2 до 8), то вам нужно использовать метод `Math.ceil();` вместо `Math.floor();`. Вся разница заключаетя в том что Math.fllor(); для использования меньших округленных чисел. А Math.ceil(); для округленных наибольшее число.

```js
console.log(Math.floor(4.2)); // 4
console.log(Math.ceil(4.2)); // 5
```

Math.ceil(); ca și math.floor() se rotungește din număr cu vir

```javascript
Math.ceil(12.7); // return 13;
```

Întoace primu parametru, adica număr indiferent dacă acolo sunt mai multe numere bifate cu vircule. Uite-te mai jos la aceste 2 exemple:

```js
Math.ceil(12.2, 24.8); // return 13
Math.ceil(124.4, 12.2, 24.8); // return 125
```

Chiar dacă numărul, spre exemplu `4.2`. Se rotungește la `5` dar nu cum am putea gîndi la `4` Dar de ce, Se înîmplă asta? Fii atent!

```js
console.log(Math.ceil(4.2) ); // return 5 WTF?
```

Dar la numere negative, situația e cu totul diferită. Dă așa cum trebuie, adica cum ne așteptăm. Dă `7` nu `8`. Fii atent!

```js
Math.ceil(-7.004); // -7
Math.ceil(-0.95);  // -0
```

Cu alte cuvinte. Ce face metoda `Math.ceil();` :
* la numere pozitive se ridica + 1;
* la numere negative se micshoreaza

```js
console.info("Math Ceil");
console.log(Math.ceil(2.1)); // 3
console.log(Math.ceil(-2.1)); // -2
console.log(Math.ceil(-2.2)); // -3
console.log(Math.ceil(-2.3)); // -3
console.log(Math.ceil(-2.4)); // -3
console.log(Math.ceil(-2.5)); // -3
console.log(Math.ceil(-2.6)); // -3
console.log(Math.ceil(-2.7)); // -3
console.log(Math.ceil(-2.8)); // -3
console.log(Math.ceil(-2.9)); // -3
console.info("Math Floor");

console.log(Math.floor(2.1)); // 2
console.log(Math.floor(-2.1)); // -2
console.log(Math.floor(-2.2)); // -2
console.log(Math.floor(-2.3)); // -2
console.log(Math.floor(-2.4)); // - -2
console.log(Math.floor(-2.5)); // - -2
console.log(Math.floor(-2.6)); // - -2
console.log(Math.floor(-2.7)); // - -2
console.log(Math.floor(-2.8)); // - -2
console.log(Math.floor(-2.9)); // - -2
```

### External links:
* [MDN Doc : Math.ceil();](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/ceil)
* [](https://sebhastian.com/javascript-math-floor/)
* [](https://www.educative.io/edpresso/mathceil-mathfloor-and-mathround-in-javascript)