### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Closing or Closure

**Note** Замыкания они полезны в качестве аргументов для функций высшего порядка, таких как sort. Замыкания полезны для функций, которые также создают другие функции, например, [...] Замыкания также полезны для `функций обратного вызова`. 

```javascript
function check() {
  var i = 5;
  var one = function() {
    var i = 10;
    console.log(i);
    var two = function() {
      var i = 15;
      console.log(i);
    }; two();
  }; one();
}
```

* JavaScript closure inside loops – simple practical example

```js
function clasureInsideLoops () {
  var funcs = [];
  for (var i = 0; i < 3; i++) {
    funcs[i] = function() {
      console.log('My value: ' + i);
    };
  }
  for (var j = 0; j < 3; j++) {
    funcs[j]();
  }
}
```

* Say hello. Very simple examle with `closing`.

```js
let name = "John";
function say_hello() {
  console.log("hello, " + name);
}

name = "Nicolai"; say_hello();
```

* learn close-chain.

```js
var one = function() {
  var number = 1;
  var inside_at_one = function() {
    var number = 10;
  }; inside_at_one();
  console.log(number);
}; one(); 

var two = function() {
  var number = 2;
  var inside_at_two = function() {
    // Be careful bellow ... 
    number = 20;
  }; inside_at_two();
  console.log(number);
}; two(); 
```

### Addition links :

* Sorax YouTube JS
* [Stack Overflow : JavaScript closure inside loops – simple practical example](https://stackoverflow.com/questions/750486/javascript-closure-inside-loops-simple-practical-example)
* [Stack Overflow : How do JavaScript closures work?](https://stackoverflow.com/questions/111102/how-do-javascript-closures-work)