### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Objects

* Check if an array includes an object 

```js
function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}
```

* Acest exemplu la programul dat consta in : să scrii `numele tau` și `luna în care te-ai născut`, de exemplu decembrie și o sa arăte cu ce star vedetă te-ai născut .. 

```js
let users = [
	{ id: 0, name: "Emty User" },
	{ id: 1411, name: "Junior Santos", day: 22, month: "ianuarie", borned_year: 1990, age: 0, country: "Poland"},
	{ id: 2922, name: "Nicolai Cushnir", day: 19, month: "decembrie", borned_year: 1993, age: 28, country: "Moldova"},
	{ id: 3437, name: "Armand Morse", day: 11, month: "mai", borned_year: 1992, age: 0, country: "Ukraine"},
	{ id: 4570, name: "Carlos Gross", day: 9, month: "noiembrie", borned_year: 1994, age: 0, country: "Romania"},
	{ id: 5607, name: "Alexandru Harding", day: 5, month: "februarie", borned_year: 1991, age: 0, country: "Finland"}
];

let stars = [
	{ id: 0, name: "Emty User" },
	{ id: 1, name: "Chester Bennington", age: 41, day: 20, month: "octombrie", borned_year: 1976, country: "none" },
	{ id: 2, name: "Billie Joe Armstrong", age: 50, day: 17, month: "februarie", borned_year: 1972, country: "none" },
	{ id: 3, name: "none", age: 0, day: 0, month: "ianuarie", borned_year: 0, country: "none" },
	{ id: 4, name: "none", age: 0, day: 0, month: "aprilie", borned_year: 0, country: "none" },
	{ id: 5, name: "none", age: 0, day: 0, month: "mai", borned_year: 0, country: "none" },
	{ id: 6, name: "none", age: 0, day: 0, month: "martie", borned_year: 0, country: "none" },
	{ id: 7, name: "none", age: 0, day: 0, month: "iunie", borned_year: 0, country: "none" },
	{ id: 8, name: "none", age: 0, day: 0, month: "iulie", borned_year: 0, country: "none" },
	{ id: 9, name: "Jared Leto", age: 50, day: 26, month: "decembrie", borned_year: 1971 },
	{ id: 10, name: "none", age: 0, day: 0, month: "noiembrie", borned_year: 0, country: "none" },
	{ id: 11, name: "none", age: 0, day: 0, month: "septembrie", borned_year: 0, country: "none" },
	{ id: 12, name: "none", age: 0, day: 0, month: "august", borned_year: 0, country: "none" }
];

function main (arg, user_borned_month) {
	for (let i = 0; i < users.length; i++) {
		for(let j = 0; j < stars.length; j++) {
			if(users[i].name === arg && users[i].month === user_borned_month) {
				if(arg && user_borned_month === stars[j].month) {
					return users[i].name + " borned same like " + stars[j].name + " in " + users[i].month;
				}
			}
		}
	}
}

console.log( main("Nicolai Cushnir", "decembrie") );
```

### Links :
* Link 1
* Link 2