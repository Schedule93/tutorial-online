### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Types of errors in javascript 

**ReferenceError**

* ReferenceError: phrase is not defined

```js
function say_hi(){
	var phrase = "Hi! ";
	return phrase;
}

console.log( say_hi() );
console.log(phrase);
```

**SyntaxError**

* SyntaxError: Missing initializer in const declaration

```js
console.log(a);
var a; 
```

* TypeError: Assignment to constant variable.

```js
constx„x„ c = 3;
c = 4;
console.log(c);
```

*

**TypeError**

**RangeError: Maximum call stack size exceeded**