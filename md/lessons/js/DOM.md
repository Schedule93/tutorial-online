### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Начало работы с DOM елементами 
Для наала работы с HTML DOM елементы вы должны знать. 
* s
* s
* s

1. Обратите внимание на код ниже. Оно не будет работать. Но не волнуйтесь. Ниже я объясню, почему этот кусок кода не работает.

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
	<title>draw</title>
	<link rel="stylesheet" href="style.css">
	<script src="script.js">
		document.getElementById('demo').innerHTML = 'hi';
	</script>
</script>
</head>
<body>
		<p id="demo"></p>
</body>
</html>
```

Во первых, вы видите этот кусок кода ? Он взят из предыдущего примера из тэга `script`.

```js
	document.getElementById('demo').innerHTML = 'hi';
```

Так вот. Это происходит потому что Браузер всегда загружает весь HTML DOM сверху вниз и пытаются получить доступ к элементу с идентификатором/id `demo` еще до того, как он был фактически отображен в DOM. Очевидно выдаст ошыпку. `Uncaught TypeError: Cannot set property 'innerHTML' of null` . Тут есть два варянта либо javascript должен запускаться после загрузки страницы HTML в конце тега `body` Либо второй варянт загрузить функцию `window.onload`

```cs
<p id="demo"></p>
```

2. Мы загружаем HTML DOM в конец тега `body`

```html
  <!DOCTYPE HTML>
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Untitled Document</title>
    </head>
    
    <body>
    <div id="hello"></div>
    
    <script type ="text/javascript">
        what();
        function what(){
            document.getElementById('hello').innerHTML = 'hi';
        };
    </script>
    </body>
    </html>
```

3. Загруска функции `window.onload`

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
	<title>draw</title>
	<link rel="stylesheet" href="style.css">
	<script src="script.js">
		window.onload = function check(){
			document.getElementById('demo').innerHTML = 'hi';
		};
	</script>
</script>
</head>
<body>
		<p id="demo"></p>
</body>
</html>
```

## Basic JS DOM elements methods :
**Finding JS Elements**
* document.getElementById(id)           
* document.getElementsByTagName(name) 
* document.getElementsByClassName(name)

**Changing JS Elements**
* element.innerHTML = new html content	Change the inner HTML of an element
* element.attribute = new value	Change the attribute value of an HTML element
* element.style.property = new style. Change the style of an HTML element
* element.setAttribute(attribute, value) Change the attribute value of an HTML element

**Adding and Deleting Elements**
* document.createElement(element)
* document.removeChild(element)	
* document.appendChild(element)
* document.replaceChild(new, old)
* document.write(text)

**all together**
* document.getElementById(id).onclick = function(){code}
* querySelector()

### Dom Methods

* Данный метод показывает ширину окна браузера.

```js
const windowWidth = window.innerWidth;
console.log("Ширина окна браузера : " + windowWidth + "px");
```

* addEventListener

```js
function addListeners(elements, callback) {
    console.log('this makes it to the console');
    for ( let i = 0; i < elements.lenght; i++ ) {
        console.log('this doesn\'t');
        elements[i].addEventListener('click', callback);
    }
    console.log('this makes it too');
}
```

* Change Color at Text when you press on the button.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <title>draw</title>
    <link rel="stylesheet" href="style.css">
    <script>
        window.load = function main() {
        var parent_dom = document.getElementById('parent-id');
        var test1 = parentDOM.getElementById('test1');
    }

    function changeColor(newColor) {
        var element = document.getElementById("view");
        element.style.color = newColor;
        element.styke.size = newSize;
    }

    </script>
</script>
</head>
<body>
    <div class="content">
        <br> <p id="view">Here is aa simple text for change the color. </p>
        <button onclick="changeColor('red');">red</button>
        <button onclick="changeColor('purple');">purple</button>
        <button onclick="changeColor('green');">green</button>
    </div>
</body>
</html>
```

* document.querySelector(); 

```js
console.log("What is: 'document.querySelector(); '???");
```

### Links :
* [456](https://www.w3schools.com/jsref/met_element_addeventlistener.asp)
* [javascripttutorial.net](https://www.javascripttutorial.net/javascript-dom/)
