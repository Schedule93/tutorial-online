### [return main page](../../README.md) | [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Types of data

* types of data
* METHODS.md
	* string:
	* Methods String
	* Numbers Methods
	* Type of data : Array
	* Methods Array
	* Type of data : Object
	* Methods Object
	* Object and Array together
 

* check if string or number
```js
function type_of_data (arg) {
	if(!isNaN(arg)){
		console.log("Number ");
	} else {
		console.log(`String `);
	}
}

/*
ParseInt - transform from str in number
Number();
String();
*/
```

* array -> methods string
	* split()
	* filter()
	* find()
	* findIndex() 
	* imports()
	* push()
	* slice()
	* [Stack Overflow](https://stackoverflow.com/questions/966225/how-can-i-create-a-two-dimensional-array-in-javascript)
* object -> methods objects
	* bind()
	* The word key `this`
		* [W3school](https://www.w3schools.com/js/js_this.asp)
* Object and Array together
* Number -> methods
	* isNaN()
	* NaN()
	* math.random()
	* math floor()
* Null or ndefined -> methods
* boolean
	* has();
	* Object.is()
* difference methods
	* method for boolean : has();
	* Object.is
	* parseint() - transform din string in number, vez ca el poae se deie cateodata NaN, mai bine use !isNaN()
	* parseFloat()
	* typeof:
		* typeof === 'undefined'
		* typeof === 'number'
		* typeof === 'string'
	