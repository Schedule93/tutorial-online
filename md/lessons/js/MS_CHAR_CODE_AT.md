### Topic charCodeAt

### Exemples :

* Paneatie nam ce face:

```js
// ??? 
function ecoding() {
	
	// must be a string.
	var letter = "^_^"; 
	
	// show the result
	var result = "";

	result = letter.charCodeAt(0)

	return result;
}

console.log(ecoding());
```

* O să arăte tot alfabetu. Cruta na samom dele. Prosta eu iam daat deapazonu. Tipa de la `a` să înceapă dar se termină cu `z`. Desigur le poți shimba. 

```js
function range(start,stop) {
  var result=[];
  for (var idx=start.charCodeAt(0),end=stop.charCodeAt(0); idx <=end; ++idx){
    result.push(String.fromCharCode(idx));
  }
  return result;
};

console.log( range("a","z"));
```

Метод `charCodeAt()` возвращает целое число от 0 до 65535, представляющее кодовую единицу UTF-16 по заданному индексу.