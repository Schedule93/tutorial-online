### [return main page](../../../README.md) || [Chapter 4. Learn JavaScript](PAGE_JS.md)

### Array JavaScript :

* array multidimensional
```js
var showMerchandise = function() {
  var hardware = ["iphone", "planshet", "laptop", "calculator"];
  var jocuri = ["lineage", "skyrim", "league of legends", "vikings", "war craft"];
  var haine = ["maoiu", "scurta", "pantaloni", "shortishi", "chipiu", "shosete"];
  var together = [hardware, jocuri, haine];
  console.log("Catalog hardware:")
  together.forEach(function(element) {
      element.forEach(function(item) {
        console.log(item);
      });
      console.log();
  });
};
```

* Vezi cei cu asta pe urmă.
```js
function playArr () {
  var variables = function() {
      var arr = ["a", "d", "f", "c"];
      var first_element = arr[0];
      var last_element;
      var undef = arr[4];
      var change = arr[2] = "changed";
      var new_element = arr[4] = "new element";
      var call_length = arr.length;
      var last_new_element = arr[arr.length] = "add new element finish";
      var jumped = arr[20] = 'something';
      return function () {
        return arr;
      }
  };
  var call = function() {
      var store = variables()();
      for(var each = 0; each < store.length; each++) {
        if(store[each] === undefined) {
          console.log(each + " " + "element not exist")
        } else {
          console.log(each + " " + store[each])
        }
      }
  }; call();
};
```

* Play with array
```js
function play_with_array() {
	var vowel = ["a", "e", "o", "u", "i", "A", "E", "O", "U", "I"];
	vowel.forEach(function(item){
		console.log(item);
	})
}
```

* Selection sort :
```Algorithm
function select(list[1..n], k)
    for i from 1 to k
        minIndex = i
        minValue = list[i]
        for j from i+1 to n do
            if list[j] < minValue then
                minIndex = j
                minValue = list[j]
                swap list[i] and list[minIndex]
    return list[k]
```

* How to combine two arrays
```js
// The most easier way.
function bond_two_array() {
	var a = [1, 3];
	var b = [5, 7];
	var c = a.concat(b);

	return c;
};

console.log( bond_two_array() );
```

* Combinarea la mai multe masive
```js
var main = function() {
	var a = [1];
	var b = [2];
	var c = [3];
	
	var d = [a, b, c];
	console.log(d);
}

console.log( main());
```

### Extern links :
* link 1
