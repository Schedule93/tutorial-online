### [return main page](../../README.md) | Chapter 7. Learn Git :

### Basic commands :
* `git pull` - update if you have worked from another place. Maybe other PC or Notebook.
* `git add .` - Add something
* `git status` - Check if added something
* `git commit -m "write something message"` - Write your changes
* `git push origin master` - Add on the Github or BitBucket
* `git log --pretty=oneline --abbrev-commit` - See history at git commits

### Addition commands :
* `git config` - ?
	* `git config user.name` - Показывает имя пользователя для git.
* `git merge vasha-vetka` Как обидинить ветки. Из главной ветки выполнить
* как удалять ветки в git.
	* delete branch locally `git branch -d localBranchName` or delete branch remotely `git push origin --delete name_branch`
* `git config --list` - ?
* `git checkout -f` - pur si simplu scrie n consola si ai sa vezi
* `git diff` - arată ceia ce tu cu adevărat ai schimbat, cod, ceva poate ai scris documentatie si asa mai departe.
* `git init` - probabil creza o reposiztorie probabil, nu sunt sigur vez documentation.
* `git reset` - ?
* `git branch` - Arată în care "branch" ești acum.

### Branch commands :
**The Notes :** Git `branch` trebuie pentru a lucra intr-o repozitorie secundara. Adica poate fii mai multe motive de ce noi vrem sa lucram asa. Una poate fii pentru a nu strica ceva, o chestiune, chestii sau poate noi lucram intr-o echipa shi feiecare lucreaza cu propriul lui repozitorie. Apoi verifica si le unesc in cel mai princil repozitorie care se numeste `master`. celelale repozitorii deja le alcatuiti dumneavoastra.
* `git checkout -b your_brnach` - Create a new branch
* `git checkout your_brnach` - Go to your branch
* `git branch -D main` delete local
* `git push origin main --delete` apoi transmite pe server
* `git checkout your_brnach` - Go from master branch to your branch
* `git add .` - add something
* `git status` - Check if added something
* `git commit -m "write something message"` - Write your changes
* `git push origin your_brnach` - Add on the Github or BitBucket
* `git push origin --delete your_branch` - delete a brach. da matinca trebuie sa fii in vetka care vrei sa stergi. Nu sunt sigur poate poti chiar din cea principala treb de vazut.
* `git checkout master` - switch back to master

### Rename main branch
When you want to rename main brnach so like GitHub, GitLab and even Bitbucket.
* `git checkout -b your_branch` - Create a new branch, or go to the local branch which you want to rename.
* `git checkout your_branch` - Go to your branch
* `git push origin your_branch`  - Obligatoriu, pentru ca să vadă pe Gitlab.
* `Settings/Repository/Default_Branch` - Go to Gitlab
* `git branch -m new_name` Rename the local branch by typing (skip)
* `git push origin -u new_name` Push the new_name local branch and reset the upstream branch. (skip)
* `git push origin --delete old_name` Delete the old_name remote branch.

### Links
* [Git no Shit](https://rogerdudler.github.io/git-guide/)
* [BitBucket Guide](https://www.atlassian.com/git)