### [return main page](../../README.md) | [Chapter 7. Learn Git : ](PAGE_GIT.md)

### Setting : 
* [PS1](PS1.md)
* Git config can be save it: 
	* `--system`
	* `--global`
	* `--local`
	* `--worktree`
	* `--file`
	
* конфигурационные файлы
	* `.bash_profile` :
	* `.bashrc`
	* `.bash_logout`
	* `.bash_history` - Dacă vrei să ştergi istoria din Terminal pe todeauna atunci trebuie să faci următorii paşii :
		1. Scrie în terminal `history -c`, pentru să ştergi istoria.
		2. Scrie în `terminal history -w`, pentru ca să nu apară din nou toată isoria veche. Adica history -w înlocueşte o lista noua a istoriei.
* `git config` - Showing list in terminal of words commands with config files.
* `git config --help` - Goes in your default browser to say about git config.
* `source .bashrc` : shtobi izminenie vstupili v silu
* `cat .gitconfig` - ?
* `git config --list` - ?

### Change long path in shell/bash terminal. 
Don't worry copy one of these in your terminal in see what made. If you don't like it. Close terminal and open again. He auto will restore default. I mean this is only for one session:
* `export PS1="Nicolai Cushnir : "`
* `export PS1="\e[0;32m\W $ \e[m";`

Dar dacă vrei global să fie pune în file `.bashrc` la sfîşitul fişierului acestuia. Pe ultimul rînd.

### Additional Links :
* Delete history commits but project save it. [Stack Overflow](https://stackoverflow.com/questions/13716658/how-to-delete-all-commit-history-in-github)
* [Git error : failed to push some refs to ](https://stackoverflow.com/questions/24114676/git-error-failed-to-push-some-refs-to)
* [Настройка терминала](https://pingvinus.ru/note/bash-promt)
* [Where system, global and local Git config files on Windows and Ubuntu Linux are](https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/Where-system-global-and-local-Windows-Git-config-files-are-saved)