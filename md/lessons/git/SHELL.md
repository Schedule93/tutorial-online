### [return main page](../../README.md) | [Chapter 7. Learn Git : ](PAGE_GIT.md)

* Să fac un proiect unde eu o să downlload automat toate repozitorile de pe GitHub, BitBucket și GitLab structurat așa cum fac eu manual.

### Introduction in Git Bash
Open your favorite `text redactor` and write:
* On the first line: `#! /bin/bash`
* Run the script `./file_name.sh`

### First Script Git Bash. It's for Linux users
```bash
# Here is a simple comment
str="Here is a simple text for me.";
echo "$str";
```

### Git shell it's for windows users.
```shell

```

### Additionaly links :
* [The Bash Hackers Wiki](https://wiki.bash-hackers.org/)
* [Git List](https://gist.github.com/RANUX/cb589d413687ca5bb6b69d9d2dbe7dc9)
* [SHELL BASH Основы работы в командном процессоре](https://www.youtube.com/watch?v=HwhMyGUGxZ0&list=PLLyG9JTjVd9VTEKisukGLJhl8H2YeIN09)
* [GIT BASH ГОРЯЧИЕ КЛАВИШ](https://gist.github.com/RANUX/cb589d413687ca5bb6b69d9d2dbe7dc9)