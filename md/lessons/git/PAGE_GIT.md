![IMG LEARN GIT](../../img/git-joy.png)

### [return main page](../../README.md) | Chapter 10. Learn Git :
* [Introduction](FIRST_STEPS.md)
* First Steps
* [Git : Write in terminal the commands](GUIDE.md)
* [Git Settings](SETTINGS.md)
* [Shell Scripts](SHELL.md)
* [Git Questions](GIT_QUESTIONS.md)
* [Перенос Строки в Git Bah](#)

### Additionaly links :
* [Tricks](https://about.gitlab.com/blog/2016/12/08/git-tips-and-tricks/)
* [Git no deep shit](http://rogerdudler.github.io/git-guide/)
* [Git Officail Ru](https://git-scm.com/book/ru/v1/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%9F%D0%B5%D1%80%D0%B2%D0%BE%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0-Git)
* [Stack Overflow : What is the Difference Between Mercurial and Git?](https://stackoverflow.com/questions/35837/what-is-the-difference-between-mercurial-and-git)
* [BitBucket Wiki](https://github.com/gitbucket/gitbucket/wiki)
* [Stack Overflow : Delete all history](https://stackoverflow.com/questions/13716658/how-to-delete-all-commit-history-in-github) && [Link 2](https://stackoverflow.com/questions/9683279/make-the-current-commit-the-only-initial-commit-in-a-git-repository)

