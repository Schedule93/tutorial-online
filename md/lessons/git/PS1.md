
### [return main page](../../README.md)

### Cum să schimbi PATH PS1 :
Pentru început poți să scrii în Git Bash `PS1="ceva text ... "`. Totuși ca rezultat o să-ți arăte `ceva text ... `. În loc de "ceva text ... " poți să scrii ce vrei tu. Deobicei e numele meu PS1="nicolai cushnir : $"

### Examples :
* `export PS1="\W "` - Arată ultima directoria unde te afli. Dar fără nici o culoare. 
* `export PS1="\e[0;36m\W "` Dar dacă vrei cu culoare, poți face așa. `s`
* `export PS1="\e[0;93m\W\e[m "` - Totuși mai bine închide la sfîrșit `\e[m`. Chiar dacă merge fără el. Cu acea diferență ca am vopsit în culoarea galbenă. `\e[0;36m`.
* `export PS1="\u "` - Arată numele utilizatorului. Dar fără culoare.
* `export PS1="\u \W "` - Arată numele utilizatorului dar și ultima directorie în care te afli. Drept că eu foloesesc în loc de `\u`, numele meu export PS1="nicolai \W ".
* `export PS1="\e[0;31m\u\e[m\e[0;93m \W\e[m "` - Arată numele meu colorat în culoarea roșii dar și arată în același moment ultima directorie în care mă aflu acum. fără simbol `\u` 

### Cum să colorezi în PS1:
Pentru a schimba diferite culori în `PS1` e nevoie de ceva simboluri specifice. Mai cocret. 
1. `\e[` - Începutul.
2. Apoi după ce urmează `\e[`, e novie să scrii culoarea necesară, spre exemplu 0;92. Vom avea așa `\e[0,92m`. 
3. \e[m — Sfîrșitu.
4. Tot împreună acum. `\e[0;36m nicolai \e[m` Cyan Color.

### List Number-Color:
![img](../../img/git_numbers_colors.png)

### Cum să pui global toate aceste setări :
Crează un fișier nou numit `.bashrc` cu redactorul text `nano` care este deja la "Git" și adaugă `export PS1="\e[0;31m\u\e[m\e[0;93m \W\e[m "` în directoria `C:\Users\<your user name>` sau scrie în terminal `~` pentru a crea acest fișier.
