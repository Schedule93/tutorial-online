![image](../../img/girl_glasses.png)

### [return main page](../../README.md) | Chapter 24. Learn Others :
**note** Лицензии можно комбинировать или изменить в будущее. Логично что если например измените лицензию из
бесплатной в проприетарное то люди не будуд платить потому что лицензия распространялась виде свободное.

## Licence Application
* [GNU_GPL](GNU_GPL.md)
* [MIT](MIT.md)
* [APACHE](APACHE.md)
* [BSD](BSD_LICENCE.md)
* [WTFPL](#)
* [BEERWARE](#)
* [Unlicense](#)
* [Copyright](LEARN_COPYRIGHT.md)
* [Mozila](MOZILA.md)
* Ms-PL(Microsoft Public License)
* Debian ?
* WTFPL

## Links :
* [Фонд свободного программного обеспечения](https://ru.wikipedia.org/wiki/%D0%A4%D0%BE%D0%BD%D0%B4_%D1%81%D0%B2%D0%BE%D0%B1%D0%BE%D0%B4%D0%BD%D0%BE%D0%B3%D0%BE_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE_%D0%BE%D0%B1%D0%B5%D1%81%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D1%8F)
* [Open Source - Stack Exchange](https://opensource.stackexchange.com/tags)
* [habra : Сравнение Open Source лицензий от GitHub](https://habr.com/ru/post/187540/)
* [habra : Свободные лицензии](https://habr.com/ru/post/40293/)
* [habra : Лицензия для вашего open-source проекта](https://habr.com/ru/post/243091/)
* [habra : Как выбрать Open Source лицензию для RAD фреймворка на GitHub](https://habr.com/ru/post/459732/)
* [Лицензия для вашего open-source проекта](https://habr.com/ru/post/243091/)
* [Boosts Licence Software](https://ru.wikipedia.org/wiki/Boost_Software_License)
* [Google Licence](https://support.google.com/a/answer/6309862?hl=ru)