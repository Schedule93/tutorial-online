### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) : 

### The command : 'RM'

* `rm -rf name_folder` - delete folders
* `rm -r dir` – удалить каталог dir
* `rm -f file` – удалить форсированно file
* `rm -rf dir` – удалить форсированно каталог dir

**Questions**

* Cînd vrem să ștergem fișiere/fișierul `test.txt` din folder. Dar folderul să rămînă ?
