### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) : 

### The Command : 'ls' 

*keys*

* `ls -A` - ?
* `ls -L` - Display default about list info.
* `ls -R ` - Показывает и отображает все файлы и подкаталоги. 
* `ls -T` - 
* `ls -X` - ?
* `ls -Z` - print any security context of each file
* `ls -1` - arată doar directorile fără informații suplimentare.
* `ls -r` - inversează de la ultimul element pînă la primul element la început.
* `ls -l` - Display list directories.
* `ls -p` - ?
* `ls -o` - ? 
* `ls -a` - Arată fișierele ascunse
* `ls -t` - Sort documentele , (not folders || Directory) după dată de la cele mai noi spre cele mai vechi. Care au orce extensii. De exemplu : txt, js, json, sh, bash și așa mai departe, nu contează de care. Principalul să fie document.  
* `ls -x .md .doc .jpg` - În ordinea alphabetică
* `ls -s` - sortează mai întîi cele care au un volum de memorie mai mare apoi cele care au un volum de memorie mai putin. 
* `ls ./ name_folder` - мы хотим посмотреть в текушийм каталоге
* `ls \bin\` - Display all catalogs
* `ls Documents/Developer/GitHub/` - din `cd ~` pot să văd ce conține alt fișier.
* `lscpu` - Display info about hardware 
* `ls -al` – форматированный список со скрытыми каталогами и файлами