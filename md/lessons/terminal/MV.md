### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) :

comman `mv` : rename or move files/folders

Dacă ești în același `folder` atunci : `mv test.txt text.txt`

Dacă ai fișierul life.txt și vrei săl redenumești atunci scrie : `mv life.txt test.txt`. Fii antent aici nu se pune apostrofe. Acest exemplu mai jos este greșit `mv 'life.txt', 'test.txt'`.

* `mv file1 file2` – переименовать или переместить file1 в file2. если file2 существующий каталог - переместить file1 в каталог file2

**links**

* [life on ubuntu](http://lifeonubuntu.com/how-to-move-or-copy-a-directory/)