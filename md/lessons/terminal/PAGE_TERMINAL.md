![IMG LEARN TERMINAL](../../img/naruto-friends.png)

### [return main page](../../README.md) | Chapter 9. Learn Terminal : 

### Chapter 9 : Learn how to use terminal.

**The Note** Есть много видов комадной строки, текстовые терминалы или оболочки которые переводится по англиский shell. Здесь мы выводим текст в окно устройство буть нутбук, компютер или даже телефон. Например PowerShell, Git Shell, vim, Emacs, Windows command line, и многие других для разных операционых систем. 
* If you use Operating System Windows you may use [Scoop Poweshell](SCOOP.md) because you will not see all path in terminal when use command `cd`.
* Dacă utilizezi comanda `cd` și dorești să treci în alt directoriu și ai un cuvînt format din 2 cuvinte atunci trebuie să pui ghilimele la început și sfîrșit. Exemplu `'cd Program Files'`.
* При открытии PowerShell обычно вы  нечего не видите но как только вы переходите в другую директорию в консоли показывать длинный путь. Например `C:\Users\Nicolai\Document\Developer\myproject`. Вы согласны с моим утверждением что это не красива. Вы можете использовать `scoop` для PowerShell, чтобы не показывать это.

**Bassic Commands Windows 10**
* `cd` : root directory
* `dir` : look in what catalog you are now.
* `cls` : clear screen 
* `cd.> test.txt` : create new document
* `cd ..` go to back directory
* `cp` : copy the file
* `echo` write something in terminal
* `del` : delete file or folder
* `rmdir` : soon will be answer ...
* `mkdir` : create a folder
* `ipconfig Ulim.md`
* `prompt Nicolai time = $t`
* `del rin.txt` Asta in caz daca file era creat
* copy con

**learn Linux Terminal**
* [cd](CD.md) : change directory
* [echo](ECHO.md) - Put something inside the document
* [whoami](WHOAMI.md) - Will show your username.
* [ls](LS.md) - Show the list directory
* [cat](CAT.md) - Show document
* [pwd](PWD.md) - Display all path
* [screenfetch](SCREENFETCH.md) - show info about your PC
* [hostname](HOSTNAME.md) - name the PC
* [snap](SNAP.md) - ?
* [man](MAN.md) - ?
* [clear](CLEAR.md) - clear the screen 
* [mv](MV.md) - rename files or move files
* [rm](RM.md) - remove file
* [rmdir](RMDIR) - delete emty folders
* [cp](CP.md) - copy files
* [touch](TOUCH.md) - create a document
* [exit](#) - exit from terminal or `ctrl-d`
* [find -name](#) - ?
* [grep](GREP.md)
* [apt-get](#) - ?
* [chmod](#) – change file/directory access permissions.
* [mkdir](MKDIR.md) - will create two folders : `mkdir 'Github', 'BitBucket'`
* [mount](#) - ?
* [chown](#) – change file/directory ownership.
* [df -h](#) - arată cît memorie o mai rămas ...
* [fdisk](#) : partition the disk. 
* [dmesg](#) - ?
* [inxi -r](#) - ? **important for postgres**
* `ln -s file` link – создать символическую ссылку link к файлу file
* mount
* `more file` – вывести содержимое file
* `head file` – вывести первые 10 строк file
* `tail file` – вывести последние 10 строк file
* `tail -f file` – вывести содержимое file по мере роста, начинает с последних 10 строк
* sudo -s
* lsblk

**Управление процессами**
* `ps` – вывести ваши текущие активные процессы
* `top` – показать все запущенные процессы
* `kill pid` – убить процесс с id pid
* `killall proc` – убить все процессы с именем proc
* `bg` – список остановленных и фоновых задач; продолжить выполнение остановленной задачи в фоне
* `fg` – выносит на передний план последние задачи
* `fg n` – вынести задачу n на передний план

**Права доступа на файлы**
* `chmod octal file` – сменить права file на octal, раздельно для пользователя, группы и для всех добавлением:

4 – чтение ®
2 – запись (w)
1 – исполнение (x)

***Примеры:***
* chmod 777 – чтение, запись, исполнение для всех
* chmod 755 – rwx для владельца, rx для группы и остальных.
* Дополнительные опции: man chmod.

**SSH**
* ssh user@host – подключится к host как user
* ssh -p port user@host – подключится к host на порт port как user
* ssh-copy-id user@host – добавить ваш ключ на host для user чтобы включить логин без пароля и по ключам

**Поиск**
* grep pattern files – искать pattern в files
* grep -r pattern dir – искать рекурсивно pattern в dir
* command | grep pattern – искать pattern в выводе command
* locate file – найти все файлы с именем file

**Системная информация**
* `date`– вывести текущую дату и время
* `cal` – вывести календарь на текущий месяц
* `uptime` – показать текущий аптайм
* `w` – показать пользователей онлайн
* `whoami` – имя, под которым вы залогинены
* `finger user` – показать информацию о user
* `uname -a` – показать информацию о ядре
* `cat /proc/cpuinfo` – информация ЦПУ
* `cat /proc/meminfo` – информация о памяти
* `man command` – показать мануал для command
* `df` – показать инф. о использовании дисков
* `du` – вывести ”вес” текущего каталога
* `free` – использование памяти и swap
* `whereis app` – возможное расположение программы app
* `which app` – какая app будет запущена по умолчанию
* `inxi -Fxz` - Показывает всю информацию о текущей системе.

**Архивация**
* tar cf file.tar files – создать tar-архив с именем file.tar содержащий files
* tar xf file.tar – распаковать file.tar
* tar czf file.tar.gz files – создать архив tar с сжатием Gzip
* tar xzf file.tar.gz – распаковать tar с Gzip
* tar cjf file.tar.bz2 – создать архив tar с сжатием Bzip2
* tar xjf file.tar.bz2 – распаковать tar с Bzip2
* gzip file – сжать file и переименовать в file.gz
* gzip -d file.gz – разжать file.gz в file

**Сеть**
* ping host – пропинговать host и вывести результат
* whois domain – получить информацию whois для domain
* dig domain – получить DNS информацию domain
* dig -x host – реверсивно искать host
* wget file – скачать file
* wget -c file – продолжить остановленную закачку
* `ip addr` - ?
* `dhclient -v name_interface` - ?

**Установка пакетов**
Установка из исходников:
* `./configure`
* `make`
* `make install`
* `dpkg -i pkg.deb` – установить пакет (Debian)
* `rpm -Uvh pkg.rpm` – установить пакет (RPM)

**Клавиатурные сочетания**
* `Ctrl + C` – завершить текущую команду
* `Ctrl + Z` – остановить текущую команду, продолжть с fg на переднем плане или bg в фоне
* `Ctrl + D` – разлогиниться, тоже самое, что и exit
* `Ctrl + W` – удалить одно слово в текущей строке
* `Ctrl + U` – удалить строку
* `!!` - повторить последнюю команду
* `exit` – разлогиниться

### links :
* [SHELL BASH Основы работы](https://www.youtube.com/watch?v=HwhMyGUGxZ0&list=PLLyG9JTjVd9VTEKisukGLJhl8H2YeIN09)
* [Linux command line](https://ss64.com/bash/)
* [5 Commands For Checking Memory Usage In Linux](https://www.linux.com/tutorials/5-commands-checking-memory-usage-linux/)
* [cmod](https://www.howtogeek.com/437958/how-to-use-the-chmod-command-on-linux/)
* [Stack Overflow : Differences between Emacs and Vim](https://stackoverflow.com/questions/1430164/differences-between-emacs-and-vim)
