### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) : 

### The command : Echo

* `echo` Tipărire. Analog comanda `console` de mai jos. 

```js
console.log("Here is a simple text in js");
```

* `*` Totul. Poate fii toate `files`, `folders`, `direcories`

* `echo hmmm > text.txt` Această comandă `>` arată ce trebuie de adăugat din partea stîngă. Poate fi `text`, `file`, `folders`, `directory` și deja în partea dreaptă arată unde trebuie de adăugat. Cu alte cuvinte el doar arată unde să trimată acest `text`.

* `echo 'here is a simple text for me' > text.txt` - Aici este un alt exemplu. Fii atent the file `text.txt` trebuie să fie creat că așa nu o să meargă.

* `>>`

* `<`

* `|`

* `~/.BASH_PROFILE`