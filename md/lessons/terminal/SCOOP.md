### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) :

### Install Scoop in PowerShell Windows 10

1. Open the command line administrator (x32)
2. write in cmd `powershell`
3. Sometimes `cmd` poate sa nu lucreze. Si poate sa scrie ceva de gen `is not recognised`. incearcă sa scrii nu in cmd ci in powershell. La mine asa si a fost. Sau incearca să adaugi la `path`, `переменые среды`.

### Copy and write commands in cmd

* Set-ExecutionPolicy RemoteSigned -scope CurrentUser
* iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
* set-executionpolicy -s cu Unrestricted
* scoop help
* scoop install curl
* scoop bucket add extras
* scoop install grep
* scoop install concfg
* **in caz daca nu merge foloseste aceasta comanda. Dar daca merge totul merge nu instala ca o sa ai probleme.** `concfg import solarized-dark` 
* scoop install pshazz
* scoop install touch
* scoop install sudo
* scoop search

### Basic command PowerShell

* `Get-Childitem` - similar commands `ls` or `dir`
* `Get-Command -Name *dns*` - see list where is all commands with this word. Change word `*dns*` with word which you need.
* `Get-Process` - watch all processes in terminal
* `Get-Command ` - list commands PowerShell
* `Get-Command -verb Get` - commands which begin with get
* `?` - soon wil be more commands ...

### Questions
* How to chnage title PowerShell ? seem lik cmd `title` 
