### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) : 

### The command : cd

* `cd -` : return prevedushia command
* `cd` : root directory
* `cd ~` : return main directory
* `cd c:\` : Go to disk C

### Tricks

* `cd / && ls -l` * Arată toate fișierele din rădăcina sistemului de operare Linux altfel spus root folders așa ca `bin`, `boot`, `home`, `lib`, `cmd`, `dev`, `usr` și altele.
