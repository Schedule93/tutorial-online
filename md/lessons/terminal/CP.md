### [return main page](../../README.md) | [Chapter 6. Learn Terminal](PAGE_TERMINAL.md) : 

**Copy File(s)**

Pentru a învăța această comandă mai bine repetă totul cum este scris mai jos. Să presupunem că noi suntem în direcoriu `Desktop`. Și avem un folder cu denumirea `learn-terminal`. Înăuntru lui avem încă două fișiere(folders) `one` și `two`. În folderul one am creiat un document de tip text cu denumirea `text.txt`. Apoi am adăugat ceva text în acest document. Acum urmează cel mai important copierea documentului. Pentru aceasta facem în felul următor. `cp one/test.txt two`

În caz cînd noi ne aflăm în direcoriul `learn-terminal` și dorim să copiem dintr-un fișier în altul facem în felul următor. `cd two && touch text.txt`

Să luăm un alt exemplu. Cînd ne aflăm în directoriul `learn-terminal` atunci scrie : `cp one/text.txt two/text.txt`

**How to copy multiples files from folder**

`cp *.txt foo/` Acestă steluță `*` înseamnă totul din fișierul(folder) `foo/`. 

*Keys*

* `--attributes-only` - не копировать содержимое файла, а только флаги доступа и владельца;
* `-f`, `--force` - перезаписывать существующие файлы;
* `-i`, `--interactive` - спрашивать, нужно ли перезаписывать существующие файлы;
* `-L` - копировать не символические ссылки, а то, на что они указывают;
* `-n` - не перезаписывать существующие файлы;
* `-P` - не следовать символическим ссылкам;
* `-r` - копировать папку Linux рекурсивно;
* `-s` - не выполнять копирование файлов в Linux, а создавать символические ссылки;
* `-u` - скопировать файл, только если он был изменён;
* `-x` - не выходить за пределы этой файловой системы;
* `-p` - сохранять владельца, временные метки и флаги доступа при копировании;
* `-t` - считать файл-приемник директорией и копировать файл-источник в эту директорию

* cp file1 file2 – скопировать file1 в file2
* cp -r dir1 dir2 – скопировать dir1 в dir2; создаст каталог dir2, если он не существует
