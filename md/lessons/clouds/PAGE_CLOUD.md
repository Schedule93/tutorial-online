* ![image](../../img/anime-manga-anime-girls.png)

### [return main page](../../README.md) || Chapter 30. Learn Clouds :

### Learn Clouds Technologies :
* [Heroku](HEROKU.md)
* [Amazon : AWS Docs](AMAZON_CLOUD.md)
* [Google Cloud](GOOGLE_CLOUD.md)
* [Github Hosting](#)
* [Digital Ocean](#)
* [Microsoft Azure](#)
* [G Suite](G_SUITE.md)
* [Notion](NOTION.md)
* [Jira](JIRA.md)

### Extern Links :
* [Clouds YouTube](https://www.youtube.com/watch?v=8jbx8O3wuLg&list=PLg5SS_4L6LYsxrZ_4xE_U95AtGsIB96k9&ab_channel=ADV-IT)
