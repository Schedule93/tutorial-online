### [return main page](../../README.md) | [Chapter 24. Learn Hardware : ](PAGE_HARDWARE.md)

### Требование для сервера. Думаю.
* `Dell R440 Xeon Gold`
* `HPE Proliant 360 gen 10`
* `HPE Apollo 4200 Gen 9 Server`
* `Supermicro superserver 5019p-mt`

### Extern Links :
* [Обзор сервера HPE ProLiant DL380 Gen10](https://www.proliant.ru/art/review_hpe_proliant_dl380_gen10.html)
* [Buy HPE PROLIANT DL380 GEN10 SERVER](https://servermall.com/sets/hp-dl380-gen10-server/?utm_source=google&utm_medium=cpc&utm_campaign=EU_Search_Servers_All_Febtest.19885750998&utm_content=HP_DL380_Gen10.148292722220.ad.&utm_term=hp%20gen10%20dl380&gclid=CjwKCAjw4ZWkBhA4EiwAVJXwqRH5LCH0-guSft9CDSLxfnQQX869c5crmyc_07p3eNgAPlPRueYxaxoC2SAQAvD_BwE)
