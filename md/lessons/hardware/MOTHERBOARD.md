### [return main page](../../README.md) | [Chapter 24. Learn Hardware : ](PAGE_HARDWARE.md)

### Motherboards for Desktop : 
* Motherboard **ASUS ROG Zenith II Extreme** 
	* Processor (`AMD Ryzen Threadripper 3990x`)
	* Chipset (`AMD TRX40`) 
	* CPU Socket (`Socket TRX4`) 
	* RAM (`DDR4 256 GB`)
* Motherboard **MSI MEG X570 Ace**
	* Processor (`?`)
	* Chipset (`?`) 
	* CPU Socket (`?`) 
	* RAM (`?`)

### Extern Links : 
* [M3A785GMH/128M - ASRock](https://www.asrock.com/mb/AMD/M3A785GMH128M/index.asp) || [характеристики](https://www.asrock.com/mb/AMD/M3A785GMH128M/index.ru.asp)
* [gigabyte](https://www.gigabyte.com/Motherboard/AMD-Socket-sTRX4) 256GB MAX - da nii imi treb 1TB
* [WS C621E SAGE](https://www.asus.com/Commercial-Servers-Workstations/WS-C621E-SAGE/)
* ASUS ROG Zenith II Extreme Alpha TRX40 579$
* Asus ROG Maximus XII HERO
* ASUS ROG Strix X570-E (Dislike) But i am not sure!
* Asus ROG Crosshair VIII Hero => Chipset = AMD x570