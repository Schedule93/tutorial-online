![IMG LEARN HARDWARE](../../img/jiraiya.png)

### [return main page](../../README.md) | Chapter 24. Learn hardware :

### Devieces
* Notebooks { Model : Lenovo 80XR }
* PC
* Принтеры
* Servers
* Переменый удленитель

### Hardware Components PC
* [Chipset](CHIPSET.md)
* [Sockets](#)
* [CPU](CPU.md)
* [Motherboard](MOTHERBOARD.md)
* [Monitors](MONITORS.md)
* [GPU](GPU.md)
* [RAM](RAM.md)
* [PSU](PSU.md)
* [Coolers](#)
* [Wires](#)
* [Box](BOX.md)
* [OSH Vs CH](OSH_VS_CH.md)
* [Personal Server](SERVER.md)
* [Cluster](CLUSTER.md)
* [Tasks](TASKS_HARDWARE.md)
* [Questions](QUESTIONS_HARDWARE.md)
* [HDD and SSD](#)
* Источник бесперебойного питание
* Твердотельный накопитель
* [Сетевой Адаптер](NETWORK_ADAPTER.md)
* [Шины для процессоров](BUSSES_FOR_PROCESSORS.md)
* [PCI Express](PCI_EXPRESS.md)

### Links
* [Amazon : HPE ProLiant DL380 Gen10](https://www.amazon.com/ProLiant-Server-Processor-Memory-Factor/dp/B085J428XM/ref=sr_1_13?crid=91Z2Y2S6LM44&keywords=HPE%2BProliant%2B360%2Bgen%2B10&qid=1647445736&sprefix=%2Caps%2C462&sr=8-13&th=1)
* [What ? Ru Doc AMD](https://www.amd.com/ru/products/chipsets-motherboards-desktop)
* [The 10 New Coolest Enterprise Servers Of 2020 (So Far)](https://www.crn.com/slide-shows/data-center/the-10-new-coolest-enterprise-servers-of-2020-so-far-/1)
* [List of open-source hardware projects](https://en.wikipedia.org/wiki/List_of_open-source_hardware_projects)
* [Librem](https://ru.wikipedia.org/wiki/Librem)
* [Аппаратная платформа компьютера](https://ru.wikipedia.org/wiki/%D0%90%D0%BF%D0%BF%D0%B0%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BB%D0%B0%D1%82%D1%84%D0%BE%D1%80%D0%BC%D0%B0_%D0%BA%D0%BE%D0%BC%D0%BF%D1%8C%D1%8E%D1%82%D0%B5%D1%80%D0%B0)
* [Архитектура компьютера](https://itnan.ru/post.php?c=1&p=449190)
* [выбор монитора](https://andiriney.ru/kharakteristiki-monitorov/)
* [Wiki : Открытое аппаратное обеспечение](https://ru.wikipedia.org/wiki/%D0%9E%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%BE%D0%B5_%D0%B0%D0%BF%D0%BF%D0%B0%D1%80%D0%B0%D1%82%D0%BD%D0%BE%D0%B5_%D0%BE%D0%B1%D0%B5%D1%81%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5)
* [Dell Buy Server : PowerEdge R440 Rack Server](https://www.dell.com/en-us/work/shop/productdetailstxn/poweredge-r440)
* [???](https://lifehacker.ru/raspberry-pi/#:~:text=%D0%9D%D0%B5%D1%81%D0%BC%D0%BE%D1%82%D1%80%D1%8F%20%D0%BD%D0%B0%20%D1%82%D0%BE%20%D1%87%D1%82%D0%BE%20Raspberry,%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82%2D%D1%81%D1%91%D1%80%D1%84%D0%B8%D0%BD%D0%B3%D0%B0%20Raspberry%20Pi%20%D1%85%D0%B2%D0%B0%D1%82%D0%B8%D1%82.)
* [5 best HP rack server to buy](https://windowsreport.com/best-hp-rack-server/)
* [Wiki : ?](https://en.wikipedia.org/wiki/System_on_a_chip)
