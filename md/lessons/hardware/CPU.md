### [return main page](../../README.md) | [Chapter 24. Learn Hardware : ](PAGE_HARDWARE.md)

### Basic components CPU :
**note** "Когда процессоры запускались на одном ядре, оно полностью отвечало за обработку данных, которые передавались на процессор. Чем больше ядер встроено в ЦПУ, тем больше они способны распределять его задачи. Это делает процессор быстрее и эффективнее. Очень важно понимать, что процессор отвечает только за исполнение задач, как и софт".

**note 2** за что отечает дапустим 3GHz или 3000 мегаХерц ? Не важно цыфра важна герцовка . Что делает ?

**note 3** Из чего состоит процессор ?
* cores : 
* threads : 
* cache :
* memory-cash : 
* sokets

## Критерии выбора процессора
**Количество ядер** Современные процессоры производятся с несколькими ядрами (до 24 штук) для ускорения работы. Количество ядер влияет на число одновременно обрабатываемых запросов. Но бездумно увеличивать ядерность не стоит, учитывайте тип выполняемых задач. При большом количестве однопоточных задач (используют только одно ядро одного процессора) лучше увеличить частоту, а не количество ядер.

**Кэш** Кэш процессора — небольшой объем сверхбыстрой памяти. Он используется для хранения оперативной информации и влияет на быстродействие. Купить сервер с процессором Intel Xeon лучше с большим кэшем, что увеличит скорость работы приложений. Оптимальный объем кэша — 8−16 Мб.

**Сокет** Тип сокета влияет на совместимость процессора с материнской платой сервера. Убедитесь в совместимости сокета, иначе процессор не будет работать. Поврежденные или бракованные сокеты приводят к нестабильной работе процессора и снижают быстродействие.

**Тактовая частота** Тактовая частота означает число выполняемых вычислений в секунду. Ориентируйтесь на нее при большом количестве однопоточных задач. Иначе смотрите на число ядер и размер кэша. Скорость многоядерных процессоров определяется архитектурой и числом выполняемых за такт команд.

**Отвод тепла** Количество выделяемого тепла зависит от мощности процессора и работы системы охлаждения. При недостаточном охлаждении процессор перегревается, замедляется, возможен выход из строя. С процессором Intel Xeon такие проблемы возникают редко.

### Modeles of CPU Open Source Vs Comercial
**OpenSPARC

not be soon ...

**RISC-V

not be soon ...

**Open Risc

not be soon ...

za shto platiti ? ne ponel. (comercial CPU)


### Models of Processors:
* Scan 3XS SER-T25. A beast driven by twin Xeon processors. ...
* Asus TS500. A mainstream tower server with flexibility in spades.
* Threadreapear x3990

### Very expensive at money and performance CPU
* AMD Ryzen Threadripper 3990X at Socket sTRX4.
* Intel core i910900K

### Links :
* [Google](https://www.google.com/search?sxsrf=ALeKk02rOBlEDCwdPC14IYxr2ncN2Zh2Vg%3A1603169190672&ei=pmuOX5_TIsKprgSdkJfoBg&q=processors+open+source&oq=processors+open+source&gs_lcp=CgZwc3ktYWIQAzIICAAQBxAFEB4yCAgAEAUQChAeMgYIABAIEB4yBggAEAgQHjIICAAQCBAKEB4yBggAEAgQHjIGCAAQCBAeOgYIABAHEB46BAgAEB46BggAEAUQHlChjwFYnJ8BYKq1AWgAcAF4AIABgQGIAasJkgEEMC4xMJgBAKABAaoBB2d3cy13aXrAAQE&sclient=psy-ab&ved=0ahUKEwifvvnQrsLsAhXClIsKHR3IBW0Q4dUDCA0&uact=5)
* [habra](https://habr.com/ru/company/galtsystems/blog/301590/)
* [List of open-source computing hardware](https://en.wikipedia.org/wiki/List_of_open-source_computing_hardware)
* [Wiki : Risc-V На русском](https://ru.wikipedia.org/wiki/RISC-V)
* [AMD Epyc 7742 vs AMD Ryzen Threadripper 3990X](https://versus.com/en/amd-epyc-7742-vs-amd-ryzen-threadripper-3990x)
* [YouTube](https://www.youtube.com/watch?v=ElTlxJB5t2c&ab_channel=HardwareRump)
* [AMD](https://www.amd.com/en/products/cpu/amd-ryzen-threadripper-3990x)
* [Wiki](https://ru.wikipedia.org/wiki/%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D0%B0%D1%80%D1%85%D0%B8%D1%82%D0%B5%D0%BA%D1%82%D1%83%D1%80%D0%B0_%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%B8)
* [Google List Open Source Processors](https://www.google.com/search?q=list+open+soirce++processors&sxsrf=ALeKk031UkrgLpoxtJrMoMPk4zByn8ezUA%3A1619131569420&ei=sfyBYN2PGZGvrgTyjJGYBQ&oq=list+open+soirce++processors&gs_lcp=Cgdnd3Mtd2l6EAM6BwgAEEcQsAM6BAgAEA06BggAEA0QHjoICAAQDRAFEB46CAgAEAgQDRAeOgQIIRAKUNtaWKNtYORuaAFwAngAgAF_iAHxB5IBAzAuOZgBAKABAaoBB2d3cy13aXrIAQjAAQE&sclient=gws-wiz&ved=0ahUKEwidrKiQ95LwAhWRl4sKHXJGBFMQ4dUDCA4&uact=5)