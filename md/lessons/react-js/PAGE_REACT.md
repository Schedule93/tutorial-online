![IMG LEARN REACT](../../img/fate-night.png)

### [return main page](../../README.md) | Chapter 16. Learn React :

### Topics :
* [Intro in React.js](INTRO_REACT.md)
* [React-Native](https://reactnative.dev/)
* [Redux](https://redux.js.org/)
* [JSX](#)

### Links
* [styled-components](https://frontend-stuff.com/blog/styled-components/#:~:text=%D1%81%D0%B5%D0%BB%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D1%8B%20%D0%B8%20%D0%B2%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D1%8C-,%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5,CSS%2C%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B9%20%D0%BE%D0%B3%D1%80%D0%B0%D0%BD%D0%B8%D1%87%D0%B5%D0%BD%20%D0%BE%D0%B4%D0%BD%D0%B8%D0%BC%20%D0%BA%D0%BE%D0%BC%D0%BF%D0%BE%D0%BD%D0%B5%D0%BD%D1%82%D0%BE%D0%BC.&text=Styled%20Components%20%D0%BF%D0%BE%D0%B7%D0%B2%D0%BE%D0%BB%D1%8F%D1%8E%D1%82%20%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C%20%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B9,%D0%B1%D0%B5%D1%81%D0%BF%D0%BE%D0%BA%D0%BE%D1%8F%D1%81%D1%8C%20%D0%BE%20%D0%BA%D0%BE%D0%BD%D1%84%D0%BB%D0%B8%D0%BA%D1%82%D0%B0%D1%85%20%D0%B8%D0%BC%D0%B5%D0%BD%20%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%BE%D0%B2.)
* [React.js для новичков](https://skillbox.ru/media/code/reactjs-dlya-novichkov-v-programmirovanii-chto-eto-kak-ustroen-i-zachem-nuzhen/)
* [Учебный курс по React, часть 23: первое занятие по работе с формами](https://habr.com/ru/company/ruvds/blog/443214/)
* [Examples Projects](https://reactjs.org/community/examples.html)
* [Привет Мир](https://ru.reactjs.org/docs/hello-world.html)
* [Путь Самурая](https://www.youtube.com/watch?v=gb7gMluAeao&list=PLcvhF2Wqh7DNVy1OCUpG3i5lyxyBWhGZ8&ab_channel=IT-KAMASUTRA)
https://www.ukad-group.com/blog/5-reasons-to-hate-react-native/
* [Recompouse](https://github.com/acdlite/recompose)
* [Flow](https://flow.org/) - with react.js is very well choice.
