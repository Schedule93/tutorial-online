### Topic Radical
Înmulțirea e o adunare repetată dar radicalul este o îmulțire repetată. De exemplu avem `8 * 3 = 24`, iar 8 * 8 * 8 = 64, asta si este radical. Altfel spus radical din 8 la 3 = 64

* Radical din `sqrt{25} = 5`

```math
\sqrt{25} = 5
\\ 2 ^ 2 = 4
\\ x + y = z || a + b = c
```

5 ^ 2 = 25 altfel spus `5 * 5 = 25`

### Video :
* [Algoritmul extragerii radacinii patrate](https://www.youtube.com/watch?v=Is0MClHJAzE&ab_channel=prof.IoanURSU)
* [Отрицательный корень](https://www.youtube.com/watch?v=YZN-8PMRvxw&list=PLSsIYv8E1ENzyVOIgmXQxTNNa4l8Uxoed&ab_channel=TutorOnline-%D1%83%D1%80%D0%BE%D0%BA%D0%B8%D0%B4%D0%BB%D1%8F%D1%88%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2)
* [math in romanian](https://www.youtube.com/watch?v=Fa7iCedwfmA)

### Links :
* Link 1
* Link 2