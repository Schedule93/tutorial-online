### Return

### Semnele la Algebră
**adunare**
* (+) + (+) = (+)
* (+) + (-) = (?)
* (-) + (+) = (?)
* (-) + (-) = (?)

**scădere**
* (+) - (+) = (?)
* (+) - (-) = (?)
* (-) - (+) = (?)
* (-) - (-) = (?)

**imulțire**
* (+) * (+) = (+)
* (+) * (-) = (-)
* (-) * (+) = (-)
* (-) * (-) = (+)

**împărțire**
* (+) / (+) = (+)
* (+) / (-) = (-)
* (-) / (+) = (-)
* (-) / (-) = (?)

Mai încolo să faci exemple cu aceste formule de mai sus. \
Uitete la profesorul online acolo trebuie sa fie youtube

### Exemple
* `3 * (-2) = -6`
* 