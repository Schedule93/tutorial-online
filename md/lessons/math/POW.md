### return

### Topic : POW or radical

### numar la patrat
De exemplu: `2 ** 10` asta e `2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2` (adica de 10 ori luat cifra 2)
Sau un alt exemplu: `3 ** 2` asta e `3 * 3` sau `7 ** 2` egal 49

**radical din ordinul 2 sau radacina patrata**
radical din 100 = 10
radical din din 4 = 2
radical este antonimul la 7 la puterea a 2 

### Questions
* `Pow` and `Square` are identical words or they have different sense?

### Links
* [Wiki : Radical](https://ro.wikipedia.org/wiki/Radical_(matematic%C4%83)