![IMG LEARN MATH](../../../public/img/anime-wonderful-face.png)

### [return main page](../../../README.md) | Chapter 15. Learn Mathematics

> Математика довольно большая наука, которую следует осваивать последовательно и очень медленно.

> Изучая математику и решая задачи, мы учимся обобщать и выделять важное, анализировать и систематизировать, находить закономерности и устанавливать причинно-следственные связи, рассуждать и делать выводы, мыслить логически, стратегически и абстрактно

### Chapter 1. Algerbra
* [Aritmetica](ARITHMETIC.md)
* [Semnele la Algebră](SIGNS_IN_ALGEBRA.md)
* [Ecuatii de gradu 1](FDE.md)
* [Ecuatii de gradu 2](EOTSP.md)
* [Radical](RADICAL.md)
* [Матрица](MATRICES.md)
* [Logarithm](LOGARITHM.md)
* [Putere](POW.md)
* [Вычисление пределов функций](#)
* [Epsilon](EPSILON.md)
* [Интегралы](#)
* [Questions](QUESTIONS.md)

### Chapter 2. Geometria
* [Геометрия](https://www.youtube.com/watch?v=hqTB9PTKvXU&list=PLlx2izuC9gjgZsBALx2ZF7IMGoNl07A9l)
* Система Координат ![image](../../img/math_cordinates.png)
* Fractal Geometry

### Chapter 3. Анализ Алгебры
* Lesson 1
* Lesson 2

### Chapter 4. Матиматический Анализ
* Lesson 1
* Lesson 2

### Chapter 5. Высшая Алгебра 
* Lesson 1
* Lesson 2

### Chapter 6. Аналитическая геометрия 
* Lesson 1
* Lesson 2

### Chapter 7. Линейная алгебра
* [YouTube : 3Blue1Brown Русский](https://www.youtube.com/watch?v=RNTRYicPvWQ&list=PLQqEY2kzSbZ6Md75EImpVXsQdbCQm1iq7&index=12)
* Lesson 2

### Chapter 8. Алгебраическая Геометрия
* Lesson 1
* Lesson 2

### Chapter 9. Вычислительная Математика
* Lesson 1
* Lesson 2

### Chapter 10. Линейное программирование
* Lesson 1
* Lesson 2

### Chapter 11. Дифференциальная Геометрия
* Lesson 1
* Lesson 2 

### Дискретная Математика (Must Have for Programming)
* [YouTube : dUdVstud](https://www.youtube.com/watch?v=f45Dzd8SUZU&list=PL5H0RoiuOF5-SGZ5X8hSFzp7l-2lcrOFL)
* Теория Графы
* Теория Множеств
* Теория вероятности
* Теория Чисел
* Теория Уравнение
* Математическая логика
* Комбинаторику

### Read Books
* [Великая Теорема Ферма](https://www.e-reading.club/bookreader.php/85558/Singh_-_Velikaya_Teorema_Ferma.html)
* [Математика для программиста: советы, разделы, литература](https://proglib.io/p/how-to-learn-maths/)

### YouTube :
* [Matematica pe intelesul tau](https://www.youtube.com/watch?v=ToGjBRKKSo0&list=PLdzCeGVYt4JFpcGNQMTR_b0UWRcA7iMZm)
* [Алгебра на русском](https://www.youtube.com/watch?v=pecXY93nAPM&list=PLCB7E37537330650A)
* [Геометрия](https://www.youtube.com/watch?v=f2AYAqDZ9Lo&list=PLB8172C0D46A230EF)
* [3Blue1Brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw/videos)
* [YouTube Изучаем математику с нуля Begin Study](https://www.youtube.com/watch?v=_Jjg4flabkU&list=PLto66BxvYZX2dxkJvb4q9Up91aoObss-n&ab_channel=BeginStudy)
* [YouTube : Изучаем математику с нуля](https://www.youtube.com/watch?v=_Jjg4flabkU&list=PLto66BxvYZX2dxkJvb4q9Up91aoObss-n&index=1)
* [Парадокс Монти Холла](https://www.youtube.com/watch?v=Razdc5Palss&list=PLSsIYv8E1ENxIhUE_eayScddkrAQ8bnKc&ab_channel=TutorOnline-%D1%83%D1%80%D0%BE%D0%BA%D0%B8%D0%B4%D0%BB%D1%8F%D1%88%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2)
* [Exponent](https://www.youtube.com/watch?v=Zt2fdy3zrZU)
* [N Eliseeva](https://www.youtube.com/watch?v=zkTFyZVav-s&list=PLGtfmJuN1mTD2ND-zeBWHv3ACO5J0G9DR)

### Links : 
* [Математика с нуля](http://spacemath.xyz/)
* [Лямбда-исчисление](https://ru.wikipedia.org/wiki/%D0%9B%D1%8F%D0%BC%D0%B1%D0%B4%D0%B0-%D0%B8%D1%81%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5)
* [Как происходит преобразование аналогового сигнала в цифровой](http://electrik.info/main/praktika/1409-preobrazovanie-analogovogo-signala-v-cifrovoy.html)
* [Formule Geometrice](https://www.mateonline.net/geometrie.htm)
* [Formule Algebra](https://www.mateonline.net/matematica/28/s/Formule_Algebra.htm)
* [Wiki Ro : numere raționale](https://ro.wikipedia.org/wiki/Num%C4%83r_ra%C8%9Bional)
* [List formula](https://www.matematica.pt/en/useful/math-formulas.php)