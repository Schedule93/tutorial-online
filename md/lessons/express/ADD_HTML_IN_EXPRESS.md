### ADD HTML IN EXPRESS
În cazul meu: Eu am creat un proiect care se numeste `find-job`, ap eaca, înăuntru la proiectul meu am creat un folder inauntru cu denumirea `client`. Inautru la client eu am facut inca un folder `HTML` unde o sa pastrez toate HTML files. O să arăte așa: `find-job/client/html/JOBS.html`.

În plus la asta eu am făcut așa, ca lîngă folderul cu denumirea de HTML să fie încă așa folderuri ca: CSS, JS, IMG. O să arăte așa:
* HTML
* CSS
* JS
* IMG

Asta tot stă în folderul principal client. Să nu uităm! Acum mai concret.

Eu am creat in root directory la project un fișier cu denumirea de `GLOBALJOB.js`

```js
app.use(express.static("client/html"));
```  

### Extern links :
* [Serving static files in Express](https://expressjs.com/en/starter/static-files.html)
* [Express examples](https://expressjs.com/en/starter/examples.html)