
### error handler

* Exemplu 1.

```js
// Error handler
app.get('/error_handler', function(req, res, next) {
  
  try {
  	throw new Error('Now was made a error special. 2');
  } catch(error) {
  	next(error);
  } 
});
```

* exemplu 2.

```js
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
```