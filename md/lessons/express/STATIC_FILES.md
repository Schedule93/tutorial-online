### [return main page](../../README.md) | [Chapter 12. Learn Express.js :](PAGE_EXPRESS_JS.md)

**Cînd file: HTML este** în folder-ul principal, adica `root project`.

Așa arată structura la proiect

```Algorithm
docs/
node_modules/
public/
server.js
```

Mai jos este arătat codul:

```js
var express = require("express");
var app = express();
var PORT = 27; 

app.use(express.static("public"));

// routes
app.get("/", function(req, res) {
	res.sendFile(__dirname + '/reprod.html');
});

app.listen(PORT, function () {
	console.log("Server-ul lucrează pe port-ul: " + PORT);
});
```

Explicație: 

### Below Must Explain: Why ?

* Dacă vrei să lucreze din folder `server`, la acelasi nivel ca `public`. Defapt rout-ul meu se află în directoria `server/routes/main_routes/main_routes.js` dar HTML-ul se află îm `public/html/main/TUTORIAL_ONLINE.html`

```js
res.sendFile(path.join( __dirname, "../public", "./../../../public/html/main/TUTORIAL_ONLINE.html") );
```

Dar pentru ca să connectez CSS files, ai nevoie de:

* Unu: Să fii convins că pentru CSS files în `root project` este:

```js
    // Trebuie pentru ca să lucreze CSS Files
	app.use(express.static('public'))
```

* Doi: Apoi la CSS conectează:

```css
    <link rel="stylesheet" href="./css/HEADER.css" />
```
