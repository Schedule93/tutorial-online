![image](../../../public/img/anime-amazed-faces.png)

### [return main page](../../../README.md) | Chapter 13. Learn Express.js : 

### Express.js Topics:
* [Export Modules](https://www.sitepoint.com/understanding-module-exports-exports-node-js/)
* [Add a HTML file](ADD_HTML_IN_EXPRESS.md)
* [Connect to CSS](CONNECT_CSS_IN_EXPRESS.md)
* [Modules](MODULES.md)
* [Static Files](STATIC_FILES.md)
* [Routes](ROUTES.md)
* [Middleware](MIDDLEWARE.md)
* [Error Handler](ERORRS_HANDLER.md) Обработчик Ошибки
* [funcția `Next();`](NEXT.md)
* [funcția `app.use();`](APP_USE.md)
* [funcția `app.set();`](APP_SET.md)
* [`set();` vs `use.();`](SET_VS_USE.md)
* [Errors](ERRORS.nd)
* [SESSION](SESSION.md)
* [Questions](QUESTIONS.md)
* [Tasks](TASKS.md)
* [Methods Express](METHODS_EXPRESS.md)
* [CRUD](CRUD.md)
* [Req Params](REQ_PARAMS.md)
* [Structure](STRUCTURE.md)
* [HTTP Status](HTTP_STATUS.md)


### Return at Node.js topics:
For Know Express.js must have know Node.js topics 
1. File System
1. Асинхронное программирование
1. Promise
1. Обработчик Ошибки

### Extern links :
* [Stack Overflow : What does middleware and app.use actually mean in Expressjs?
](https://stackoverflow.com/questions/7337572/what-does-middleware-and-app-use-actually-mean-in-expressjs)
* [Stack Overflow : NodeJS / Express: what is “app.use”?](https://stackoverflow.com/questions/11321635/nodejs-express-what-is-app-use)
* [jsman.ru/express](https://jsman.ru/express/#express----nodejs--)
* [GitHub : Express.js](https://github.com/Apress/pro-express.js/tree/master/proexpressjs)
* [Forms, File Uploads and Security](https://www.sitepoint.com/forms-file-uploads-security-node-express/)
* [Get Started](https://scotch.io/tutorials/getting-started-with-node-express-and-postgres-using-sequelize)
* [CRUD](https://zellwk.com/blog/crud-express-mongodb/)
* [Static Files](https://www.youtube.com/watch?v=8lY9u3JFr5I&list=PLQqEY2kzSbZ4NMd7xsuc28a6Kc-_300Jb&index=21)
* [Validation](https://www.freecodecamp.org/news/how-to-make-input-validation-simple-and-clean-in-your-express-js-app-ea9b5ff5a8a7/)
* [YouTube : Express.js - Static Files](https://www.youtube.com/watch?v=8lY9u3JFr5I&list=PLQqEY2kzSbZ4NMd7xsuc28a6Kc-_300Jb&index=1&ab_channel=Nodecasts)
* [GitHub : pro-express](https://github.com/Apress/pro-express.js/tree/master/proexpressjs)
*

