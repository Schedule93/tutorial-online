### [return main page](../../README.md) | [Chapter 9. Learn Express.js :](PAGE_EXPRESS_JS.md)

### For routes you must know :
1. Create a new **folder** with name `routes`
1. Go to folder `routes`
1. Create here a new **file** - `route.js`.
1. Write this code:
```js
var router = require('express').Router();
var path = require("path");

// Routes :
router.get("/", function(req, res) {
	res.send("<h2> Here is main page </h2>");
});

module.exports = router;
```
1. From file `main.js` connect `route.js`
```js
var path = require("path");
const router = require("./routes/route.js");
app.use("/", router);
```	 

### Exemples :
* [Exemple 1](#) - from main page or root project
* [Exemple 2](#) - from 
* [Exemple 3](#) - from path `public/html/main/main.html`