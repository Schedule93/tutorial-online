### [return main page](../../README.md) | [Chapter 9. Learn Express.js :](PAGE_EXPRESS_JS.md) 

### App.use();

**Very Important** Middleware este pentru a conecta CSS, HTML, JS! Să ții minte. pentru fiecare din ele trebuie app.use(); aparte.

**Warrning 1** Промежуточное ПО, то есть middleware - это функция или функции, выполняемые между запросом(requests) клиента и ответом сервера(response). Наиболее распространенные функции промежуточного программного обеспечения - это управление ошибками, взаимодействие с базой данных, получение информации из статических файлов или других ресурсов. Чтобы перейти на стек промежуточного программного обеспечения, необходимо вызвать следующий обратный вызов, вы можете увидеть его в конце функции промежуточного программного обеспечения, чтобы перейти к следующему шагу в потоке. 

**Warrning 2** А также по другому можно сказать веб-сервер можно рассматривать как функцию, которая принимает запрос и выдает ответ. Поэтому, если вы рассматриваете веб-сервер как функцию, вы можете организовать его на несколько частей и разделить их на более мелкие функции, чтобы их композиция была исходной функцией.

Промежуточное ПО - это более мелкие функции, которые вы можете комбинировать с другими, и очевидным преимуществом является то, что вы можете использовать их повторно.

**Warning 3** Написание промежуточного программного обеспечения для использования в приложениях Express:

Функции промежуточного программного обеспечения - это функции, которые имеют доступ к объекту запроса [request object](http://expressjs.com/en/4x/api.html#req) (req), объекту ответа [response object](http://expressjs.com/en/4x/api.html#res) (res) и следующей функции в цикле запроса-ответа приложения. Следующая функция - это функция в маршрутизаторе Express, которая при вызове выполняет промежуточное программное обеспечение, заменяющее текущее промежуточное программное обеспечение.

Функции промежуточного программного обеспечения могут выполнять следующие задачи:

* Выполните любой код.
* Внесите изменения в объекты запроса и ответа.
* Завершите цикл запрос-ответ.
* Вызов следующего промежуточного программного обеспечения в стеке.

Если текущая функция промежуточного программного обеспечения не завершает цикл запроса-ответа, она должна вызвать next (), чтобы передать управление следующей функции промежуточного программного обеспечения. В противном случае запрос останется зависшим.

![img](../../img/gVnGL.png)

* Определение маршрута имеет следующую структуру:

```algorithm
app.METHOD(PATH, HANDLER);
```

Где:
* app - это экземпляр express.
* METHOD - метод запроса HTTP.
* PATH - путь на сервере.
* HANDLER - функция, выполняемая при сопоставлении маршрута.

Смоти ниже ...

* **Как обрабатывать ошибки 404?** В Express код 404 не является результатом ошибки. Обработчик ошибок не фиксирует их, потому что код ответа 404 указывает лишь на факт отсутствия дополнительной работы. Другими словами, Express выполнил все функции промежуточной обработки и маршруты и обнаружил, что ни один из них не отвечает. Все, что вам нужно сделать, - добавить промежуточный обработчик в конец стека (после всех остальных функций) для обработки кода 404:

```js
app.use(function(req, res, next) {
  res.status(404).send('Sorry cant find that!');
});
```

* **Как определяется обработчик ошибок?**
Функции промежуточного обработчика для обработки ошибок определяются так же, как и другие промежуточные обработчики, но с указанием не трех, а четырех аргументов в сигнатуре (err, req, res, next)).

```js
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});
```

* Свяжите промежуточное ПО уровня приложения с экземпляром объекта приложения с помощью функций app.use () и app.METHOD (), где МЕТОД - это HTTP-метод запроса, который обрабатывает функция промежуточного ПО (например, GET, PUT или POST) строчными буквами.

В этом примере показана функция промежуточного программного обеспечения без пути монтирования. Функция выполняется каждый раз, когда приложение получает запрос.

```js
app.use(function (req, res, next) {
  console.log('Time:', Date.now())
  next()
})
```

* В этом примере показана функция промежуточного программного обеспечения, установленная по пути / user /: id. Функция выполняется для любого типа HTTP-запроса по пути / user /: id путь. 

```js
app.use('/user/:id', function (req, res, next) {
  console.log('Request Type:', req.method)
  next()
})
```

* маршрутизатор и его функция обработчика, обрабатывает систему промежуточного программного обеспечения. Функция обрабатывает запросы GET к пути / user /: id.

```js
app.get('/user/:id', function (req, res, next) {
  res.send('USER')
})
```

* Загрузки серии функций промежуточного программного обеспечения в точку монтирования с путем монтирования. Он иллюстрирует вложенный стек промежуточного программного обеспечения, который выводит информацию о запросе для любого типа HTTP-запроса по пути / user /: id.

```js
app.use('/user/:id', function (req, res, next) {
  console.log('Request URL:', req.originalUrl)
  next()
}, function (req, res, next) {
  console.log('Request Type:', req.method)
  next()
})
```

* ???

```js

/* Create Middleware */
function middleware(){};

// Get all users from database.
var stack = middleware(-function(req, res, next) {
    users.getAll(function(err, users) {
        if (err) next(err);
        req.users = users;
        next();  
    });

    // add a post ?
}, function(req, res, next) {
    posts.getAll(function(err, posts) {
        if (err) next(err);
        req.posts = posts;
        next();
    })

    // get a post, article.
}, function(req, res, next) {
    req.posts.forEach(function(post) {
        post.user = req.users[post.userId];
    });

    // See all psots ?
    res.render("blog/posts", {
        "posts": req.posts
    });
});

// But Here ?
app.get("/posts", function(req, res) {
   stack.handle(req, res); 
});

app.get("/posts", [
    function(req, res, next) {
        users.getAll(function(err, users) {
            if (err) next(err);
            req.users = users;
            next();  
        });
    }, function(req, res, next) {
        posts.getAll(function(err, posts) {
            if (err) next(err);
            req.posts = posts;
            next();
        })
    }, function(req, res, next) {
        req.posts.forEach(function(post) {
            post.user = req.users[post.userId];
        });

        res.render("blog/posts", {
            "posts": req.posts
        });
    }
], function(req, res) {
   stack.handle(req, res); 
});
/* End middleware */
```

* ?

```js
// middleware
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', router);
app.use(log_errors);
app.use('/', learn_readline);
app.use(error_handler); // status ?
api.use(logger('dev'));
api.use(bodyParser());
app.use('/', connect_db); // connect to postgres
```

----

* Вот простой пример функции промежуточного программного обеспечения под названием `my_logger`. Эта функция просто печатает `LOGGED`, когда запрос на приложение проходит через него. Функция промежуточного программного обеспечения назначается переменной с именем my_logger.

```js
var my_logger = function (req, res, next) {
  console.log('LOGGED')
  next()
}

app.use(my_logger)
```

> Обратите внимание на приведенный выше вызов next (). Вызов этой функции вызывает следующую функцию промежуточного программного обеспечения в приложении. Функция next () не является частью Node.js или Express API, но является третьим аргументом, который передается функции промежуточного программного обеспечения. Функцию next () можно назвать как угодно, но по соглашению она всегда называется «next». Чтобы избежать путаницы, всегда используйте это соглашение.

Чтобы загрузить функцию промежуточного программного обеспечения, вызовите app.use (), указав функцию промежуточного программного обеспечения. Например, следующий код загружает функцию промежуточного слоя myLogger перед маршрутом к корневому пути (/).

```js
var express = require('express')
var app = express()

var myLogger = function (req, res, next) {
  console.log('LOGGED')
  next()
}

app.use(myLogger)

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.listen(3000
```

Каждый раз, когда приложение получает запрос, оно выводит на терминал сообщение «LOGGED».

Порядок загрузки промежуточного программного обеспечения важен: функции промежуточного программного обеспечения, которые загружаются первыми, также выполняются первыми.

Если myLogger загружается после маршрута к корневому пути, запрос никогда не достигает его, и приложение не выводит «LOGGED», потому что обработчик маршрута корневого пути завершает цикл запрос-ответ.

Функция промежуточного программного обеспечения myLogger просто печатает сообщение, а затем передает запрос следующей функции промежуточного программного обеспечения в стеке, вызывая функцию next ().

----

* none

```js
console.log("none");
```

* ???

```js
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname,'public')));
```

### Links :
* [Часто задаваемые вопросы FAQ](https://expressjs.com/ru/starter/faq.html)