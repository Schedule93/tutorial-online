### Modules. Express.js :

* Cînd noi trevuie să folosim modulul `path` ?

```js
const path = require('path');
```

* Care este diferența dintre modulul `url` și `path` ?
```js
const url = require('url');
```

* Probabil cînd vrem să însriem ceva în baza de date sau json format care are RPS cu mult mai mare decît baza de date, fie Postgres sau MongoDB.
```js
const fs = require('fs');
```

* Cum sa fac modulul meu personal ? Și cîte moduri poți să faci.
```js
const router = require('./routes/routes.js');
```

```js
const readline = require('readline');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const pug = require('pug');
const hash require('pbkdf2-password')();
const logger = require('morgan');
```

* Diferent between `session` and `cookie` ?

```js
const session = require('express-session');
const cookieParser = require('cookie-parser');
```

* Pasport.js

```js
var passport = require('passport');
```

* ssss

```js
var createError = require('http-errors');
```
