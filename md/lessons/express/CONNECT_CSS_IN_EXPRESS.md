### [return main page](../../README.md) | Chapter 13. Learn Express.js : 

### Connect to CSS

**Error** cind o sa ai asa gresala `Refused to apply style from [...] because its MIME type ('text/html') is not a [...] supported stylesheet MIME type, and strict MIME checking is enabled.` Înseamnă cî fă urmatorii pași :

1. `href="text-css"` from html file must delete.
1. In main.js add this line `app.use( express.static( path.join(__dirname, 'public') ) );`

* Structure at project

```algorithm
public
	css
	js
	img
	fonts
main.js
```
