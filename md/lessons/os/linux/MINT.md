### [return main page](../../../README.md) | [Chapter 25. Learn Operating Systems](../page-main-os/PAGE_OS.md) :

**Download software for Linux Mint**

* [Rust Language](https://idroot.us/install-rust-ubuntu-16-04-lts/)
* [Addobe Flash Player](https://itsfoss.com/install-adobe-flash-player-in-ubuntu-13-04/)
* [Google Chrome](https://www.wikihow.com/Install-Google-Chrome-Using-Terminal-on-Linux)
* [install Sublime Text 3](https://tipsonubuntu.com/2017/05/30/install-sublime-text-3-ubuntu-16-04-official-way/)
* [Node.js](https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/)
* [VLC Player](#)
* [Git](https://linuxize.com/post/how-to-install-git-on-ubuntu-18-04/)
* [ClipGrab](ttps://tipsonubuntu.com/2016/09/08/clipgrab-lets-download-youtube-vimeo-videos-ubuntu/)
* [Qmmp](https://www.fossmint.com/qmmp-multimedia-player-for-linux-alternative-to-winamp/)
* [EMACS](http://ubuntuhandbook.org/index.php/2019/02/install-gnu-emacs-26-1-ubuntu-18-04-16-04-18-10/)

### Extern Links :
* Link 1
* Link 2
