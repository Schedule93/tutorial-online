### [return main page](../../../README.md) | [Chapter 25. Learn Operating Systems](../page-main-os/PAGE_OS.md) :

### Operating Systems :

**Вопросы**
* Что такое Линукс ?
* Для чего он нужен ?
* Возможности линукс :
	* Оптимизация системы
	* Нет ограничений только твой фантазии
	* Шифрование данных
	* Security
	* [High Performance](https://www.monitis.com/blog/20-linux-server-performance-tips-part-2/)
	* Гипкость системы
	* и многое другое
* Плюсы и Минусы
* [Distributives](DISTRIBUTIVES.md)
* [Рабочие столы](DESKTOP_ENVIRONMENTS.md)
* Advantage
* Minuses
	* Обновление
	* старый интерфейс
	* Сложный
	* [Нет идиного стандарта](https://www.youtube.com/watch?v=72EUCJdmvG0&list=PLQqEY2kzSbZ4tQKqls5EYw5S1dKiFvuWN)
* Backups for Linux 

**Ответы**

* **Что такое Линукс?** Если кратко это операционая система похоже на конструктор. Делай что хочешь как хочешь и самое главное когда ты пожелаешь. Только будь осторожен в некотырые слуиях он работает действительно страно. Например обновление линукс, не во всех дистрибутивы но это та еще боль. Об этом мы еще поговорим. Так вот, если в `Windows` устанавливаешь систему и работаешь то в Линукс это не так. Ты должен её настраивать под себя и свой нужды. Например, рабочие окружение или в некоторых случаях нет нормальных драйверов. Потому что производитель не хочет выкладывать свой код в свободным доступе. Самое главное что должен знать про линукс это то что когда пользаватель начинает знакомство с отой операционый системе часто видит что ему то или другое вещь не нравится.
* **Для чего он мне нужен ?** я думую, мне нужен линукс потому что хочу держать свой сервер у себя дома без `AWS`, `Digital Ocean` ну может как варянт "HEROKU". Пока думаю. Самое главное для чего мне нужен линукс, канешно личный домашний сервер.

### Первые шаги в мире линукс

1. [Learn the basic linux commands](http://blog.sedicomm.com/2017/07/04/komandy-linux-ot-a-do-z-obzor-s-primerami/) or [here](https://lifehacker.ru/komandy-linux/)
2. Learn about the filesystem structure. What are the files stored in /etc?
3. Learn about linux permissions. -rwx-r-x-r-x what?!
4. Learn about linux processes. There's demon.. eh.. daemons in linux?!
5. Briefly read up the popular servers that can be run on linux. Apache, MySQL, DNS, NFS, Samba, Squid - I have it all!
6. Try to set up a web or fileserver. Seriously, do it.
7. Learn a little about shell scripting. Can you automate your daily work?
8. Learn to use the man command - I use it every day.
9. If you don't remember exactly what the command was, type the first letter or two of the command and press Tab twice, this will list all commands starting with the given letter.
9. Playing around, moving files, starting and stopping processes, setting up Apache etc. is the best way to learn in my opinion.