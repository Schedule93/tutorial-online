### [return main page](../../../README.md) | [Chapter 25. Learn Operating Systems](../linux/PAGE_LINUX.md) :

### Learn Operating System Linux
* [Система Инициализации](INIT_SYS.md)
* [Firsts Steps](FIRSTS_STEPS.md)
* [Distributives](DISTRIBUTIVES.md)
* [Update](UPDATE.md)
* [Users and Groups](USER.md)
* [Variabile Environment](VAR_ENV.md)
* [File System](FS.md)
* [Review](REVIEW.md)
* [Questions](QUESTIONS.md)
* [File Manager](FM.md) (File System Structure)
* [Package Manager](PM.md)
* [Window Managers](WM.md)
* [Системный трей](https://www.youtube.com/watch?v=zsBevItnwLw)
* [Kernel Core](KERNEL.md)
* [Clusters](CLUSTER.md)
* [Tom](TOM.md)
* [Tasks](TASK.md)
* [VPS/VDS](https://veesp.com/ru/blog/how-to-setup-web-server-on-virtual-server)
* [SSH](SSH.md)
* [Programs](PROGRAMS.md)
* [Grab](GRAB.md)
* Profile: in Linux is called `perf`
* EFI
* SNAP
* Drivers :- p.s. в последнее время данные вопросы менее актуальны ввиду подписи прошивок, но что будет если ключ утечёт? [see here](https://m.habr.com/ru/company/1cloud/blog/464019/comments/)
* What means generic ?
* [Profilers](PROFILERS.md)
* [Планировшик Linux](#)

### Книги
* [Read Linux Books : Part 1](https://proglib.io/p/6-best-linux-books/)
* [Read Linux Books : Part 2](https://proglib.io/p/unix-linux-books)
* Unix и Linux. Руководство системного администратора. Э.Немет, Г.Снайдер и др
* Unix. Профессиональное программирование. У.Р.Стивенс, С.А.Раго

### YouTube
* [Codeby](https://www.youtube.com/watch?v=r4tK4ifayF8&list=PLPxEwGlYuJQlGUzoAoZsNlSHjIgRUe9dU&index=1)

### Notes 
* нужно привыкнуть к таким концепциям, как `репозитории`, `зависимости`, `пакеты и менеджеры пакетов`. 

### Extern Links : 
* [AWS  на русском](https://www.youtube.com/watch?v=Ck1SGolr6GI&list=PLg5SS_4L6LYufspdPupdynbMQTBnZd31N)
* [High Performance](https://www.zap.org.au/~john/intro-to-linux-and-hpc.pdf)
* [Основы Linux](http://www.linux-ink.ru/static/Docs/Courses/adv-user-guide/adv-user-guide/index.html)
* [YouTube : Администратор Linux](https://www.youtube.com/watch?v=3ggJ9MTg0QU&list=PLQedGskj0bhhaiYLcgY3JAatiiznrX_WZ&ab_channel=%D0%A2%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%D0%9F%D0%9E)
* [Chrome](https://developer.chrome.com/docs/devtools/network/reference/w)
