### [reurn main page](../../README.md) | [Chapter 25. Learn Operating Systems](../PAGE_OS.md) :

### GRAB
**În primul rînd trebuie să rezolv problema `etc/dev` sau geva de genu ista**
* Когда компьютер загружается, BIOS передает управление первому загрузочному
* Первый сектор жесткого диска называется Master Boot Record (MBR)
* Длина этого сектора всего 512 байт. Он содержит небольшой фрагмент кода (446 байт), который называется основной загрузчик, и таблицу разделов (64 байта). Таблица разделов содержит описание первичных и дополнительных разделов жесткого диска.
* MBR vs GPT

### Links 
* [Wiki Book](https://ru.wikibooks.org/wiki/Grub_2)
* [Ubuntu](https://help.ubuntu.ru/wiki/grub)
* [Восстановление GRUB](https://help.ubuntu.ru/wiki/%D0%B2%D0%BE%D1%81%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_grub)
* [Мультизагрузка](https://ru.d-ws.biz/articles/multiboot.shtml)
* [GRUB Documentation GNU](https://www.gnu.org/software/grub/grub-documentation.html)
* [GNU GRUB Manual](https://www.gnu.org/software/grub/manual/grub/grub.html)
* [Grub Arch](https://wiki.archlinux.org/index.php/GRUB_(%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9))

**Grub2**

* [GRUB - полное руководство](http://rus-linux.net/MyLDP/boot/GRUB-bootloader-Full-tutorial.html)