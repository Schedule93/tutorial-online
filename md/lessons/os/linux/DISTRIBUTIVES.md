### [return main page](../../README.md) | [Chapter 25. Learn Operating Systems](../PAGE_OS.md) :

### Distriutives :
* [UBUNTU](UBUNTU.md)
* [MINT](MINT.md)
* [Red Hat](RED_HAT.md)
* [KALI](KALI.md)
* [DEBIAN](DEBIAN.md)
* [GENTOO](GENTOO.md)
* [ARCH](ARCH.md)
* Black Arch

### Read Books
* [The BlackArch Linux Guide На Русском Языке](https://blackarch.org/blackarch-guide-ru.pdf)

### Links
* [Debian Vs Arch](https://www.educba.com/debian-vs-arch/)
* [Distributives Linux](https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg)
* [Parabola](#) odsnovanaia na Arch Linux
* zagrucsiki Operatings Systems - `UEFI`, `BIOS` is private but `Core Boot` or `Libre Boot` is open source. 
* [Ядро Wiki](https://ru.wikipedia.org/wiki/%D0%AF%D0%B4%D1%80%D0%BE_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D0%BE%D0%B9_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D1%8B)