### [reurn main page](../../README.md) | [Chapter 25. Learn Operating Systems](../PAGE_OS.md) :

### Gentoo - Advantages and Disadvantages

### Advantages
* High Performance. Но я не уверен что производительность будет заметно. Возможно на 10%, или максимум на 20%.
* Гентоо любят не за то что оптимизирует под конкретное железо, а за то что систему можно заточить под любые задачи.
* Написан на С++.
* OpenRC

### Disadvantages
* Дистрибутив Gentoo не стабилен, ломается как твоя бывшая :). Ну это не точно. Я понимаю всё зависит от пользователя, но всё равно я считаю что разработчики ПО допускают уязвимости в своих программах.
* Не безопасный дистрибутив. Ну это не точно, возможно правда, а возможно это просто стереотип. Хотя я склоняюсь к первому. Вот [сылка](https://www.gentoo.org/support/security/#:~:text=Security%20is%20a%20primary%20focus,patches%20to%20secure%20those%20vulnerabilities.)
* BSD2 License.
