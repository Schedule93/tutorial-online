![image](../../../img/wallpaper2you_91833.jpg)

### [return main page](../../../README.md) | [Chapter 23. Learn Operating Systems](../page-main-os/PAGE_OS.md) :

* [Administrative Tools](ADMIN_TOOLS.md)
* Command line
* The Core
* [Register](REGISTRY.md)
* [Encoding](../page-main-os/ENCODING.md)
* [Горячие Клавиши Windows](HOT_KEYS_WIN.md)
* [Microsoft Management Console](MMC.md)
* Vezi bat file - exemplu `main.bat`, bat merge ca rashirenie || bat файлы
* Steps Recorder

### Links 
* [MSC](https://www.google.com/search?q=msc+microsoft&sxsrf=ALeKk02Tfk1NRcJdtGg3irJer6BiGe7x3Q%3A1621502442802&ei=6immYOuxMIzosAfTjpCYCw&oq=msc+microsoft&gs_lcp=Cgdnd3Mtd2l6EAMyAggAMgIIADICCAAyBggAEBYQHjIGCAAQFhAeMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeMgYIABAWEB4yBggAEBYQHjoHCAAQRxCwAzoNCC4QxwEQowIQsAMQQzoHCAAQsAMQQzoECAAQQzoFCAAQyQNQ_BtYzipg1StoAXACeACAAaABiAH7CpIBBDAuMTGYAQCgAQGqAQdnd3Mtd2l6yAEKwAEB&sclient=gws-wiz&ved=0ahUKEwjrzuuo99fwAhUMNOwKHVMHBLMQ4dUDCA4&uact=5)
* Link 2
* Link 3

### Tasks :
* Зделать на Windows 10 полупрозрачным:
    1. проводник окон. `win + e`
    2. Текстовый Редактор
    3. PowerShell

