### [return main page](../../../README.md) | [Chapter 18. Learn Operating Systems](../page-main-os/PAGE_OS.md) :

### Administrative Tools and them description.
* Component Services
* Computer Management
* Defragment and Optimize Drives
* Disk Cleanup
* Event Viewer
* iSCSI Initiator
* ODBC Data Sources (32-bit)
* ODBC Data Sources (64-bit)
* Performance Monitor
* Recovery Drive
* Registry Editor
* Resource Monitor
* Services
* System Configuration
* [System Information](SYSTEM_INFORMATION.md)
* [Task Scheduler](TASK_SCHEDULER.md)
* Windows Defender Firewall with Advanced Security
* Windows Memory Diagnostic

### Links :
* Link 1