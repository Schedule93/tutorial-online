![IMG LEARN TERMINAL](../../img/naruto-friends.png)

### [return main page](../../README.md) | Chapter 4. Learn. How to use Web Pack : 

### Chapter 4 : Web Pack

1. install webpack with the help at Node.js from terminal:

* `npm install -D webpack webpack-cli`

2. Create a new file in root project

* `webpack.config.js`

### Links :
* [Youtube : Webpack. Full Course 2020](https://www.youtube.com/watch?v=eSaF8NXeNsA&list=PLQqEY2kzSbZ4NMd7xsuc28a6Kc-_300Jb&index=6&ab_channel=%D0%92%D0%BB%D0%B0%D0%B4%D0%B8%D0%BB%D0%B5%D0%BD%D0%9C%D0%B8%D0%BD%D0%B8%D0%BD)
* [YouTube : Play List => Jack Coder](https://www.youtube.com/watch?v=JcKRovPhGo8&list=PLkCrmfIT6LBQWN02hNj6r1daz7965GxsV&index=1)