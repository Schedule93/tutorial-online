### [return main page](../../README.md) | [Chapter 3. Learn CSS](PAGE_CSS.md)

* Nu înțeleg de ce anume acest cod a mers. Mă refer la clasa `.align-header:hover` și `.align-header:hover a `. M-am chinuit mult.

```css
.header {
	display: block;
	background-color: auto;
	width: 100%;
	height: 30px;
	float: left;
	margin: 50px 0px 50px 0px; 
}

.align-header {
	background-color: #111;
	width: 1200px;
	height: 30px;
	margin: auto;
	border: 2px solid brown;
	border-radius: 20px;
	padding: 19px 18px 16px 18px;
	cursor: pointer;
}

.link_return_main_page {
	text-decoration: none;
	text-align: center;
	font-size: 24px;
	letter-spacing: 2px;
	color: gold;
}

.hyper_2_return_main_page_header {}

.align-header:hover {
	background-color: white;
	border: 2px solid green;
}

.align-header:hover a {
	color: green;
}
```

