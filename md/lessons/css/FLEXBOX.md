### [return main page](../../README.md) | [Chapter 3. Learn CSS](PAGE_CSS.md)

#### Questions:

* **Что такое flexbox ?** Flexbox это свойство CSS которые выравнивает элементы горизонтальном или вертикальном направлениях контейнера автоматически и распределяет равномерно. Другими словами это, замена ствойству CSS { `float: left | right` }
* **Для чего он нужен?** Нужен для того чтобы выравнивать блоки HTML элементов качественно и автоматически без танцев с бубнами, вроде `float: left`, `margin: auto;`, `padding: 0px;`. А может даже `display: inline-block`, `position: relative;` и другие.
* **И как им полбзоватся?** [...]
* **Для чего он был придуман?** Он был задуман для сложный и гибкий макетов.
* **Какую проблему он решает?** [...]

## Introduction

* Главное что вы должны знать про `flexbox`:
	* `flex-container` : содержит flex-elements. flex-container это просто блок и его можно назвать как угодно не обязательно как этот.
	* `flex-elements` : это может быть теги `<li>`, вложенные блоки `div` либо многое другие. 
	* `Оси` : эти оси определяет направления. Но их легко можно изменить.
		* основная ось : Идёт слева на право.
		* поперечная ось : Идёт сверху вниз. 

**Warning** Одни свойства применимы только контейнеру а другие только к элементам.

## Свойства приминимы только контейнеру.

#### flex

```cs
display: flex;
display: inline-flex;
```

#### flex-direction

* Свойства приминимы контейнеру.
	* `flex-direction: row;` Значение по умолчание 
	* `flex-direction: row-reverse;` Направление основной ось уже начинается с право-лево 
	* `flex-direction: column;` Основная ось идёт сверху-вниз. А паперечная слева-направо и елементы расположены вертикально. 
	* `flex-direction: column-reverse`; Снизу-вверх элементы будут отображатся.
* Свойства приминимы к елементам.

**note** : Часто они применяются к тегам `ul`,

----

display: flex

Vez ca nu e corect să gîndești că proprietatea flex-direction va începe din partea stîngă spre partea dreaptă așa cum era înainte. Fiincă acum deseori se întîlnesc cazuri cînd trebuie să îneapă invers din partea dreaptă spre partea stîngă. Spre exemplu limba arabă. flex-direction:row-reverse fiincă cum am mai spus mai sus că limba lor diferă de altele prin faptul că acolo totdeauna se începe din partea dreată. Din acest caz așa se face. Asta e doar un exemplu de ce nu trebuie de gîndit așa : din stinga în drepta sau invers. Corect va fii inceput și svîrșit. Așa că la modul implicit, adica din partea stingă spre dreapta se utilizează. Dar oricînd poți schimba acest mood.  

----

#### flex-wrap  

Приминим тольько для контейнера. По умолчание когда не хватает места в ряд для элементов он не спускает елементы вниз. Другими словами если не помешяется елементы в одну строку то ствойство `flex-wrap:;` автоматически ставит на одно строку ниже. Это хорошо для тово чтобы лучшее отобразить сайт на телефон или планшет.

* `flex-wrap: nowrap;` Значение по умолчани. ничего не делает 
* `flex-wrap: wrap;` Новый нижний ряд
* `flex-wrap: wrap-reverse;` Скоро будет ...

**note 1 : flex-wrap** : Vezi fii atent cu `flex-wrap: wrap;`. Uitete un exemplu simplu daca tu ai un block mare cu 500px de exemplu si inside lui ai 7 elemente a fiecarui 100px + margin 5px. Atunci numai flex-wrap: se va cobori mai jos cu o line. Adica nu se va mai ajune spatiu sil va cobori mai jos. 

**note 2 : flex-wrap** Nu incurca ceia ce am scris mai sus `la note 1` cu ceia ce cind deschiz google chrome si incerci sa vizualizezi pentru diferite devices ca telefon sau planshet. Aici putin nu mi clar trebuie sa mai studiez ... Matinca daca nu o sa fie stoista ast presupun ca ele o sa inghisueaska intre ele dar cu flex-wrap ele o sa duca in alt rind mai jos. 

#### flex-flow

* `flex-flow: row wrap;` Объединенное свойств flex-direction и flex-wrap

Иногда своиства `flex-wrap: wrap;` может не работать, это и за того что всемсто `width` надо поставить ствойста `max-width` чтобы оно жымалось. Например. Или другой пример сылка на [Stack Overflow](https://stackoverflow.com/questions/44135352/flex-wrap-is-not-wrapping-when-i-reduce-the-window-size)

```html
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"> <title>learn-html</title>
	<link rel="stylesheet" href="css/w3school.css">
</head>
<body>
	<div class="flex-container ">
		<div class="flex-element">1</div>
		<div class="flex-element">2</div>
		<div class="flex-element">3</div>
		<div class="flex-element">4</div>
		<div class="flex-element">5</div>
		<div class="flex-element">6</div>
		<div class="flex-element">6</div>
		<div class="flex-element">7</div>
		<div class="flex-element">8</div>
		<div class="flex-element">9</div>
	</div>
</body>
</html>
```

```css
body {
	background-color: #eee;
}

.flex-container {
	background: #fff;
	display: flex;
	flex-wrap: wrap;
	flex-direction: row;
	justify-content: center;
	box-shadow: 1px 1px 2px #ccc;
	max-width: 800px;
	margin: auto;
	text-align: center;
}

.flex-element {
	background: orange;
	cursor: pointer;
	margin: 10px;
	border: 1px solid #000;
	padding: 10px 16px;
	width: 10px;
}
```

**note** Uite `flex-flow` asta in loc sa scrii flex-direction și flex-wrap. Ea prosta intr-o stoviostva scrii tot aceia numa ca intr-un rind. Exemplu `flex-flow: row no-wrap;` Tupa din 2 stvost noi tupa am facut una.

#### justify-content 

* `justify-content: center;`
* `justify-content: flex-end;` Находится в правом углу блока.
* Значение по умолчание `fustify-content: flex-start;` Находится в левом углу блока.
* `justify-content: space-around;` Indrepta elementele dupa, inainte si chiar intre ele.
* `justify-content: space-between;` Dintre linii.
* `justify-conten: space-evenly;`

**Note 1 : justify-content** La aceasta stoista nu treb de pus `width` sau `height`. Ca nu o sa mearga.

#### align-items

*Basic keywords*

* `align-items: normal;` 
* `align-items: stretch;` 

*Positional alignment* 
*align-items does not take left and right values.*

* `align-items: center;` Pack items around the center. 
* `align-items: start;` Pack items from the start. 
* `align-items: end;` Pack items from the end. 
* `align-items: flex-start;` Pack flex items from the start. 
* `align-items: flex-end;` Pack flex items from the end.

*Baseline alignment*

* `align-items: baseline;` 
* `align-items: first baseline;` 
* `align-items: last baseline;` Overflow alignment (for positional alignment only). 
* `align-items: safe center;` 
* `align-items: unsafe center;` 

*Global values*

* `align-items: inherit;` 
* `align-items: initial;`
* `align-items: unset;`

**note 1** Aceassta stvoistva po ideeie el indinde blocu de sus in jos, automat.

#### align-content

# Свойства приминимы к елементам.

#### order 

```css
.item {
  order: 5; /* default is 0 */
}
```

#### flex-grow

`flex-grow: 1;` Default. Cu cit numarul e mai mare cu atat se imulteshte la 2. Adica e mai mare blocu.

```html
<div class="flex-container ">
	<div class="flex-element element-1">1</div>
	<div class="flex-element element-2">2</div>
	<div class="flex-element element-3">3</div>
</div>
```

```css
.flex-container {
	background: #fff;
	display: flex;
	flex-wrap: wrap;
	flex-direction: row;
	box-shadow: 1px 1px 2px #ccc;
	max-width: 800px;
	margin: auto;
	text-align: center;
}

.flex-element {
	background: rgb(180, 50, 180);
	cursor: pointer;
	margin: 10px;
	border: 1px solid #000;
	padding: 10px 16px;
	width: 10px;
	order: 0;
	flex-grow: 1;
}

.element-2 {
	flex-grow: 2;
}
```

### flex-shrink: 1;

`flex-shrink: 1;` Default. Fii atent deoarece cu cit e mai mare numarul cu atat elementul este mai mic.

### flex-basis

`flex-basis: auto;` Значение по умолчание. Необходимо когда мы не хотим слишком много жать элемент в блоке. Вместо авто можно задать размер в `px`

### align-self

```css
.item {
  align-self: auto | flex-start | flex-end | center | baseline | stretch;
}
```

**Note** : În cel mai rău caz dacă ai uitat ... 

```css
.ul-align-header-links {
	display: flex;
	flex-direction: row;
	justify-content: center;
	width: 800px;
    height: 50px;
}
```

### Links
* [w3School.com](https://www.w3schools.com/css/css3_flexbox.asp)
* [The two axes of flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)
* [Tricks Display](https://css-tricks.com/almanac/properties/d/display/)
* [MDN : Syntax at display](https://developer.mozilla.org/en-US/docs/Web/CSS/display)
* [Stack Overflow : Why don't flex items shrink past content size?](https://stackoverflow.com/questions/36247140/why-dont-flex-items-shrink-past-content-size)