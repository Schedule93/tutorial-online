![sleep](../../img/anime-sleep-boy.png)

### [return main page](../../README.md) | Chapter 3. Learn CSS : 
* [flexbox](FLEXBOX.md)
* [Components](COMPONENTS.md)
* [WebKit](https://developer.mozilla.org/en-US/docs/Web/CSS/WebKit_Extensions)
* [Z index](https://www.w3schools.com/CSSref/tryit.asp?filename=trycss_zindex)
* [Text](TEXT.md)
* [Gradients](GRADIENTS.md)
* [Position](POSITION.md)
* [Image Gallery](https://www.w3schools.com/css/css_image_gallery.asp)
* [Styling Images](https://www.w3schools.com/css/css3_images.asp)
* [Forms](https://www.w3schools.com/css/css_form.asp)
* [Pagination](https://www.w3schools.com/css/css3_pagination.asp)
* [Multiple Columns](https://www.w3schools.com/css/css3_multiple_columns.asp)
* [Overflow](https://www.w3schools.com/css/css_overflow.asp)
* [Selectors CSS](SELECTORS.md)
* [Types Devices](DEVICES.md)
* [@font-face](fonts.md)
* [List](LIST.md)
* [Display](DISPLAY.md)
* [Links](LINKS.md) ???
* Box-sizing: border-box;
* Pseudo-classes
* Pseudo-elements
* Grid
* Clear
* Outline
* Cursor
* Animation
* width: calc(50% - 400px);

```css
*, *:before, *:after {
	box-sizing: border-box;
}
```

```css
.wf-loading body {
	visibility: hidden;
}
```

```css
background-color: whitesmoke
```

* În css poți să folosești și alte metode în afară de `rgb();`.

```css
hwb(220, 61%, 4%)
hsl(220, 81%, 78%)
rgb(156, 185, 245)
#9cb9f5
```

### YouTube Video :
* See Анна Блок on the YouTube(CSS3).

### Extern Links :
* [How to make ul and li style in css just for menu](https://stackoverflow.com/questions/41668217/how-to-make-ul-and-li-style-in-css-just-for-menu) => Fa exemplu ist. Important!
* [Stack Overflow](https://stackoverflow.com/questions/7055393/center-image-using-text-align-center)
* [w3School : Text](https://www.w3schools.com/css/css_text.asp)
* [Em, px, rem](https://engageinteractive.co.uk/blog/em-vs-rem-vs-px)
* [Search form HTML & CSS beautiful](https://webdesign.tutsplus.com/tutorials/css-experiments-with-a-search-form-input-and-button--cms-22069)