### [return main page](../../README.md) | [Chapter 3. Learn CSS](PAGE_CSS.md)

### CSS : Position properties. 
There are five different position values:

* static
* relative
* fixed
* absolute
* sticky

#### Position: static;
Это позиция по умолчанию и в любом случае он ничего не делаит.  Например `top: 10px;`Блок остается на месте

```html
<!-- Soon will be code-->
```

```css
.square_1 {
	background-color: rgb(100, 0, 240);
	width: 50px;
	height: 50px;
	position: static;
	top: 10px;
}
```

#### Position: relative; 
Schimba din positia normala in cea care o vrei tu. Joacate cu top, left, right, bottom si o sa intelegi cum lucreaza. 

```html
<!-- Soon will be code-->
```

```css
.two {
	background-color: rgb(240, 100, 0);
	width: 50px;
	height: 50px;
	position: relative;
	top: 50px;
}
```

#### Position: fixed; 
Используем тогда когда нам необходимо чтобы при прокрутки страницы вниз блок оставался на месте а страница прокручивается дальше в низ.

```html
<!-- Soon will be code-->
```

```css
.three {
	background-color: rgb(100, 240, 0);
	width: 50px;
	height: 50px;
	position: fixed;
	right: 0;
}
```

#### Position: absolute; 
Po ideie cand dai prorutka la pagina in jos position: absolute el tot merge in jos.

```html
<!-- Soon will be code-->
```

```css
/* Soon will be code */
```

#### Position: sticky; 
Используется когда у нас есть список чего то, напимер контактов.

```html
<!-- Soon will be code-->
```

```css
/* Soon will be code */
```

* vertical-align: middle;

```css
vertical-align: middle;
```

### Links position property
* [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/position)
* [Codecademy](https://www.freecodecamp.org/news/how-to-use-the-position-property-in-css-to-align-elements-d8f49c403a26/)
* [Tricks](https://css-tricks.com/almanac/properties/p/position/)
* [Layout](https://learnlayout.com/position.html)
* [htmlbook](http://htmlbook.ru/css/position)
* [Advanced](https://internetingishard.com/html-and-css/advanced-positioning/)
* [Absolute Vs Relative](https://dzone.com/articles/css-position-relative-vs-position-absolute)
* [Tailwindcss](https://tailwindcss.com/docs/position/)
* [Tutorialpoint](https://www.tutorialspoint.com/css/css_positioning.htm)
