<!-- ../../public/html/header.html -->

![image](../../img/boss.jpg)

### [return main page](../../README.md) | | Chapter 10. Learn Preprocessor :

### Why Pug.js :
Почему `Pug.js` ? Ну как минимум иместь возможности сделать что то подобного как CMS (Content Management System) для администратора. То есть другими словами например, я хочу один раз песать HTML code, а изминялось везде где мне это потребуиться. Вот это и занимаеться `Pug.js` а при этом в обыном HTML мы должны копировать учаски кода которые мы хотим отображать идентично и поставить туда где нам нужно.

### For install pug :
1. You must have Node.js
2. Write in terminal `npm install pug-cli -g`
4. Go to your local project  and write: `npm install pug --save`
5. Check if you successed instaled Pug.js `pug --version`.
6. To see list with pug commands, write: `pug --help`.
7. For render pug create file `example.pug` then write in terminal : `pug check.pug -P` If will not be flag `-P` then will be compact mode. 
8. `npx pug footer.pug --pretty`

### Topics :
* [Tasks](TASKS.md)
* [Questions](QUESTIONS.md)
* ejs (analogy liek pug.js)
* pug.js
	* [Convert](https://html2jade.org/)
	* [Bootstrap](../../README.md)

### Extern Links :
* Link 1
* Link 2
