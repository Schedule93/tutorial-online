![img](img/wallpaper.png)

### [return main page](#)

### Learn TypeScript. Topics :
* [Introduction](docs/INTRO.md)
* [Symbols](docs/SYMBOLS.md)
* [Integration](docs/INTEGRATION.md)
* [Minuses](docs/MINUSES)
* [Questions](docs/QUESTIONS.md)
* [Tasks](docs/TASKS.md)
* [Examples](docs/EXAMPLES.md)
* [Errors](docs/ERRORS.md)

### Înainte de a învăța typescript, ai nevoie să cunoști:
* Geters and Set
* ECMAScript 6
* OOP (Basic)
* Pattern

### Algorithm
1. scoop install typescript
1. check version `tsc --version`
1. tsc - firt thing must be compiled the file
1. run with node `name_file.js`

### Extern links :
* [TypeScript Documentation](https://www.typescriptlang.org/docs/handbook/typescript-from-scratch.html)
