
* I don't remember what i made. ???

```js
let source: string = 'Nicolai is a good friend';

let result: string = '';

function DC(l: string): string {
	var consonants = 'bcdfghjklmnpqrstvwxyz';
	if (consonants.indexOf(l.toLowerCase()) != -1) return `${l}o${l}`;
	return l;
}

for (let l of source) result += DC(l);

console.log(result);
```