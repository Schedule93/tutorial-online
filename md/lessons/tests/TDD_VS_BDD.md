### [return main page](../../README.md) | [Chapter 27. Learn to do tests in JavaScrit and Node.js](PAGE_TESTS.md) :

### TDD VS BDD

* Understanding of TDD and BDD approaches in software development.

Они звучат похоже, и их легко запутать, но как нам разобраться в TDD и BDD? В TDD (разработка через тестирование) степень соответствия функциональности проверяется письменным тестовым примером. По мере развития кода исходные тестовые примеры могут давать ложные результаты. BDD (Behavior Driven Development) также является подходом, ориентированным на сначала тестирование, но отличается тем, что тестирует фактическое поведение системы с точки зрения конечных пользователей.

### links :
* [https://blog.testlodge.com/tdd-vs-bdd/](TDD vs BDD – What’s the Difference Between TDD and BDD?)