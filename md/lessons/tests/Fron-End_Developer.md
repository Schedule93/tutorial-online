**Front-End Developer:** 
* SSR
* SPAs and front-end frameworks (we currently use Vue.js, but we know you can pick this up if you worked with React previously)
* Apollo, 
* Web APIs, 
* Оптимизация и тестирование на совместимость с браузером,
* Component-based UI structuring promoting re-use of code and components, 
* Плавный и отзывчивый дизайн пользовательского интерфейса.
* Делаем продукты пригодными для использования на экранах любого размера и плотности пикселей.
* Modern Typescript and Javascript, Web APIs and CSS