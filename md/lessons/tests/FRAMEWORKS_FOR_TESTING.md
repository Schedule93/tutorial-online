### return

### Frameworks
* `WebdriverIO`. Не путать с Selenium WebDriver, который поддерживает только тестирование браузера. WebdriverIO — это платформа автоматизации тестирования как для браузерного, так и для собственного мобильного тестирования.
* `Cypress`
* `TestCafe` 
* `Playwright`
* `Puppeteer`
* `Mocha`
* `Jest`
* `Nightwatch`
* `Jasmine`
