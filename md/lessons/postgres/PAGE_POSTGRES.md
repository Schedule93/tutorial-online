![IMG LEARN POSTGRES](../../img/postgres.png)

### [return main page](../../README.md) | Chapter 12. Learn Postgres : 

* `индексация данных` - или quick search/find это когда мы пишем что то в гоогл или другой посисковый системе и отображает сылки на наш сайт. Например как хабра, у них очень хорошые инексации

### Commands Postgres
* `\?` - help doc
* ssdsd

### Node Postgres

<!-- Connect node postgress -->
* De exemplu ai file `con_node_pg.js`

```js
// Connect to database PostgreSQL
const connect_pg = require('express').Router();
const path = require('path'); 
const fs = require('fs');

// on windows 10 wil be going all well
// but on the linux it's a problem.
function err_connect_linux() {
	const {Client} = require('pg');
	const client = new Client({
        user: "postgres",
        password: "write_your_password",
        host: "localhost",
        port: 5432,
        database: "tutorial-online"  
    });

	client.connect()
		.then(() => console.log("Connected successfuly at PostgreSQL database"))
		.catch(e => console.log(e))
		.finally(() => client.end())
}

err_connect_linux();

console.info("I Now Work With File connect.js");
console.log("\n" + "The Route `connect_pg` going well.");
console.log("Se află în folderul database" + "\n");

module.exports = connect_pg;
```

### Video
* [YouTube : Аве Кодер](https://www.youtube.com/watch?v=PfyC39EzTmk&list=PLPPIc-4tm3YQsdhSV1qzAgDKTuMUNnPmp)

### Links 
* [Database](https://db-engines.com/en/ranking)
* [PostgreSQL and Node.js](https://mherman.org/blog/postgresql-and-nodejs/)
* [PSQL](https://gist.github.com/Kartones/dd3ff5ec5ea238d4c546)
* [node-postgres](https://node-postgres.com/)
* [Работа с PostgreSQL: от полного нуля до полного просветления](https://proglib.io/p/learn-postgresql/)
* [Installing and Configuring PostgreSQL 9.4 on Linux Mint/Ubuntu](https://www.codeproject.com/articles/898303/installing-and-configuring-postgresql-on-linux-min)
* [YouTube Уроки MySQL](https://www.youtube.com/watch?v=0fRpVTFyOgo&list=PLeYxjiX1MAIk1yC8Jb489zRRuN6HoS4FB)
* [Node-postgres](https://www.youtube.com/watch?v=ufdHsFClAk0&t=795s)
* [How To Install PostgreSQL Linux](https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart)
* [Triggers](https://blog.arctype.com/learn-sql-triggers/)
* [PostgreSQL : Документация](https://postgrespro.ru/docs/postgresql)