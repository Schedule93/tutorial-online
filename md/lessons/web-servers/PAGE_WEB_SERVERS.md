![image](../../img/like.jpeg)

### [return main page](../../README.md) | Chapter 19. Web Servers

### Web Servers
* [Overview : Comparison of web server software](https://en.wikipedia.org/wiki/Comparison_of_web_server_software)
* [OpenLiteSpeed](https://openlitespeed.org/)
* [Lighttpd](https://www.lighttpd.net/)
* [Node.js like Web Servers](NODE_SERVER.md)
* [Hiawatha Web Server](#)
* [GlassFish GNU License](#)
* [Apcache](APACHE.md)
* [Varnish](#)
* [Caddy](https://github.com/caddyserver/caddy)
* [NaviServer](#)
* [NGINIX](NGINX.md)
* [All list](https://www.google.com/search?sa=X&sxsrf=ALeKk02QLj-u5Wqzu3xfmlKWKfPkU3xr-A:1586723822899&q=Gunicorn&stick=H4sIAAAAAAAAAIVSPY_TQBC1pftIHJAu4YpTKAihuBNN7LXXH6IggASHRIQ40LXW7dpe5_wVe03sREIgfgIFLRIVBSUVSBR0pKC4AlEBorsKKkSDjiS365Zu3s57M29nprbalXpRTw7kIE0hOB8Oad5JvE7hog51s7GbdWji5cVB5s7E9QXT9sFMXGpA6uWWwkEoUyObiWd7pKcoBKnEnATRTKwvkgr2pyaLtagsFc7DkQztSLE4RrpcRNGId3AcP_AQA6cWGdBSNAI8A2CZlgGrD7IQlDz2Y8thpKK09UBjwKDQ9DXGIlibcrVyaOWQxYZRjAgTxI6KplWCRrlciSeE-9AmZjidJ2oLoONRyL-MEJjbaCwbyCSGNjelTYux5fMUUAOUKgs0nwUAJC1x5T6IbT-qBl86CuJ_pJRqlSbQVZ8yGiHW2DEYTfczb8JiT1VlwmJEqYOqcRPVrHboQQhNgxaMCDPDthhRxWNrXG3e0MbBpFpFDkZVCQyhIusa5laLvNT5TBQKMiZCEYIo5rUdUz8sfojHYmPj-M_Xc-1v4rPXHz-LX0Rp5bpL89YTUWrcd_MHySBxht6klbYSqT5wI-Rm9K7X2pekG0kYujgfJnFrV7rUvtDD1UNvftj26WHb_LB3hPZF6X-krrS3XKup-n53AK69PXr-ae1Kc_fdycm2vddvb3Zb0vpteifBB2Gzf_Tm8e_tn1e7W02yKXQe4f3-Vr0j7AjK3xfC93u_bvYvC0_fv_qwtlITN4QzL1drtx7GQ5xk8T9NEOLakAMAAA&ved=2ahUKEwiL3Mfu3uPoAhVB1qYKHR3dDy4Q-BYwIHoECBMQPw&biw=1366&bih=624)
* [Envoy](#) C++ | Micro Services Architectures

### Deploy
* ?
* ?

### Links
* [W3techs](https://w3techs.com/technologies/overview/web_server)
* [Вебсервер в домашних условиях](http://citforum.ru/internet/webservers/home/)
* [NGINIX VS Varnish](https://www.scalescale.com/tips/nginx/nginx-vs-varnish/)
* [Battle of web servers](https://muetsch.io/caddy-a-modern-web-server-vs-nginx.html)
* [siteuptime](https://www.siteuptime.com/blog/2019/12/19/how-to-create-your-own-server-at-home-for-web-hosting/)