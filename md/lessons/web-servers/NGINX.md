### [return main page](../../README.md) | Chapter 19. Web Servers

### NGINX Questions:
* Что такое Proxy? 
* А обратный Proxy Server?
* URI VS URL?
* NGINX и gRPC?
* VPN
* What is VDS ? { Virtual Dedicated Servers }
* What is VPS ? { Virtual Provate Server }

### Read Books :
* Link 2

### Notes
* Однако для повышения безопасности веб-серверы часто «прячутся» за прокси-сервером или балансировщиком нагрузки

### Links:
* [Установка NGINX на Windows 10](http://nginx.org/ru/docs/windows.html)
* [Nginx: Документация](http://nginx.org/ru/docs/)
