### Automation of Scripts :
**Important!** Aici este fișierul cu explicație. La automatizare la unele script-uri. Exemple :
* Vreau să fac așa ceva - Cînd pornesc calculatorul, deodată să pornească următoarele programe, precum, { Submlime-Text-3, cmder, Google Chrome, проводник в Windows 10, MPC-HC } Și așa mai departe.
* Cu ajutorul mause-ului, să simulez `mouse click`, doar fără mine. Crearea script-urilor, de acest gen.

### Explicație 1 :
* Cum să pornesc auto proprame, fișiere, chiar și în jormat.js ? Defapt sunt 3 posibilități de a face asta: 

**Primua metodă** este desigur să adaugi în folder `Startup`.  
1. Apasă `Win + R`. După enter. 
1. Scrie : `shell:startup`
1. Adaugă în acest folder toate programele de care ai nevoie. Dar! Important! Numai shurtcut. (ярлык)

```shell
@echo off
cd ../Documents/Developer/Github/my-projects/node-server
%AppData%\npm\nodemon.cmd main.js
```

**A doua metodă** cum putem face asta e `Task Scheduler`.

**A treia metodă** este desigur `bat files`. 

```BAT
start "" "C:\Program Files\Program1\program1.exe"
start "" "C:\Program Files\Program2\program2.exe"
```

Сохраните файл с расширением .bat и добавьте его в папку автозагрузки, как описано в первом способе.

### Extern links :
* link 1
* link 2
