### [return main page](../../README.md) || Project Screens :

* Pagina principală :
![img](../../public/img/TO.png)

* Paginca cu noutăți :
![img](../../public/img/project_screens/screen_01.png)

* Paginca cu noutăți - putțin mai jos, așa arată :
![img](../../public/img/project_screens/screen_02.png)

* Paginca cu noutăți - Deja jos la footer :
![img](../../public/img/project_screens/screen_03.png)
