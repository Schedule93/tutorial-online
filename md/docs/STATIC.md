![image](public/img/team-naruto.png)

### Description at this website.
Этот сайт все еще находится в разработке. Он еще очень сырой, так что много не ждите. Плюс я часто изменяю контент на страницах. И каждый день добавляю понемногу. `tutorial-online` который с трудом можно назвать сайтом был задуман во первых для себя чтобы я лучшее научился программировать. А у уже потом чтобы научить других программировать. Не поймите меня неправильно я сам много не знаю. Так что я делаю это с душой.

### Documentation at this site
Тут я напишу в будущем как пользоваться этим сайтом. Но пока хочу рассказать что ниже практически в каждой стати вы увидите внешние ссылки. Это сделано специально для меня чтобы научится программировать и не только. Я даже мечтаю создать оделный сайт по этой тематики.

### Tutorial Online - Full Stack JavaScript Developer :
* [Chapter 1. Learn Vim](md/lessons/vim-text-editor/PAGE_VIM.md)
* [Chapter 2. HTML5](md/lessons/html/PAGE_HTML.md)
* [Chapter 3. CSS3](md/lessons/css/PAGE_CSS.md)
* [Chapter 4. JavaScript](md/lessons/js/PAGE_JS.md)
* [Chapter 5. Web Pack](md/lessons/web_pack/WEB_PACK.md)
* [Chapter 6. SASS](md/lessons/sass/PAGE_SASS.md)
* [Chapter 9. Terminal](md/lessons/terminal/PAGE_TERMINAL.md)
* [Chapter 10. Git](md/lessons/git/PAGE_GIT.md)
* [Chapter 11. Node.js](md/lessons/node/PAGE_NODE.md)
* [Chapter 12. Express.js](md/lessons/express/PAGE_EXPRESS.md)
* [Chapter 13. Pug.js](md/lessons/pug/PAGE_PUG.md)
* [Chapter 14. PostgreSQL](md/lessons/postgres/PAGE_POSTGRES.md)
* [Chapter 15. React.js](md/lessons/react/PAGE_REACT.md)
* [Chapter 16. Redis](md/lessons/redis/PAGE_REDIS.md)
* [Chapter 17. GraphQL](md/lessons/GRAPHQL/PAGE_GRAPHQL.md) 
* [Chapter 18. ECMASCRIPT](md/lessons/ecmascript/PAGE_ECMASCRIPT.md)
* [Chapter 19. TypeScript](md/lessons/typescript/PAGE_TYPESCRIPT.md)
* [Chapter 20. Tests](md/lessons/tests/PAGE_TESTS.md)
* [Chapter ?. Network](md/lessons/network/PAGE_NETWORK.md)
* [Chapter ?. Mathematics](md/lessons/math/PAGE_MATH.md)
* [Chapter ?. Algorithms](md/lessons/algorithms/PAGE_ALGORITHMS.md)
* [Chapter ?. Hardware](md/lessons/hardware/PAGE_HARDWARE.md)
* [Chapter ?. Operating Systems](md/lessons/os/page-main-os/PAGE_OS.md)
* [Chapter ?. Web Servers](md/lessons/web-servers/PAGE_WEB_SERVERS.md)
* [Chapter ?. RabbitMQ](md/lessons/RabbitMQ/PAGE_RABBIT_MQ.md)
* [Chapter ?. Docker](#)
* [Chapter ?. Licenses App](md/lessons/licenses/PAGE_LICENSES_APP.md)
* [Chapter ?. Automatisation](md/lessons/automatisation/PAGE_AUTOMATION.md)
* [Chapter ?. Clouds](md/lessons/clouds/PAGE_CLOUD.md)
* [Chapter ?. Security](md/lessons/security/PAGE_SECURITY.md)
* [Chapter ?. Methodology](md/lessons/methodology/methodology.md)

### External Sites Links:
* [TechEmpower](https://www.techempower.com/benchmarks/)
* [Benchmarks Game](https://benchmarksgame-team.pages.debian.net/benchmarksgame/)
* [My GiHub](https://github.com/NicolaiCushnir?tab=repositories)
* [My Stack Overflow](https://stackoverflow.com/users/7037339/schedule)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Open Source Initiative](https://opensource.org/licenses)
