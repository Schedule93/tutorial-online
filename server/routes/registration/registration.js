var registration = require('express').Router();
var path = require("path");

// Routes :
registration.get("/sign_up", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../public/html/header-menu/registrations/SIGN_UP.html") );
});

module.exports = registration;
