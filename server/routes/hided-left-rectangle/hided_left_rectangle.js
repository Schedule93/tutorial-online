var hided_left_rectangle = require('express').Router();
var path = require("path");

// Routes :
hided_left_rectangle.get("/VIM", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../public/html/field_of_study/programming/text-redactors/text-redactors.html") );
});

module.exports = hided_left_rectangle;