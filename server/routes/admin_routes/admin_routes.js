
// 

let adm = require("express").Router();
let path = require("path"); 

// Bellow is routes at - Admin: 
adm.get("/connect_to_admin_panel", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../public/html/admin/connect-admin.html") );
});

adm.get("/admin_panel", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', './../../../public/html/admin/admin-page.html'));
});

adm.get("/admin_panel_tasks", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', './../../../public/html/admin/admin-page.html'));
});

adm.get("/admin_panel_record", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', './../../../public/html/admin/admin-page.html'));
});

adm.get("/admin_panel_settings", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', './../../../public/html/admin/admin-page.html'));
});

adm.get("/admin_panel_view", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', './../../../public/html/admin/admin-page.html'));
});

adm.get("/admin_panel_exit", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', './../../../public/html/admin/admin-page.html'));
});

module.exports = adm;