
// basement-routes:
let basement = require("express").Router();
let path = require("path"); 

basement.get('/LANGUAGES', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/LANGUAGES.html'));
});

basement.get('/FAQ', function(req, res){
	res.sendFile(path.join(__dirname, '../public', '/html/FAQ.html'));
});

basement.get('/SETTINGS', function(req, res){
	res.sendFile(path.join(__dirname, '../public', '/html/SETTINGS.html'));
});

basement.get('/ABOUT_US', function(req, res){
	res.sendFile(path.join(__dirname, '../public', '/html/ABOUT_US.html'));
});

basement.get('/map', function(req, res){
	res.sendFile(path.join(__dirname, '../public', '/html/basement/MAP.html'));
});

module.exports = basement;