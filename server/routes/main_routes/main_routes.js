var main_routes = require('express').Router();
var path = require("path");

// Routes :
main_routes.get("/", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../public/html/main-page/TUTORIAL_ONLINE.html") );
});

module.exports = main_routes;
