
// menu-header-menu

let header_menu_news = require("express").Router();
let path = require("path"); 

header_menu_news.get('/header_menu_news', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/header-menu/news/NEWS.html'));
});

header_menu_news.get('/NEWS_TOPIC_1', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/header-menu/news/TEMA_1.html'));
});

header_menu_news.get('/NEWS_TOPIC_2', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/header-menu/news/TEMA_2.html'));
});

header_menu_news.get('/NEWS_TOPIC_3', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/header-menu/news/TEMA_3.html'));
});

header_menu_news.get('/NEWS_TOPIC_4', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/header-menu/news/TEMA_4.html'));
});

module.exports = header_menu_news;