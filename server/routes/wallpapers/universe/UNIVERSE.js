
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var universe = require('express').Router();
var path = require("path");

// Routes :
universe.get("/universe", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/security_web/page_security_web/SECURITY_WEB.html") );
});

module.exports = universe;
