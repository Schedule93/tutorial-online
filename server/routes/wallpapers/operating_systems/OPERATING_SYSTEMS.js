/* aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa */ 

var os = require('express').Router();
var path = require("path");

// Routes :

//  This routes : Operating Systems
os.get("/operating_systems", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/page_os/PAGE_OS.html") );
});

os.get("/windows", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/windows/WINDOWS.html") );
});

// Below is routes for Linux :
os.get("/linux", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/linux/linux_page/PAGE_LINUX.html") );
});

os.get("/linux_distributions", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/linux/distributions/page_distributions/PAGE_DISTRIBUTIONS.html") );
});

os.get("/linux_mint", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/linux/linux_page/PAGE_LINUX.html") );
});

os.get("/ARCH_LINUX", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/linux/distributions/arch/PAGE_ARCH.html") );
});

os.get("/debian_linux", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/linux/distributions/debian/PAGE_DEBIAN.html") );
});

os.get("/gentoo_linux", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/operating_systems/linux/distributions/gentoo/PAGE_GENTOO.html") );
});

module.exports = os;
