
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var radio_electronica = require('express').Router();
var path = require("path");

// Routes :
radio_electronica.get("/intro_radio_electronica", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../public/html/field_of_study/radio_electronica/INTRODUCTION.html") );
});

radio_electronica.get("/radio_electronica", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../public/html/field_of_study/radio_electronica/RADIO_ELECTRONICA.html") );
});

radio_electronica.get("/PAGE-RADIO-ELECTRONICS", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../public/html/field_of_study/radio_electronica/page-radio-electronics/PAGE-RADIO-ELECTRONICS.html") );
});

module.exports = radio_electronica;
