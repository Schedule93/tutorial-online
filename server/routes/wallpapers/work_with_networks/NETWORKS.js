
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var network = require('express').Router();
var path = require("path");

// Routes :
network.get("/networks", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../public/html/field_of_study/work_with_networks/NETWORKS.html") );
});

module.exports = network;
