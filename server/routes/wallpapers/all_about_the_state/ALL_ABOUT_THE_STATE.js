
// Here is file :
// all about the state.
let all_about_the_state = require("express").Router();
let path = require("path"); 

all_about_the_state.get('/all_about_the_state', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/field_of_study/all_about_the_state/page-all-about-the-state/ALL_ABOUT_THE_STATE.html'));
});

module.exports = all_about_the_state;