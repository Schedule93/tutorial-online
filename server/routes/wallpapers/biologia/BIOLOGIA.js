
// Here is file :
// all about the state.
let biologia = require("express").Router();
let path = require("path"); 

biologia.get('/page-biologia', function(req, res){
	res.sendFile(path.join(__dirname, '../public', './../../../../public/html/field_of_study/biologia/page-biologia/PAGE-BIOLOGIA.html'));
});

module.exports = biologia;