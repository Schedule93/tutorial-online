
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var java_routes = require('express').Router();
var path = require("path");

//  This route : Java Chapter :
java_routes.get("/java-page", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../public/html/field_of_study/programming/stack-java/page-java/PAGE-JAVA.html") );
});

module.exports = java_routes;
