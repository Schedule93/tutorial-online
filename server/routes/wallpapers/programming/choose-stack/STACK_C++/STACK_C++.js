
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var stack_c_plus_plus = require('express').Router();
var path = require("path");

// Routes :

//  This route : JavaScript Chapter Chapter Inside Stack:
stack_c_plus_plus.get("/stack_c_plus_plus", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../public/html/field_of_study/programming/stack-c-plus-plus/page-C++/stack-C++.html") );
});

module.exports = stack_c_plus_plus;
