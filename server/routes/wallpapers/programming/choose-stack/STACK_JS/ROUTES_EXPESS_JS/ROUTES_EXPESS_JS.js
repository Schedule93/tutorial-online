
// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 
var express_routes = require('express').Router();
var path = require("path");

// Routes :
express_routes.get("/express.js", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Express.js/EXPRESS_JS.html") );
});

express_routes.get("/express.js_routes", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Express.js/EXPRESS_JS_ROUTES.html") );
});

module.exports = express_routes;
