
// Here is file: ROUTES React.js : 
var react = require('express').Router();
var path = require("path");

// Bellow is routes at react.js :
react.get("/react.js", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/PUG_PAGE.html") );
});

module.exports = react;
