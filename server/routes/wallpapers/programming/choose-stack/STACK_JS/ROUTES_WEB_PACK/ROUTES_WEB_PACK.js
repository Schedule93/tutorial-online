
// Here is file: ROUTES_CSS3.js 
var web_pack_routes = require('express').Router();
var path = require("path");

// Routes :
web_pack_routes.get("/web-pack", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/web-pack/PAGE-WEBPACK.html") );
});

module.exports = web_pack_routes;

