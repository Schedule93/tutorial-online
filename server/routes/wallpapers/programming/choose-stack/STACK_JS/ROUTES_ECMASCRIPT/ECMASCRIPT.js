
// Here is file: ROUTES_CSS3.js 
var ecmascript = require('express').Router();
var path = require("path");

// Routes :
ecmascript.get("/ecmascript", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/CSS3/CSS3.html") );
});

module.exports = ecmascript;

