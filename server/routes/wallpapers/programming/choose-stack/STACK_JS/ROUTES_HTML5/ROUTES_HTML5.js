
// Here is file: ROUTES_HTML5.js 
var ROUTE_HTML5 = require('express').Router();
var path = require("path");

// Routes :

//  This route : JavaScript Chapter Chapter Inside Stack:
ROUTE_HTML5.get("/javascript", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/javascript/javascript.html") );
});

ROUTE_HTML5.get("/stack-js", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/page-javascript/STACK-JS.html") );
});

ROUTE_HTML5.get("/arrow_funcs", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../public/html/field_of_study/programare/stack-javascript/JavaScript/ARROW_FUNCS.html") );
});

module.exports = ROUTE_HTML5;

