
// Here is file: AUTOMATION.js 
var automation = require('express').Router();
var path = require("path");

// Routes :
automation.get("/automation", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Automation/AUTOMATION.html") );
});

automation.get("/bat_files", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Automation/AUTOMATION.html") );
});

automation.get("/bash_scripts", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Automation/AUTOMATION.html") );
});

module.exports = automation;

