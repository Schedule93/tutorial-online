
// Here is file: ROUTES_CSS3.js 
var pug_routes = require('express').Router();
var path = require("path");

// Bellow is routes at Pug.js :

pug_routes.get("/PUG_PAGE", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/PUG_PAGE.html") );
});

pug_routes.get("/INTRO_PUG", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/INTRO_PUG.html") );
});

pug_routes.get("/FIRST_STEPS", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/FIRST_STEPS.html") );
});

pug_routes.get("/EXTENDED", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/EXTENDED.html") );
});

pug_routes.get("/INCLUDES", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/INCLUDES.html") );
});

pug_routes.get("/MIXINS", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/MIXINS.html") );
});

pug_routes.get("/ITERATION", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/ITERATION.html") );
});

pug_routes.get("/CASE", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/CASE.html") );
});

pug_routes.get("/FILTERS", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Pug.js/FILTERS.html") );
});

module.exports = pug_routes;
