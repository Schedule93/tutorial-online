
// Here is file: ROUTES_CSS3.js 
var css_routes = require('express').Router();
var path = require("path");

// Routes :
css_routes.get("/TERMINAL", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Terminal/TERMINAL.html") );
});

module.exports = css_routes;

