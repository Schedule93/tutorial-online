
// Here is file: ROUTES_CSS3.js 
var css_routes = require('express').Router();
var path = require("path");

// Routes :
css_routes.get("/CSS3", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/CSS3/CSS3.html") );
});

module.exports = css_routes;

