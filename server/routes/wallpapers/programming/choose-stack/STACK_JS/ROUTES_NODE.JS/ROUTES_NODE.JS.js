
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var node_routes = require('express').Router();
var path = require("path");

// Routes :
node_routes.get("/PAGE_NODE_JS", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Node.js/NODE_JS.html") );
});

node_routes.get("/file_systen", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Node.js/FILE_SYSTEM.html") );
});

module.exports = node_routes;
