
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var javascript_routes = require('express').Router();
var path = require("path");

// Routes :

//  This route : JavaScript Chapter Chapter Inside Stack:
javascript_routes.get("/javascript", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/javascript/javascript.html") );
});

javascript_routes.get("/stack-js", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/page-javascript/STACK-JS.html") );
});

// trebuie sa svhimbi la route html, nu js.
javascript_routes.get("/HTML5", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/HTML5/HTML5-PAGE.html") );
});

module.exports = javascript_routes;
