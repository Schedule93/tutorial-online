
// Here is file: ROUTES_CSS3.js 
var sass_routes = require('express').Router();
var path = require("path");

// Routes :
sass_routes.get("/SASS", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/web-pack/PAGE-WEBPACK.html") );
});

module.exports = sass_routes;

