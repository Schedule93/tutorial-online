
// Here is file: ROUTES_CSS3.js 
var git_routes = require('express').Router();
var path = require("path");

// Routes :
git_routes.get("/GIT", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Git/GIT-PAGE.html") );
});

git_routes.get("/GIT_BASH", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/Git/GIT-PAGE.html") );
});

module.exports = git_routes;

