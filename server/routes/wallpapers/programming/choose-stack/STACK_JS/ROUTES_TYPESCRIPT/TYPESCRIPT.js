
// Here is file: ROUTES_CSS3.js 
var typescript = require('express').Router();
var path = require("path");

// Routes :
typescript.get("/typescript", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/CSS3/CSS3.html") );
});

module.exports = typescript;

