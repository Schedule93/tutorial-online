
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var assembly_route = require('express').Router();
var path = require("path");

// Routes :

//  This route : JavaScript Chapter Chapter Inside Stack:
assembly_route.get("/stack_assembly_language", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../public/html/field_of_study/programming/stack-assembly/page-assembly/page-assembly.html") );
});

module.exports = assembly_route;
