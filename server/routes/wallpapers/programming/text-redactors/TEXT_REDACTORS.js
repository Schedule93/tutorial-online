

// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var text_redactors = require('express').Router();
var path = require("path");

// Routes :

//  This route : JavaScript Chapter Chapter Inside Stack:
text_redactors.get("/text_redactors", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../public/html/field_of_study/programming/text-redactors/page-text-redactors/text-redactors.html") );
});

text_redactors.get("/learn-vi", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../public/html/field_of_study/programming/text-redactors/learn-vi/LEARN-VI.html") );
});

text_redactors.get("/learn-vim", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../public/html/field_of_study/programming/text-redactors/learn-vim/LEARN-VIM.html") );
});

text_redactors.get("/sublime-text-3", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../public/html/field_of_study/programming/text-redactors/sublime-text-3/SUBLIME-TEXT-3.html") );
});

text_redactors.get("/visual-studio-community", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "../../../../../public/html/field_of_study/programming/text-redactors/visual-studio-community/VSC.html") );
});

module.exports = text_redactors;
