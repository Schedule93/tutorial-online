
// http://localhost:3/html/field_of_study/programare/js-stack/page-js-stack/express_js/EXPRESS_JS.html

var postgres_routes = require('express').Router();
var path = require("path");

// Routes :

//  This route : JavaScript Chapter Chapter Inside Stack:
postgres_routes.get("/POSTGRES", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../../public/html/field_of_study/programming/stack-javascript/technologies/PostgreSQL/POSTGRES.html") );
});

module.exports = postgres_routes;
