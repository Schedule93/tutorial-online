
var page_programming = require('express').Router();
var path = require("path");

// Routes :
page_programming.get("/stacks-of-languages-programming", function(req, res) {
	res.sendFile(path.join( __dirname, "../public", "./../../../../../public/html/field_of_study/programming/stacks-programming/STACKS-OF-LANGUAGES-PROGRAMMING.html") );
});

module.exports = page_programming;
