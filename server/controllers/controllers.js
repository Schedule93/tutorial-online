
// Connect File: controllers.js

// Color connected file to be more beautiful.
console.log(
	"%cFile" +
	"%c : " +
	"%cconsole.js" +
	"%c - " +
	"%c Was Connected ... " +
	"%c - " +
	"%cPATH : ../js/arrow_funcs.js (folder: public)",
	"background: #5fc7bb; color: white; border-radius: 4px; padding: 0px 8px 0px 8px; text-shadow: 0px 0.5px 2.5px black;  border: 1px solid black;",
	"color: black;",
	"background: orange; color: black; border-radius: 4px; padding: 0px 8px 0px 8px; border: 1px solid black;",
	"color: purple",
	"background: white; border: 1px solid green; color: green; border-radius: 4px;",
	"color: black",
	"background: silver; border: 1px solid black; border-radius: 4px; padding: 0px 8px 0px 8px;"
	);

// Bellow write your Code :

// Work with: routes/userRoutes.js
// Aceste 3 rinduri de mai jos, trebuie
// sa fie in root project, nu in routes
const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.get('/users', userController.getAllUsers);
router.get('/users/:id', userController.getUserById);
router.post('/users', userController.createUser);
router.put('/users/:id', userController.updateUser);
router.delete('/users/:id', userController.deleteUser);

module.exports = router;
